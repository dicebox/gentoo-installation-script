#!/bin/bash
# GPLv3+
# Copyright Julien Cerqueira 2019

# TODO: isolate user input requests in their own functions

# TODO: select partition table type: GPT or msdos
# TODO: write random data on luks device

# TODO: log in a file

# TODO: add colors to printf output

# TODO: store bootkey partition name in its own variable
# TODO: clean drives in a better way than just unreliable brute-force cleaning (lsblk could help)

# TODO: detect if the target VG name already exists, and if yes rename it (we are probably reinstalling on the same machine, that's name the VG name is already taken)

# TODO: having support for swapfile for hibernation, means that the small swap partition could be encrypted with a random key

function reload_variables {
    . ./variables.sh
}

function reload_actions {
    . ./actions.sh
}

function reload_menus {
    . ./menus.sh
}

function menu_main {
    clear
    reload_variables
    reload_actions
    reload_menus
    declare -a ordrarray=(
        exit
        menu_newinstallation
        menu_recovery_static
        menu_recovery
        reload_actions
        reload_variables
        reload_menus
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

menu_main
