# GPLv3+
# Copyright Julien Cerqueira 2019-2020

# TODO: use udisks/udisksctl to manage mounts
# TODO:unmountusbbootkey?
# TODO:fromtemporarysystem_setupnetwork
# TODO:fromtemporarysystem_setpartitionsizeusbbootkey
# TODO:fromtemporarysystem_verifyluksheaderbackup
# TODO:fromtemporarysystem_verifylvmheaderbackup
# TODO:fromtemporarysystem_clean_root_mountpoints
# TODO:fromtemporarysystem_cleanstage3
# TODO:fromtemporarysystem_downloadprofilesquickhack
# TODO:fromtemporarysystem_verifyprofilesquickhack
# TODO:fromtemporarysystem_uninstallprofilesquickhack
# TODO:fromtemporarysystem_createfstab
# TODO:fromtemporarysystem_createcrypttab
# TODO:fromtemporarysystem_createreposconf
# TODO:fromtemporarysystem_createmakeconf
# TODO:fromtemporarysystem_enableunstablegentoosources
# TODO:fromtemporarysystem_uninstallportagedependancies
# TODO:fromtemporarysystem_setupgpgportage
# TODO:fromtemporarysystem_bootstraptoolchain
# TODO:fromtemporarysystem_rebuildworld
# TODO:fromtemporarysystem_setupportageprofile
# TODO:fromtemporarysystem_convertxkeyboardlayout
# TODO:fromtemporarysystem_setupsystemdconsolekeymap
# TODO:fromtemporarysystem_setupsystemdconsolefont
# TODO:fromtemporarysystem_setupsystemdhostname
# TODO:fromtemporarysystem_setupsystemdlocales
# TODO:fromtemporarysystem_setupsystemdtimezone
# TODO:fromtemporarysystem_linksystemdmtab
# TODO:fromtemporarysystem_setupx11keyboard
# TODO:fromtemporarysystem_setupsystemdhosts
# TODO:fromtemporarysystem_buildfedoraconfigkernel
# TODO:fromtemporarysystem_buildfedoraconfigmodules
# TODO:fromtemporarysystem_buildinitrd
# TODO:fromtemporarysystem_mergeinitrdkernel
# TODO:verify secureboot backup
# TODO:fromtemporarysystem_selinux
# TODO:fromtemporarysystem_lockefibiossetup
# TODO:fromtemporarysystem_enablesecurebootsetupmode
# TODO:fromtemporarysystem_rebootnow
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:
# TODO:

# When running emerge inside systemd-nspawn, for example during installation
# the next error can pop up:
# Unable to configure loopback interface: Operation not permitted
# https://github.com/systemd/systemd/issues/13308

# https://wiki.gentoo.org/wiki/Catalyst stage3 building tools

### NEWINFRASTRUCTURE_FROMRUNNINGSYSTEM ###
function fromrunningsystem_downloadfedora {
    # TODO: options for wget?
    wget "${fedorabaseurl}${fedorachecksum}"
    wget "${fedorabaseurl}${fedoraiso}"
}

function fromrunningsystem_verifyfedora {
    curl "${fedoragpg}" | gpg --import
    gpg --verify-files "${fedorachecksum}"
    sha256sum -c "${fedorachecksum}"
}

function fromrunningsystem_selectliveusbkey {
    printf "Select the live usb key on which you want to copy the fedora live image\n"
    while [ "${liveusbmedia}" == "" ]; do
        # We show the full path of each symbolic link and their corresponding target
        udisksctl status
        printf "Type the full path (/dev/sdX or /dev/disk/by-id/ID) of the live usb media. Or press Enter to refresh the list of storage medium\n"
        read liveusbmedia
        #printf "\nliveusbmedia=${liveusbmedia}\n\n"
    done
}

function fromrunningsystem_preparefedorausbkey {
    # TODO: replace livecd-iso-to-disk by the set of commands to achieve the same result
    #wget https://raw.githubusercontent.com/livecd-tools/livecd-tools/master/tools/livecd-iso-to-disk.sh
    #chmod u+x livecd-iso-to-disk.sh
    #./livecd-iso-to-disk.sh --efi --format --reset-mbr --overlayfs --overlay-size-mb 2048 "${fedoraiso}" "${liveusbmedia}"
    # resulting usb key is not bootable on a efi system
    # we will need a second computer to generate the usb keys, so we do not have to constantly reboot, while debugging
    # we just do a direct write for now
    su -c "dd if=\"${fedoraiso}\" of=\"${liveusbmedia}\" status=progress"
}

function fromrunningsystem_installmooltipass {
    #mooltipy is much simpler to install but lacks the ssh-agent feature
    #moolticute daemon + mc-cli + mc-agent got more features, but moolticute daemon is much more complicated to install early

    #pip install --user mooltipy
    #pip install --user git+https://github.com/oSquat/mooltipy
    # unmerged python3 support
    #pip install --user git+https://github.com/bobsaintcool/mooltipy
    pip install --user git+file:///run/media/$(whoami)/Gentoo\ Install/leaf/mooltipy.git/
    #udev?

    #AND/XOR


    #we would need only the daemon for now
    #mkdir build
    #cd build
    #qmake -qt=5 ../Moolticute.pro
    #make

    #AND

    #go get github.com/raoulh/mc-cli
    #go get github.com/raoulh/mc-agent

    #mount liveusb-homedir
    #copy gpg key

    #   use Mooltipass
    #   - luks backup key (password)
    #   - luks primary key (gpg key)
    #   - gpg key (one per user)
    #   - email (one per user, local domain)
    #   - bios/efi setup
    #   - bios/efi admin
    #   - intelme
    #   - amdme
    #   - dracut remote login (gpg key)
}

function fromrunningsystem_initialisemooltipassandsmcard {
    printf "Mooltipass tutorial\n"
    read
    printf "Plug a smartcard into your Mooltipass and either create a new pin, or type its corresponding pin.\n"
    # Create new user? validate
    # Enter PIN
    # Re-enter PIN
    # How do we generate a safe pin?
    read
    # Do we need to do anything else? Will have to reset one of the mooltipass to check
}

function fromrunningsystem_settargethostname {
    printf "Type the hostname of the machine you will install\n"
    read targethostname
}

function fromrunningsystem_generatepassphrases { #move to only fromtemporary
    # NOTE: use us-altgr-intl keymap to be compatible with us keymap
    # us-intl isn't compatible with the us keymap
    #mooltipy login set example.com -u user_name
    #mc-cli login set

    #only passphrases needed fromrunningsystem
    #  bios/efi setup. size/characters restrictions?
    #  #bios/efi admin isnt that the same than bios/efi setup?
    #  intelme or amdme: ring0. size/characters restriction?
    #  remote installation ssh key
    mpusers="biosefiadmin ring0 remoteinstallssh"
    for mpuser in ${mpusers}; do
        mooltipy login set "${targethostname}" -u ${mpuser}
    done
}

function fromrunningsystem_generatekeys { #move to only fromtemporary
    #generate key password on mooltipass

    # We do not need a gpg key during installation
    #gpg --gen-key
    #store key on mooltipass. do we really want to do that?
    #gpg --output revoke.asc --gen-revoke mykey
    #store revoke.asc on mooltipass. this makes more sense

    if [ ! -e ~/.ssh ]; then
        mkdir -v ~/.ssh
        chmod -v 600 ~/.ssh
    fi
    ssh-keygen -t rsa -b 4096 -C "remoteinstall@${targethostname}" -f ~/.ssh/remoteinstall-${targethostname}

    #mooltipy data set ssh_key
    #mc-agent add

    #only keys needed during installation
    #  ssh temporary system key: "${targethostname}"-remoteinstallation-ssh
    #  luks key: "${targethostname}"-luks-gpg , this one can be generated later
}

function fromrunningsystem_boottargetmarchine {
    printf "Read the 4 following messages before booting the machine.\n"
}

function fromrunningsystem_lockefibiossetup { # from uefi/bios
    printf "Enter bios/uefi to set the setup/admin bios/efi password AND intelme/amdme password\n"
}

function fromrunningsystem_backupsecurebootkeys { # from uefi
    printf "If you want to/have to backup the secureboot keys directly from uefi you may want to do it now\n"
}

function fromrunningsystem_verifysecurebootkeysbackup { # from running system
    printf "If you just saved the secureboot keys from uefi, you may want to generate some checksums and to verify them against a copy of the keys\n"
}

function fromrunningsystem_enablesecurebootsetupmode { # from uefi
    printf "ONLY IF the secureboot keys are saved you can enable secureboot setup mode now. You will be able to enable it later otherwise\n"
}

function fromrunningsystem_boottemporarysystem {
    printf "You may now boot from the liveusb key.\n"
}

function fromrunningsystem_setupinstallenvironmenttemporarysystem {
    printf "git init --bare\n"
    printf "git clone or git init\n"
    printf "git remote add\n"
    printf "git push\n"
    printf "open Settings, go to Region & Language, set the language and keyboard you want\n"
    printf "open Terminal\n"
    printf "start tmux, git clone, cd to git dir, create new tmux window, cd to git dir, gedit gentoo-garden-installation-script.sh\n"
    printf "first tmux window will be used for git commands and running the script\n"
    printf "create new Terminal tab, su -, start tmux\n"
    # close the fedora installation window
    # open terminal

    # su -

    # encountering a weird bug which affects mooltipy emulated keyboard
    # not all characters get displayed
    # on top of that I haven't been able to make it display any capital
    # applications not relaying on gnome/display maybe even gtk, seem to be fine
    # so we install an other terminal emulator application for now
    # make sure internet is reachable
    # dnf install mate-terminal

    # close gnome terminal
    # open mate terminal

    # first tmux window will be used to run the script
    # start tmux
    # clone git gentoo-install-script repo
    # cd to repo dir

    # second tmux window will be used to run git commands
    # create new tmux window
    # change to new window
    # cd to repo dir

    # third tmux window will be used to edit the script
    # create new tmux window
    # change to new window
    # cd to repo dir
    # gedit script.sh

    # switch to second tmux window
    # git config --global user.email "dicebox@suisjelibre.net"
    # git config --global user.name "dicebox"

    # switch to first tmux window

    # start a root session, should not be needed, but if it does, it will be already here
    # mate-terminal --tab -- su -c /usr/bin/tmux -
    # gnome-terminal --tab -- su -c /usr/bin/tmux -

    # the dev process is (the first edit and set of git commands are optional):
    # edit the script
    # git diff
    # git add
    # git commit -m ""
    # git push
    # git log
    # run the script

    # dnf install emacs adobe-source-code-pro-fonts
    # git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
    # dotspacemacs
    # better-defaults
    # auto-completion?
    # git
    # shell-scripts
}

function fromrunningsystem_setupsshaccess {
    printf "You can enable a remote installation through ssh\n"
    printf "Enter remote user name\n"
    read remoteuser
    printf "Enter remote IP\n"
    read remoteip
    ssh-copy-id -i .ssh/remoteinstall-"${targethostname}".pub "${remoteuser}"@"${remoteip}"
}

### NEWINFRASTRUCTURE_FROMTEMPORARYSYSTEM ###
function fromtemporarysystem_setupdevenv {
    printf "open gnome terminal\n"
    printf "dnf install mate-terminal emacs adobe-source-code-pro-fonts\n"
    printf "git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d\n"
    printf "open mate terminal\n"
    printf "start tmux:\n"
    printf "new window \"script\"; cd ~/gentoo-installation-script; ./gentoo-garden-installation-script.sh\n"
    printf "close gnome terminal\n"
    printf "new window \"git\"; cd ~/gentoo-installation-script; git status\n"
    printf "new window \"shell\"; mate-terminal --tab -- su -c /usr/bin/tmux -\n"
    printf "start emacs:\n"
    printf "edit .spacemacs: \n"
    printf "better-defaults auto-completion git shell-scripts\n"
    printf "restart emacs\n"
    printf "edit gentoo-installation-script/gentoo-garden-installation-script.sh\n"
    printf "\n"
}

function fromtemporarysystem_setupnetwork {
    printf "\n"
}

function fromtemporarysystem_setupsshaccess {
    printf "Use the next infos to setup the ssh access.\n"
    ip addr
    printf "\n"
    whoami
    printf "\n"
    printf "configure noroot access, and key only access\n"
    printf "PermitRootLogin no\n"
    printf "PasswordAuthentication no\n"
    printf "ChallengeResponseAuthentication no\n"
    printf "UsePAM no\n"
    printf "X11Forwarding no\n"
    printf "systemctl restart sshd.service\n"
    # check https://github.com/dev-sec/chef-ssh-hardening for more hardening
    # found from https://askubuntu.com/questions/346857/how-do-i-force-ssh-to-only-allow-users-with-a-key-to-log-in
    printf "\n"
}

function fromtemporarysystem_installmooltipass {
    #we want to access the passwords and the key
    #especially needed to access the keys
    #SUDO
    #NETWORK
    printf "Make sure that internet is reachable, then press Enter\n"
    read
    # We are cloning in a git repository. We should avoid that but for now lets say that's not important
    git clone https://github.com/mooltipass/mooltipass-udev.git
    sudo cp -v mooltipass-udev/udev/69-mooltipass.rules /etc/udev/rules.d/
    # We need to test pip path/name
    #pip3 install --user mooltipy
    #pip3 install --user git+https://github.com/oSquat/mooltipy
    # unmerged python3 support
    #pip3 install --user git+https://github.com/bobsaintcool/mooltipy
    #pip3 install --user git+file:///run/media/$(whoami)/Gentoo\ Install/leaf/mooltipy.git/
    pip3 install --user git+https://github.com/dicebox/mooltipy

    printf "\n"
    printf "\n"
    printf "You can disconnect the network for now\n"
    printf "\n"
}

function fromtemporarysystem_settargethostname {
    printf "Type the hostname of the machine you will install\n"
    read targethostname
    # TODO: get the next 3 variables update out of here
    btrfssubvol_part="/dev/${targethostname}-vg/lvbtrfssubvolumes"
    pvname="${targethostname}""-pv"
    vgname="${targethostname}""-vg"
    printf "\n"
}

function fromtemporarysystem_generatepassphrases {
    #  temporary system root. do it from temporary system?
    #  temporary system user. do it from temporary system?
    #  luks gpg key. do it from temporary system, then reencrypt it when in target system?
    #  root login. do it from temporary system then change it again when in target system?
    #  user login. do it from temporary system then change it again when in target system?
    #  #selinux admin. do it from target system
    # NOTE: use us-altgr-intl keymap, as us-intl isn't compatible with the us keymap
    # NOTE/TODO: backup-sig is more like a domain wide cedential than host wide, should we create a domain set in the mooltipass? seems interesting
    printf "Plug your mooltipass now and insert your smartcard in it. Press Enter when done\n"
    read
    mpusers="tmproot tmpuser luksboot root leaf backup-sig backup-crypt"
    for mpuser in ${mpusers}; do
        mooltipy login set "${targethostname}" -u ${mpuser}
    done
}

function fromtemporarysystem_changepasswords {
    printf "Change liveusb user password (use your mootlipass)\n"
    passwd
    printf "Change liveusb root password (use your mootlipass)\n"
    su -c "passwd"
}

function fromtemporarysystem_downloadgentooinstallationfiles {
    #wget -r -np -nd -l1  "${gentoobaseurl}${gentoostage3}" -A "stage3-amd64-hardened+nomultilib-*.tar.xz*"
    wget -r -np -nd -l1  "${gentoobaseurl}${gentoostage3}" -A "stage3-amd64-hardened-selinux+nomultilib-*.tar.xz*"
    # NOTE: use this next one for hardened selinux nomultilib stage3
    # NOTE: installing from fedora which has SELinux support built-in, with a SELinux stage3
    # means that we need to disable Portage's SELinux support until the system is ready
    # to enable it. See
    # https://wiki.gentoo.org/wiki/SELinux/Installation#Installing_policies_and_utilities.2C_part_one
    # thanks to perfinion@gentoo-hardened.freenode.net for mentioning it
    # So we need to modify the make.conf to add FEATURES="-selinux"
    # we will certainly need to run the system in permissive mode for some time too
    #wget -r -np -nd -l1  "${gentoobaseurl}${gentoostage3}" -A "stage3-amd64-hardened-selinux+nomultilib-*.tar.xz*"
    wget -r -np -nd -l1 "${gentoobaseurl}${gentoosnapshot}" -A "portage-latest.tar.xz*"
}

function fromtemporarysystem_receivegentookeys {
    printf "Retrieve key to verify stage3. Press Enter to proceed\n"
    #read
    gpg2 --verbose --keyserver "${keyserver}" --recv-keys "${keystage3}"

    printf "Retrieve key to verify snapshot. Press Enter to proceed\n"
    #read
    gpg2 --verbose --keyserver "${keyserver}" --recv-keys "${keysnapshot}"
}

function fromtemporarysystem_verifygentooinstallationfilesdownloads {
    printf "Verify stage3. Press Enter to proceed\n"
    #read
    gpg2 --verify ${sigstage3}
    sha512sum -c ${sigstage3}

    printf "Verify snapshot. Press Enter to proceed\n"
    #read
    gpg2 --verify "${sigsnapshot}" portage-latest.tar.xz
    md5sum -c portage-latest.tar.xz.md5sum
}

function fromtemporarysystem_selectusbbootkey {
    while [ "${bootkey}" == "" ]; do
    # We show the full path of each symbolic link and their corresponding target
    udisksctl status
    printf "Type the full path (/dev/sdX, not /dev/disk/by-id/ID) of the usb boot key. Or press Enter to refresh the list of storage medium\n"
    read bootkey
done

}

function fromtemporarysystem_createtableusbbootkey {
    sudo parted "${bootkey}" print
    printf "Abort now if unsure, otherwise press Enter\n"
    read
    # We are doing gruikkk here, to avoid to have too much code with tests everywhere
    # This will generate errors, which we could be able to filter out, when we will care about it
    udisksctl unmount -b "${bootkey}"?
    sudo swapoff "${bootkey}"?
    sudo cryptsetup luksErase "${bootkey}"?
    printf "If the all filesystems of the drive have been unmounted correctly press Enter.\n"
    read
    sudo parted "${bootkey}" mklabel gpt
}

function fromtemporarysystem_setpartitionsizeusbbootkey {
    printf "\n"
}

function fromtemporarysystem_partitionusbbootkey {
    # It will store: luks encrypted key file, kernel + initrd(embedded into the kernel), signed checksums of previous files
    # Obvious remaining threats: breakable encryption, breakable signage
    # We need to change to post-quantum crypto ASAP(which could still takes months to years)
    # We can assume that it is safe enough to use this usb boot key, and its copies, as regular user usb keys
    # We can reserve only 1GB for the boot partition
    # Remaining space can be a luks physical volume for lvm
    # There are better alternative to vfat partitions to transfer data to other systems: local/ad-hoc wired/wireless networks
    # Transfer speed could be slower through the network though
    #sudo parted -a optimal "${bootkey}" mkpart "EFI System Partition" 0% 1GiB
    # NOTE: LiveUSB System Recovery does not exist yet
    # but it would be used to repair or reinstall the system
    # It could be used to mess with hardware by embedding memtest or UBCD too
    # NOTE: we add underscores in partitions's names as it seems as of february 24th 2020
    # parted do not take into account simple or double quotes to delimit the name string
    # NOTE: minimum btrfs partition size is 109MiB, or 189MiB with DUP
    # so let's use 128MiB or 192MiB
    # or is it 229MiB min? let's use 256MiB
    printf "Untested function, press enter to continue.\n"
    read
    sudo parted -a optimal "${bootkey}" mkpart "EFI_System_Partition" 1MiB 1025MiB
    sudo parted "${bootkey}" set 1 boot on
    sudo parted -a optimal "${bootkey}" mkpart "LUKS_Boot" 1025MiB 1281MiB
    sudo parted -a optimal "${bootkey}" mkpart "LiveUSB_System_Recovery" 1281MiB 100%
}

function fromtemporarysystem_formatusbbootkey {
    printf "Untested function, press enter to continue.\n"
    read
    sudo mkfs.vfat -F32 -n "UEFI-BOOTFS" "${bootkey}"1
    sudo cryptsetup luksFormat "${bootkey}"2
    sudo cryptsetup luksOpen "${bootkey}"2 luks-bootkey
    # NOTE: raid1c2 for now, will move to raid1c3 (>= kernel 5.5) later
    # actually, DUP is not backup as stated in mkfs.btrfs manpage, so let's not rely on it
    sudo mkfs.btrfs -f -m dup -d dup -L "Main LUKS Keyfile" /dev/mapper/luks-bootkey
    sudo cryptsetup luksClose /dev/mapper/luks-bootkey
    # NOTE: raid1c2? Maybe not, for LUKS keyfile it makes sense as we do not lose much space,
    # but here we would lose much more storage space
    sudo mkfs.btrfs -f -L "LiveUSB System Recovery" "${bootkey}"3
}

function fromtemporarysystem_mountusbbootkey {
    # use mktemp -d instead
    sudo mkdir -v /tmp/efiboot
    sudo mount -v -t vfat "${bootkey}"1 /tmp/efiboot
}

function fromtemporarysystem_unmountusbbootkey {
    sudo umount -v /tmp/efiboot
    sudo rmdir -v /tmp/efiboot
}

function fromtemporarysystem_storemainlukskey {
    # NOTE: to be compatible with dracut+systemd,
    # we should instead have the unencrypted keyfile in a luks volume
    # which would be on the usb key
    # so the usb key would be: usbkey-part1(EFI:1-2GiB), usbkey-part2(LUKS:10MIB), usbkey-part3(ext4: remaining space)
    # considering  mess could be easily done in part1, we want to use this key only on trusted
    # machines. So part3, could be used to hold tools needed to repair/reinstall such system.
    #
    # 8MB of random data, from which is derived the luks key
    # Is sudo, in sudo dd really needed?
    # We add --pinentry-mode loopback, otherwise gpg throws us an error:
    # gpg: problem with the agent: Inappropriate ioctl for device
    # gpg: error creating passphrase: Operation cancelled
    # gpg: symmetric encryption of '[stdin]' failed: Operation cancelled
    # Guided by https://stackoverflow.com/questions/51504367/gpg-agent-forwarding-inappropriate-ioctl-for-device
    #sudo dd if=/dev/urandom bs=8388607 count=1 | sudo gpg --pinentry-mode loopback --symmetric --cipher-algo AES256 --output /tmp/efiboot/"${targethostname}"-luks-key.gpg
    luksmountdir="$(sudo mktemp -d)"
    sudo cryptsetup luksOpen "${bootkey}"2 luks-bootkey
    sudo mount -v /dev/mapper/luks-bootkey "${luksmountdir}"
    # NOTE/TODO: umask is not applied this way, we need to change that, see menubackup for luks
    #sudo umask 0377
    sudo sh -c "umask 0377; dd if=/dev/urandom of="${luksmountdir}"/"${targethostname}"-luks-key.bin bs=8388607 count=1"
    sudo ls -l "${luksmountdir}"/"${targethostname}"-luks-key.bin
    #sudo umask 0022
    sudo umount -v /dev/mapper/luks-bootkey
    sudo cryptsetup luksClose /dev/mapper/luks-bootkey
    sudo rmdir -v "${luksmountdir}"
    # the resulting file is too big to be stored on the mooltipass
    # NOTE: reencrypt the keyfile
    # sudo cp /tmp/efiboot/"${targethostname}"-luks-key.gpg /tmp/efiboot/"${targethostname}"-luks-key.gpg.backup
    # read oldpassword && sudo gpg --decrypt --passphrase "${oldpassword}" /tmp/efiboot/"${targethostname}"-luks-key.gpg | sudo gpg --pinentry-mode loopback --symmetric --cipher-algo AES256 --output /tmp/efiboot/"${targethostname}"-luks-key.gpg.new
    #
    # sudo gpg --decrypt --output /tmp/efiboot/centranthus-luks-key /tmp/efiboot/centranthus-luks-key.gpg
    # sudo gpg --pinentry-mode loopback --symmetric --cipher-algo AES256 --output /tmp/efiboot/centranthus-luks-key.gpg.newnew /tmp/efiboot/centranthus-luks-key
}

function fromtemporarysystem_selectmainstorage {
    while [ "${mainstorage}" == "" ]; do
    # We show the full path of each symbolic link and their corresponding target
    udisksctl status
    printf "Type the full path (/dev/sdX or /dev/disk/by-id/ID) of the live usb media. Or press Enter to refresh the list of storage medium\n"
    read mainstorage
done

}

function fromtemporarysystem_preparemainstorageforluks {
    sudo parted "${mainstorage}" print
    printf "Abort now if unsure, otherwise press Enter\n"
    read
    #read
    # We are doing gruikkk here, to avoid to have too much code with tests everywhere
    # This will generate errors, which we could be able to filter out, when we will care about it
    udisksctl unmount -b "${mainstorage}"?
    sudo swapoff "${mainstorage}"?
    sudo cryptsetup luksErase "${mainstorage}"?
    printf "If the all filesystems of the drive have been unmounted correctly press Enter.\n"
    read
    # write random data on it
    #sudo dd if=/dev/urandom of="${mainstorage}"
}

function fromtemporarysystem_createmainluks {
    #sudo gpg --decrypt /tmp/efiboot/"${targethostname}"-luks-key.gpg | sudo cryptsetup --key-file - luksFormat "${mainstorage}"
    luksmountdir="$(sudo mktemp -d)"
    sudo cryptsetup luksOpen "${bootkey}"2 luks-bootkey
    sudo mount -v /dev/mapper/luks-bootkey "${luksmountdir}"
    sudo cryptsetup --key-file "${luksmountdir}"/"${targethostname}"-luks-key.bin luksFormat "${mainstorage}"
    sudo cryptsetup luksDump "${mainstorage}"
    sudo umount -v /dev/mapper/luks-bootkey
    sudo cryptsetup luksClose /dev/mapper/luks-bootkey
    sudo rmdir -v "${luksmountdir}"
}

function fromtemporarysystem_backupmainluksheader {
    sudo cryptsetup luksHeaderBackup "${mainstorage}" --header-backup-file /tmp/efiboot/"${targethostname}"-luks-header.img
}

function fromtemporarysystem_verifyluksheaderbackup {
    printf "\n"
    # generate checksum and store it
    # backup header again, checksum it and compare the checksums
}

function fromtemporarysystem_unlockmainluks {
    #sudo mount "${bootkey}"1 /tmp/efiboot/
    #export GPG_TTY=$(tty)
    #printf "targethostname: ${targethostname}\n"
    #printf "mainstorage: ${mainstorage}\n"
    #printf "pvname: "${targethostname}"-pv\n"
    #sudo gpg --decrypt /tmp/efiboot/"${targethostname}"-luks-key.gpg | sudo cryptsetup --key-file - luksOpen "${mainstorage}" "${targethostname}"-pv
    #sleep 1
    #sudo umount /tmp/efiboot/
    luksmountdir="$(sudo mktemp -d)"
    sudo cryptsetup luksOpen "${bootkey}"2 luks-bootkey
    sudo mount -v /dev/mapper/luks-bootkey "${luksmountdir}"
    sudo cryptsetup --key-file "${luksmountdir}"/"${targethostname}"-luks-key.bin luksOpen "${mainstorage}" "${targethostname}"-pv
    sudo umount -v /dev/mapper/luks-bootkey
    sudo cryptsetup luksClose /dev/mapper/luks-bootkey
    sudo rmdir -v "${luksmountdir}"
    }

function fromtemporarysystem_closeluks {
    sudo cryptsetup luksClose "${targethostname}"-pv
    printf "\n"
}

function fromtemporarysystem_status_dm-event.service {
    sudo systemctl status dm-event.service
}

function fromtemporarysystem_status_lvm2-monitor.service {
    sudo systemctl status lvm2-monitor.service
}

function fromtemporarysystem_configurehostlvm.conf {
    # NOTE: let's keep the presentation of the document by using tabulations
    # Enable thin pool autoextend
    sudo sed -i "s/^\tthin_pool_autoextend_threshold = 100$/\tthin_pool_autoextend_threshold = 70/" /etc/lvm/lvm.conf
    sudo sed -i "s/^\t# thin_pool_discards = \"passdown\"$/\tthin_pool_discards = \"nopassdown\"/" /etc/lvm/lvm.conf
    sudo sed -i "s/^\t# thin_pool_zero = 1$/\tthin_pool_zero = 0/" /etc/lvm/lvm.conf
}

function fromtemporarysystem_createpv {
    sudo pvcreate --verbose /dev/mapper/"${targethostname}"-pv
}

function fromtemporarysystem_removepv {
    sudo pvremove --verbose /dev/mapper/"${targethostname}"-pv
}

function fromtemporarysystem_createvg {
    #vgname="vg${hostname}"
    sudo vgcreate --verbose "${targethostname}"-vg /dev/mapper/"${targethostname}"-pv
}

function fromtemporarysystem_removevg {
    sudo vgremove --verbose "${targethostname}"-vg
}

function fromtemporarysystem_deactivatevg {
    sudo vgchange -an "${targethostname}"-vg
    printf "\n"
}

function fromtemporarysystem_setthinpoolsize {
    sudo vgdisplay
    printf "Enter the desired size of the thin pool, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read thinpoolsize
}

function fromtemporarysystem_setthinpoolchunksize {
    sudo vgdisplay
    printf "Enter thin pool chunk size (2MiB):\n"
    read thinpoolchunksize
}

function fromtemporarysystem_createthinpool {
    # NOTE: is this the recommended way to create one?
    sudo lvcreate --type thin-pool --size "${thinpoolsize}" --chunksize "${thinpoolchunksize}" -n "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setswapsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createswap {
    sudo lvcreate --virtualsize "${lvsize}" --name lvswap --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setrootsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createroot {
    sudo lvcreate --virtualsize "${lvsize}" --name lvroot --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_sethomesize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createhome {
    sudo lvcreate --virtualsize "${lvsize}" --name lvhome --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setoptsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createopt {
    sudo lvcreate --virtualsize "${lvsize}" --name lvopt --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setsrvsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createsrv {
    sudo lvcreate --virtualsize "${lvsize}" --name lvsrv --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setusrsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createusr {
    sudo lvcreate --virtualsize "${lvsize}" --name lvusr --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setvarsize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createvar {
    sudo lvcreate --virtualsize "${lvsize}" --name lvvar --thinpool "${targethostname}"-tp "${targethostname}"-vg
}

function fromtemporarysystem_setbtrfssubvolumessize {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize
}

function fromtemporarysystem_createbtrfs {
    sudo lvcreate --size "${lvsize}" --name lvbtrfssubvolumes "${targethostname}"-vg
}

function fromtemporarysystem_setext4size {
    sudo vgdisplay
    printf "Enter the desired size of the LV, you have to precise the unit, you can use LVM suffixes like %%FREE and %%PVS\n"
    read lvsize

}

function fromtemporarysystem_createext4 {
    sudo lvcreate --size "${lvsize}" --name lvext4 "${targethostname}"-vg
} # databases, virtual machines, swap file for hibernation

function fromtemporarysystem_backuplvmheader {
    sudo vgcfgbackup "${targethostname}"-vg -f "${targethostname}"-vgcfgbackup
    # checksum
}

function fromtemporarysystem_verifylvmheaderbackup {
    printf "\n"
    # backup again and compare both checksums
}

function fromtemporarysystem_formatswap {
    sudo mkswap -L "main_swap"  /dev/"${targethostname}"-vg/lvswap
}

function fromtemporarysystem_formatroot {
    sudo mkfs.ext4 -L "${targethostname}_root" /dev/"${targethostname}"-vg/lvroot
}

function fromtemporarysystem_formathome {
    sudo mkfs.ext4 -L "${targethostname}_home" /dev/"${targethostname}"-vg/lvhome
}

function fromtemporarysystem_formatopt {
    sudo mkfs.ext4 -L "${targethostname}_opt" /dev/"${targethostname}"-vg/lvopt
}

function fromtemporarysystem_formatsrv {
    sudo mkfs.ext4 -L "${targethostname}_srv" /dev/"${targethostname}"-vg/lvsrv
}

function fromtemporarysystem_formatusr {
    sudo mkfs.ext4 -L "${targethostname}_usr" /dev/"${targethostname}"-vg/lvusr
}

function fromtemporarysystem_formatvar {
    sudo mkfs.ext4 -L "${targethostname}_var" /dev/"${targethostname}"-vg/lvvar
}

function fromtemporarysystem_formatbtrfs {
    sudo mkfs.btrfs -L "${targethostname}_btrfssubvolumes" "${btrfssubvol_part}"
}

function fromtemporarysystem_createbtrfssubvolumes {
    sudo mkdir -v ${mount_subvolumes_path}
    sudo mount ${btrfssubvol_part} ${mount_subvolumes_path}

    echo ${subvolumes}
    for s in ${subvolumes}; do
        sudo btrfs subvolume create ${mount_subvolumes_path}/"${s}"
    done
    sudo btrfs subvolume list -a ${mount_subvolumes_path}

    sudo umount ${mount_subvolumes_path}
    sudo rmdir ${mount_subvolumes_path}
}

function fromtemporarysystem_create_root_mountpoints_btrfs {
    sudo mkdir -v ${mount_root_path}
    sudo mount -v -o subvol=/rootdir ${btrfssubvol_part} ${mount_root_path}

    #sudo mkdir -v --parents ${mount_root_path}/efi
    # we mount our ESP to /boot instead
    # TODO: mode the creation of /srv/db-vm-swapfile and /boot somewhere after
    # unpacking stage3. but why?
    sudo mkdir -v --parents ${mount_root_path}/srv/db-vm-swapfile
    sudo mkdir -v --parents ${mount_root_path}/boot
    for d in ${rootdirs}; do
        sudo mkdir -v ${mount_root_path}/${d}
    done

    sudo mount -v -o subvol=/usr ${btrfssubvol_part} ${mount_root_path}/usr
    for d in ${usrdirs}; do
        sudo mkdir -v ${mount_root_path}/usr/${d}
    done
    sudo umount -v ${mount_root_path}/usr

    sudo mount -v -o subvol=/var ${btrfssubvol_part} ${mount_root_path}/var
    for d in ${vardirs}; do
        sudo mkdir -v --parents ${mount_root_path}/var/${d}
    done
    sudo umount -v ${mount_root_path}/var

    sudo umount -v ${mount_root_path}
}

function fromtemporarysystem_create_root_mountpoints {
    sudo mkdir -v ${mount_root_path}
    #sudo mount -v -o subvol=/rootdir ${btrfssubvol_part} ${mount_root_path}
    sudo mount -v /dev/"${targethostname}"-vg/lvroot ${mount_root_path}

    #sudo mkdir -v --parents ${mount_root_path}/efi
    # we mount our ESP to /boot instead
    # TODO: move the creation of /srv/db-vm-swapfile and /boot somewhere after
    # unpacking stage3. but why?
    #sudo mkdir -v --parents ${mount_root_path}/srv/db-vm-swapfile
    sudo mkdir -v --parents ${mount_root_path}/boot
    sudo mkdir -v --parents ${mount_root_path}/home
    sudo mkdir -v --parents ${mount_root_path}/opt
    sudo mkdir -v --parents ${mount_root_path}/srv
    sudo mkdir -v --parents ${mount_root_path}/usr
    sudo mkdir -v --parents ${mount_root_path}/var
    #for d in ${rootdirs}; do
    #    sudo mkdir -v ${mount_root_path}/${d}
    #done

    #sudo mount -v -o subvol=/usr ${btrfssubvol_part} ${mount_root_path}/usr
    #for d in ${usrdirs}; do
    #    sudo mkdir -v ${mount_root_path}/usr/${d}
    #done
    #sudo umount -v ${mount_root_path}/usr

    #sudo mount -v -o subvol=/var ${btrfssubvol_part} ${mount_root_path}/var
    #for d in ${vardirs}; do
    #    sudo mkdir -v --parents ${mount_root_path}/var/${d}
    #done
    #sudo umount -v ${mount_root_path}/var

    sudo umount -v ${mount_root_path}
}

function fromtemporarysystem_clean_root_mountpoints {
    printf "\n"
}

function fromtemporarysystem_clean_subvolumes {
    sudo mkdir -v ${mount_subvolumes_path}
    sudo mount ${btrfssubvol_part} ${mount_subvolumes_path}

    for s in ${subvolumes}; do
        sudo btrfs subvolume delete ${mount_subvolumes_path}/"${s}"
    done
    sudo btrfs subvolume list -a ${mount_subvolumes_path}

    sudo umount ${mount_subvolumes_path}
    sudo rmdir ${mount_subvolumes_path}
}

function fromtemporarysystem_formatext4 {
    sudo mkfs.ext4 -L "${targethostname}_ext4" /dev/"${targethostname}"-vg/lvext4
} # databases, virtual machines, swap file for hibernation

function fromtemporarysystem_activateswap {
    sudo swapon --verbose /dev/"${targethostname}"-vg/lvswap
}

function fromtemporarysystem_deactivateswap {
    sudo swapoff --verbose /dev/"${targethostname}"-vg/lvswap
}

function fromtemporarysystem_mountbtrfssubvolumes {
    sudo mkdir -v ${mount_root_path}
    sudo mount -v -o subvol=/rootdir ${btrfssubvol_part} ${mount_root_path}
    for d in ${rootdirs}; do
        sudo mount -v -o subvol=/"${d}" ${btrfssubvol_part} ${mount_root_path}/"${d}"
    done
    for d in ${usrdirs}; do
        sudo mount -v -o subvol=/usr"${d}" ${btrfssubvol_part} ${mount_root_path}/usr/"${d}"
    done
    for d in ${vardirs}; do
        # NOTE: for the subvol we remove the prefix cache/ to just keep the last directory
        sudo mount -v -o subvol=/var"${d#cache/}" ${btrfssubvol_part} ${mount_root_path}/var/"${d}"
    done
}

function fromtemporarysystem_umountbtrfssubvolumes {
    #sudo umount -v ${mount_root_path}/usr/portage/packages
    for d in ${usrdirs}; do
        sudo umount -v ${mount_root_path}/usr/"${d}"
    done
    for d in ${vardirs}; do
        sudo umount -v ${mount_root_path}/var/"${d}"
    done
    #fromtemporarysystem_umountefi
    #fromtemporarysystem_umountext4
    for d in ${rootdirs}; do
        sudo umount -v ${mount_root_path}/"${d}"
    done
    sudo umount -v ${mount_root_path}
}

function fromtemporarysystem_mountlvmvolumes {
    sudo mkdir -v ${mount_root_path}
    #sudo mount -v -o subvol=/rootdir ${btrfssubvol_part} ${mount_root_path}
    #for d in ${rootdirs}; do
    #    sudo mount -v -o subvol=/"${d}" ${btrfssubvol_part} ${mount_root_path}/"${d}"
    #done
    #for d in ${usrdirs}; do
    #    sudo mount -v -o subvol=/usr"${d}" ${btrfssubvol_part} ${mount_root_path}/usr/"${d}"
    #done
    #for d in ${vardirs}; do
    #    # NOTE: for the subvol we remove the prefix cache/ to just keep the last directory
    #    sudo mount -v -o subvol=/var"${d#cache/}" ${btrfssubvol_part} ${mount_root_path}/var/"${d}"
    #done
    # TODO: we need to change the defaults mount options, for example we want nodev and other restrictions
    # NOTE: after mounting, we can observe a long period of write on /home, which has a
    # virtual size of 950GiB. The writing speed is around 15MiB/s on a SSD, which is really
    # low. Many questions about all that remain.
    # https://serverfault.com/questions/865836/lvm-logical-volumes-mapped-size-much-bigger-than-filesystem-used-disk-space
    # fstrim /mnt/gentoo/home doesn't change anything
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvroot ${mount_root_path}
    #sudo mount -v /dev/"${targethostname}"-vg/lvroot ${mount_root_path}
    sleep 1
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvhome ${mount_root_path}/home
    #sudo mount -v /dev/"${targethostname}"-vg/lvhome ${mount_root_path}/home
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvopt ${mount_root_path}/opt
    #sudo mount -v /dev/"${targethostname}"-vg/lvopt ${mount_root_path}/opt
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvsrv ${mount_root_path}/srv
    #sudo mount -v /dev/"${targethostname}"-vg/lvsrv ${mount_root_path}/srv
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvusr ${mount_root_path}/usr
    #sudo mount -v /dev/"${targethostname}"-vg/lvusr ${mount_root_path}/usr
    sudo mount -v -o "defaults,discard" /dev/"${targethostname}"-vg/lvvar ${mount_root_path}/var
    #sudo mount -v /dev/"${targethostname}"-vg/lvvar ${mount_root_path}/var
}

function fromtemporarysystem_unmountlvmvolumes {
    sudo umount -v ${mount_root_path}/home
    sudo umount -v ${mount_root_path}/opt
    sudo umount -v ${mount_root_path}/srv
    sudo umount -v ${mount_root_path}/usr
    sudo umount -v ${mount_root_path}/var
    sleep 1
    sudo umount -v ${mount_root_path}
}

function fromtemporarysystem_mountefi {
    # https://wiki.archlinux.org/index.php/systemd-boot#Installation
    sudo mount -v ${bootkey}1 ${mount_root_path}/boot
    printf "\n"
}

function fromtemporarysystem_umountefi {
    sudo umount ${mount_root_path}/boot
    printf "\n"
}

function fromtemporarysystem_mountext4 {
    sudo mkdir -v --parents ${mount_root_path}/srv/db-vm-swapfile
    sudo mount -v /dev/"${targethostname}"-vg/lvext4 ${mount_root_path}/srv/db-vm-swapfile
    printf "\n"
}

function fromtemporarysystem_umountext4 {
    sudo umount -v ${mount_root_path}/srv/db-vm-swapfile
    printf "\n"
}

function fromtemporarysystem_unpackstage3 {
    sudo tar xJpf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner --directory=${mount_root_path}
}

function fromtemporarysystem_cleanstage3 {
    printf "\n"
}

function fromtemporarysystem_copyusrlocaltoopt {
    sudo cp -a ${mount_root_path}/usr/local ${mount_root_path}/opt
}

function fromtemporarysystem_unpackportagesnapshot {
    sudo mkdir -v --parents ${mount_root_path}/var/db/repos
    sudo tar xJpf portage-latest.tar.xz --xattrs-include='*.*' --numeric-owner --directory=${mount_root_path}/var/db/repos
    sudo mv -v ${mount_root_path}/var/db/repos/portage ${mount_root_path}/var/db/repos/gentoo
}

function fromtemporarysystem_cleanportagesnapshot {
    sudo rm -RI ${mount_root_path}/usr/portage/*
    printf "\n"
}

# NOTE/TODO:
# mkdir /mnt/gentoo/srv/backup/{bup,dar,dar.tmp,snapshot.mnt,archive.mnt,forbind.mnt}
# bup: bup repositories
# dar: dar catalogue, anything else?
# dar.tmp: dar tmp files, like slices, checksums, signatures, can be used as a mount point
# readonly.mnt: mount point for system snapshot, or data snapshot or archive snapshot
# for system and data, we mount the snapshots, as the origins are in use, or supposedly
# in use. Mount them read-only. delete the snapshot after unmounting it if they are not needed
# Used for bup backups, and bup archive pull.
# readwrite.mnt: mount point for archive. Is this needed?
# for now it would be needed to "merge" the local bup repositories with the archive
# bup repository, but this could still used a snapshot of the archive, and then
# merge the 2 lvm volumes, the archive lv and the archive snapshot lv. Just pay attention to
# not write on both the origin and snapshot. Let's just use snapshots the classical way,
# they are a past state of the volume, so let's make one before writing, keep it long enough
# so we can make sure everything went right, then delete the snapshot.
# Used for bup archive push.
# forbind.mnt: mount read-only the snapshot of /srv, then bind mount read-only the expected
# directory residing on it in readonly.mnt. Why would we want that? we can cancel any path
# with the --fs-root option of dar. And we could mount the origin instead of the snapshot, so
# we can revert back to the snapshot if needed, like its intended use.

# NOTE: archive of old archives (cleanme.bup)
# menubackup_createbackupusergpgkeys
# menubackup_installdarhelper
# #snapshot of /srv
# # in menu menubackup_menusystemdatasnapshotspre
# menubackup_createdatasnapshot
# menubackup_activatedatasnapshot
# menubackup_mountdatasnapshot
# #mount (readonly) snapshot in forbind.mnt
# #mount (readonly) forbind.mnt/backup/cleanme.bup in readonly.mnt
# mount (readonly) snapshot in readonly.mnt
# # lvdar is located on the 5Tb drive for now
# # mount lvdar in /srv/backup/dar.tmp
# mount /dev/mapper/vggarden-lvdar /mnt/gentoo/srv/backup/dar.tmp
# create dar-helper.sh config
# edit dar-helper.sh config (defaults should cover our use case, shouldn't they?)
# NOTE/TODO: dar needs gcrypt for --hash
# TODO: generate mooltipass backup-crypt password for the --key option
# mount_root_path=/mnt/gentoo
# update dar-helper script inside chroot with menubackup_installdarhelper
# sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --bind=/dev/sr0 \
#         dar --create /srv/backup/dar.tmp/system_bup --fs-root /srv/backup/readonly.mnt/backup/ --go-into cleanme.bup --slice 34G --hash sha512 --key : --execute "/opt/bin/dar-helper-launcher.sh %p %b %n %N %e %c %u"
# sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --bind=/dev/sr0 \
#         sudo -i dar --create /srv/backup/dar.tmp/system_bup --fs-root /srv/backup/readonly.mnt/backup/ --go-into cleanme.bup --slice 35G --hash sha512 --key : --execute "sudo -i -u root-backups /opt/bin/dar-helper-launcher.sh %p %b %n %N %e %c %u"
# sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
#         sudo -i dar --create /srv/backup/dar.tmp/system_bup --fs-root /srv/backup/readonly.mnt/backup/ --go-into cleanme.bup --slice 700M --hash sha512 --key : --execute "sudo -i -u root-backups /opt/bin/dar-helper-launcher.sh %p %b %n %N %e %c %u"
# create local config
# edit gpguser and copy root backup identity/email

# NOTE: explanation about 34G dar slices
# dvdisaster RS03 is less compute intensive when using more than 30% redundancy
# also, dvdisaster RS03 has a standard size of 48439848960 bytes (should be bytes right?)
# 48439848960 / 1.30 = 37261422276.9231 , which should be the maximum available data space
# if we wand to limit CPU load
# this is about 34.7024 GiB, so taking into account the iso overhead and the other files,
# other than the dar slice, this gives us a dar slice size of 34G

# TODO: look into dvdisaster code to check for RS03 augmented images standard sizes
# why is it 48439848960 instead of 50050629632 (no ssa) or 49782194176 (default 256MiB ssa)

# TODO:
# look at the manpage of mkisofs and check if the options used for genisoimage are compatible
# and optimal

# NOTE/TODO:
# for some reasons we cannot su to root from root-backups
# for now, let's just run dar-helper as root, we will see later to reduce privileges

# TODO: set root password, so we can su from root-backups
# then generate sig for the slice checksum
# then follow dar-helper menu:
# createiso
# augmentiso
# burndiskdryrun
# burndisk
# dvdisasterscan
# dvdisasterextractimage
# asktesttype
# askimagetype
# dvdisastertestimage
# printdisklabel
# cleanslicechecksumsig

# NOTE/TODO: the secure-path use flag of sudo is a mess
# it edits files /etc without creating a backup
# it breaks a gentoo chroot/container in a way so only sudo works through systemd-nspawn from a fedora 31 host
# shell started with systemd-nspawn works fine though
# and there are no documentation, and the ebuild is not commented enough so we have to work far too hard to understand what's happening
# guess how much I like it? and what I think about people involved with this patch. Actually, I would like to hear from them what was going on here
# well, I'll have to check if the culprit is really sudo and this use flag

# TODO: select what /dev/sr* to add in the chroot
# NOTE: fedora and gentoo have opposed groups numbers for cdrom and floppy, as both are gid 11
# NOTE: we can --bind /dev/sr0 to access it from inside the container/chroot
# NOTE: inside the chroot, we have to give access to root-backups to /dev/sr0,
# one user may already have access to it, check with getfacl,
# this is because of UID collision between fedora users and chroot users

function menubackup_ {
    printf "\n"
}

function menubackup_selectbupversion {
    equery keywords app-backup/bup
}

function menubackup_selectdarversion {
    equery keywords app-backup/dar
}

function menubackup_installtools {
    # NOTE: install latest stable versions available even even if they are masked
    # NOTE/TODO: have a menu to select which version to install
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge app-backup/bup =app-backup/dar-2.6.5 app-cdr/dvdisaster app-cdr/cdrtools app-cdr/dvd+rw-tools
}

function menubackup_createrootgituser {
    # https://forums.gentoo.org/viewtopic-t-1064076-start-0.html
    # NOTE: we create a dedicated user for root commits
    # this way we can keep the root account locked
    # and we can have a config file for git
    # NOTE: do we want to use our main offline user here?
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         useradd -m root-git

}

function menubackup_gitconfigrootgit {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "git config --global user.email \"root-git@localhost.localdomain\"" root-git
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "git config --global user.name \"root-git\"" root-git
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "git config --list" root-git
}

function menubackup_gitconfigenableinclude {
    # NOTE: then let's use
    # https://stackoverflow.com/questions/54842380/how-do-i-use-my-personal-git-config-when-committing-as-root
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         git -c "include.path=/home/root-git/.gitconfig" config --list --show-origin --includes
}

function menubackup_backupmooltipass {
    printf "\n"
    # NOTE: in moolticute git repo
    # dicebox | limpkin: do CPZ stand for Code Protected Zone and CTR for Counter, the AES CTR mode?
    # limpkin | correct
    # limpkin | cpz is code protected zone on the smartcard
    # limpkin | can be read, can't be written
    # MPDevice::exportDatabase and MPDevice::importDatabase
    # from MPDevice::exportDatabase
    # memMgmtModeReadFlash
    # generateExportFileData
    # moolticute export decryption from python:
    # https://github.com/mooltipass/minible/tree/master/scripts/smc_decode
    # https://github.com/mooltipass/minible/blob/master/scripts/smc_decode/smc_decode_win.py#L187
    # for now let's just dump the db with mooltipy, can we do that in an encrypted "stream"/file?
    date
    ~/.local/bin/mooltipy login list -D
    printf "print this information, store it in an encrypted file, or take a picture with a dedicated (not a smartphone) camera.\n"
}

function menubackup_createbaremainluksgitrepo {
    # NOTE: is luksmountdir restricted enough? read only for root?
    # NOTE:
    # does not work: sudo -c "umask 0077; touch /tmp/test-umask/testfile"
    # works: su -l -c "umask 0077; touch /tmp/test-umask/testfile"
    # works: sudo sh -c "umask 0077; touch /tmp/test-umask/testfile"
    # NOTE: su vs sudo
    # on small/personal networks with one admin, use either ones
    # on bigger networks, with more than one admin, use sudo
    # so let's use sudo by default? and have a regular user account and a sudoered user
    # account?
    # NOTE:
    # https://unix.stackexchange.com/questions/8581/which-is-the-safest-way-to-get-root-privileges-sudo-su-or-login
    # http://www.opsreportcard.com/section/32
    #sudo mkdir -v -p -m 0700 ${mount_root_path}/srv/git/luks-${targethostname}.git
    sudo mkdir -v -p -m 0700 ${mount_root_path}/srv/git/system-luks.git
    #sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${mount_root_path}/srv/git/luks-${targethostname}.git init --bare
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${mount_root_path}/srv/git/system-luks.git init --bare
}

function menubackup_createlukstmpdir {
    luksgitdir="$(sudo mktemp -d)"
    sudo chmod -v 0700 ${luksgitdir}
}

function menubackup_createmainluksworkdirgitrepo {
    # NOTE: is luksmountdir restricted enough? read only for root?
    #gitdir="$(sudo mktemp -d)"
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} init
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} annex init
    # NOTE: can we use origin as shortname here?
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} remote add srv ${mount_root_path}/srv/git/system-luks.git
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} checkout --orphan "luks-${targethostname}"
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} reset --hard
}

function menubackup_cpmainlukskeytogitrepo {
    # TODO: create a branch with targethostname
    # encrypted main luks keyfile
    luksmountdir="$(sudo mktemp -d)"
    sudo chmod -v 0700 ${luksmountdir} # overkill, luks keyfile is already 0400
    sudo cryptsetup luksOpen "${bootkey}"2 luks-bootkey
    sudo mount -v /dev/mapper/luks-bootkey "${luksmountdir}"
    # NOTE: if at some point we are able to use dracut+systemd with a gpg encrypted keyfile
    # we would be able to simply copy the key here
    # NOTE: use hostname-luks-key.bin for now but hostname-luks.key would be preferred
    sudo gpg --pinentry-mode loopback --symmetric --cipher-algo AES256 --output ${luksgitdir}/"${targethostname}"-luks-key.gpg ${luksmountdir}/"${targethostname}"-luks-key.bin
    sudo umount -v /dev/mapper/luks-bootkey
    sudo cryptsetup luksClose /dev/mapper/luks-bootkey
    sudo rmdir -v "${luksmountdir}"

    lukstmpdir="$(sudo mktemp -d)"
    sudo chmod -v 0700 ${lukstmpdir}
    # NOTE/TODO: we need to store temporarily the luks headers in a root only readable dir
    # before encrypting them
    # we cannot use a pipe here, not simply at least
    # encrypted main luks headers
    # NOTE: are headers really in binary format? (so .bin)
    sudo cryptsetup -v luksHeaderBackup "${mainstorage}" --header-backup-file ${lukstmpdir}/"${targethostname}"-luks-main-header.bin
    printf "define luks-main-header passphrase:\n"
    sudo gpg -v --pinentry-mode loopback --symmetric --cipher-algo AES256 --output ${luksgitdir}/"${targethostname}"-luks-main-header.gpg ${lukstmpdir}/"${targethostname}"-luks-main-header.bin

    # encrypted usbkey luks headers
    sudo cryptsetup -v luksHeaderBackup "${bootkey}"2 --header-backup-file ${lukstmpdir}/"${targethostname}"-luks-usbkey-header.bin
    printf "define luks-usbkey-header passphrase:\n"
    sudo gpg -v --pinentry-mode loopback --symmetric --cipher-algo AES256 --output ${luksgitdir}/"${targethostname}"-luks-usbkey-header.gpg ${lukstmpdir}/"${targethostname}"-luks-usbkey-header.bin
    sudo rm -Rfv "${lukstmpdir}"
}

function menubackup_addcommitpushmainlukskeytogitrepo {
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} annex add .
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} commit -a -m "Add ${targethostname}'s gpg enceypted main LUKS key and luks headers (main and usb)"
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${luksgitdir} push srv "luks-${targethostname}"
    #sudo rm -Rf "${gitdir}"
}

function menubackup_rmlukstmpdir {
    sudo rm -vRf "${luksgitdir}"
}

function menubackup_createlvmbackuparchivedirs {
    sudo mkdir -v -p -m 0700 ${mount_root_path}/etc/lvm/backup
    sudo mkdir -v -p -m 0700 ${mount_root_path}/etc/lvm/archive
}

function menubackup_cplvmbackuparchivetochroot {
    # TODO: -a is overkill here, let's use the correct options instead
    sudo cp -v -a /etc/lvm/backup/"${targethostname}"-vg ${mount_root_path}/etc/lvm/backup
    # sudo cp -a /etc/lvm/archive/${targethostname}-vg* ${mount_root_path}/etc/lvm/archive
    # ERROR: cp: cannot stat '/etc/lvm/archive/centranthus-vg*': No such file or directory
    # So let's use find instead
    sudo find /etc/lvm/archive/ -type f -name "*${targethostname}-vg*" -exec cp -v -a {} ${mount_root_path}/etc/lvm/archive \;
}

function menubackup_createbarelvmgitrepo {
    # NOTE: this repo can be shared among a network.
    # It is not as critical as the luks git repo
    # NOTE: we likely do not need to make it 0700, do we?
    sudo mkdir -v -p ${mount_root_path}/srv/git/system-lvm.git
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${mount_root_path}/srv/git/system-lvm.git init --bare
}

function menubackup_createlvmtmpdir {
    lvmgitdir="$(sudo mktemp -d)"
}

function menubackup_createlvmworkdirgitrepo {
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} init
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} remote add srv ${mount_root_path}/srv/git/system-lvm.git
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} checkout --orphan "lvm-${targethostname}"
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} reset --hard
}

function menubackup_cpvgcfgbackuptogitrepo {
    # NOTE: do we need to run vgcfgbackup regularly when using thinpool?
    # why do we need it anyway?
    # NOTE: https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/6/html/logical_volume_manager_administration/backup
    # NOTE: we need either this initial backup or to run all the lvm commands inside the chroot
    # later, as described in the redhat doc, any change will be automagically backed up
    # NOTE: shouldn't we simply copy the backup in the chroot then?
    # we need /etc/lvm/backup and /etc/lvm/archive
    sudo vgcfgbackup -v "${targethostname}"-vg -f ${lvmgitdir}/"${targethostname}"-vgcfgbackup
}

function menubackup_addcommitpushvgcfgbackuptogitrepo {
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} add .
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} commit -a -m "Add ${targethostname}-vg's vgcfgbackup's dump."
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${lvmgitdir} push srv "lvm-${targethostname}"
}

function menubackup_rmlvmtmpdir {
    sudo rm -vRf "${lvmgitdir}"
}

# NOTE: why is this function so early?
function menubackup_createdatachecksumsandlists {
    printf "\n"
    #  use bup to check for new/modified files
}

function menubackup_createbarekernelgitrepo {
    sudo mkdir -v ${mount_root_path}/srv/git/kernel-config.git
    sudo git -c "include.path=${mount_root_path}/home/root-git/.gitconfig" -C ${mount_root_path}/srv/git/kernel-config.git init --bare
    # NOTE: clone (or create a git repo?) the git repo in /home/kernel-build/kernel-config
    # create a branch with the name corresponding to the version of the fedora kernel version
    # add a function in the systemdkernelinitrd menu to be able to git add, git commit and git push after every config
}

function menubackup_createsystembuprepo {
    # we set restrictive permissions here, is there any reason why we should not to?
    sudo mkdir -v -p -m 0700 ${mount_root_path}/srv/backup/system.bup
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         bup --bup-dir=/srv/backup/system.bup init
}

function menubackup_createsystemsnapshots {
    sudo lvcreate -n snap-lvroot --snapshot ${targethostname}-vg/lvroot # mounted RO, and remounted RW while editing /etc
    sudo lvcreate -n snap-lvusr --snapshot ${targethostname}-vg/lvusr # mounted RO
    sudo lvcreate -n snap-lvvar --snapshot ${targethostname}-vg/lvvar # mounted RW
    sudo lvcreate -n snap-lvopt --snapshot ${targethostname}-vg/lvopt # mounted RO
}

function menubackup_activatesystemsnapshots {
    sudo lvchange -v -ay -K ${targethostname}-vg/snap-lvroot
    sudo lvchange -v -ay -K ${targethostname}-vg/snap-lvusr
    sudo lvchange -v -ay -K ${targethostname}-vg/snap-lvvar
    sudo lvchange -v -ay -K ${targethostname}-vg/snap-lvopt
}

function menubackup_createsystemsnapshotsmountpoint {
    # NOTE: ${systemsnapsmntdir} is in tmpfs, so gets destroyed as soon as systemd-nspawn finishes
    # NOTE:
    # dicebox | I use the --strip-path option, as I first create lvm snapshots that I mount in a temporary directory, so I want to strip the path of the temporary directory
    # johill | but I think we determined at some point that it's still in the index or something, so you have to use the same temporary directory every time
    #systemsnapsmntdir="$(sudo mktemp -d --tmpdir=${mount_root_path}/var/tmp)"
    #printf "systemsnapsmntdir=${systemsnapsmntdir}\n"
    #printf "pathinchroot=${systemsnapsmntdir#${mount_root_path}}\n"
    #sudo ls ${mount_root_path}/tmp
    #ls -d ${systemsnapsmntdir}
    systemsnapsmntdir=${mount_root_path}/mnt/system-snaps-mountpoint
    sudo mkdir -v ${systemsnapsmntdir}
}

function menubackup_mountsystemsnapshots {
    # TODO: mount read-only and check other mounts if they need that too
    sudo mount --verbose /dev/${targethostname}-vg/snap-lvroot ${systemsnapsmntdir}
    sudo mount --verbose /dev/${targethostname}-vg/snap-lvusr ${systemsnapsmntdir}/usr
    sudo mount --verbose /dev/${targethostname}-vg/snap-lvvar ${systemsnapsmntdir}/var
    sudo mount --verbose /dev/${targethostname}-vg/snap-lvopt ${systemsnapsmntdir}/opt
}

function menubackup_createsystemchecksumsandlists {
    # NOTE: use bup to check for new/modified files when doing incremental backup
    # NOTE: we want to run that in the snapshots
    # they will be backed up in the bup repo, so we could reuse them when we restore from bup
    # NOTE: is FIND-* ok? should we call them LIST-* or LST-* instead? or without any prefix?
    #dir=/var
    currentdir=$(pwd); cd /
    # NOTE: should LC_ALL=C be moved next to sort instead?
    # NOTE: we don't need to exclude the files created as we are not looking for regular files
    LC_ALL=C sudo find . -type b | sort | sudo tee -a FIND-BLOCKS > /dev/null
    LC_ALL=C sudo find . -type c | sort | sudo tee -a FIND-CHARS > /dev/null
    LC_ALL=C sudo find . -type d | sort | sudo tee -a FIND-DIRS > /dev/null
    LC_ALL=C sudo find . -type p | sort | sudo tee -a FIND-PIPES > /dev/null
    LC_ALL=C sudo find . -type l | sort | sudo tee -a FIND-SYMLINKS > /dev/null
    LC_ALL=C sudo find . -type s | sort | sudo tee -a FIND-SOCKETS > /dev/null
    sudo find . ! -path "./SHA1SUMS" -type f -exec sha1sum {} \; | sudo tee -a SHA1SUMS > /dev/null
    cd ${currentdir}
}

function menubackup_bupindexsavesystem {
    # NOTE: why is it a problem that the index holds the path when bup-index was run
    # if we restore it to an other location?
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         bup --bup-dir=/srv/backup/system.bup index ${systemsnapsmntdir#${mount_root_path}}
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         bup --bup-dir=/srv/backup/system.bup save -n ${targethostname} --strip-path=${systemsnapsmntdir#${mount_root_path}} ${systemsnapsmntdir#${mount_root_path}}
    # bup --bup-dir=/srv/backup/cleanme.bup fsck --generate
}

function menubackup_unmountsystemsnapshots {
    #umount /dev/mapper/${targethostname}-vg/snap-lvopt ${mntdir}/opt
    #umount /dev/mapper/${targethostname}-vg/snap-lvvar ${mntdir}/var
    #umount /dev/mapper/${targethostname}-vg/snap-lvusr ${mntdir}/usr
    #umount /dev/mapper/${targethostname}-vg/snap-lvroot ${mntdir}
    sudo umount --verbose --recursive ${systemsnapsmntdir}
}

function menubackup_removesystemsnapshotsmountpoint {
    sudo rmdir --verbose ${systemsnapsmntdir}
}

function menubackup_deactivatesystemsnapshots {
    sudo lvchange -an ${targethostname}-vg/snap-lvroot
    sudo lvchange -an ${targethostname}-vg/snap-lvusr
    sudo lvchange -an ${targethostname}-vg/snap-lvvar
    sudo lvchange -an ${targethostname}-vg/snap-lvopt
}

function menubackup_removesystemsnapshots {
    sudo lvremove ${targethostname}-vg/snap-lvopt
    sudo lvremove ${targethostname}-vg/snap-lvvar
    sudo lvremove ${targethostname}-vg/snap-lvusr
    sudo lvremove ${targethostname}-vg/snap-lvroot
}

function menubackup_createdatabuprepo {
    mkdir /srv/backup/data.bup
    bup --bup-dir=/srv/backup/data.bup init
}

function menubackup_createdatasnapshot {
    sudo lvcreate -n snap-lvsrv --snapshot ${targethostname}-vg/lvsrv
}

function menubackup_activatedatasnapshot {
    sudo lvchange -ay -K ${targethostname}-vg/snap-lvsrv
}

function menubackup_createmountpoint {
    mntdir="$(sudo mktemp -d)"
}

function menubackup_mountdatasnapshot {
    sudo mount -o ro /dev/${targethostname}-vg/snap-lvsrv ${mount_root_path}/srv/backup/readonly.mnt
}

function menubackup_bupindexsavedata {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         bup index --exclude /srv/backup /srv
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         bup save -n ${targethostname}-srv /srv
}

function menubackup_unmountdatasnapshot {
    umount /srv/backup/readonly.mnt
}

function menubackup_removemountpoint {
    rmdir -v ${mntdir}
}

function menubackup_deactivatedatasnapshot {
    lvchange -an ${targethostname}-vg/snap-lvsrv
}

function menubackup_removedatasnapshot {
    lvremove ${targethostname}-vg/snap-lvsrv
}

function menubackup_selectarchiveusbdrive {
    printf "\n"
}

function menubackup_preparearchiveusbdrive {
    printf "\n"
}

function menubackup_mountarchiveusbdrive {
    printf "\n"
}

function menubackup_updatesystembuprepotousbdrive {
    printf "\n"
}

function menubackup_updatedatabuprepotousbdrive {
    printf "\n"
}

function menubackup_unmountarchiveusbdrive {
    printf "\n"
}

function menubackup_createbuparchivesnapshot {
    lvcreate -n snap-lvbup --snapshot usbarchive-vg/lvbup
}

function menubackup_activatebupsnapshots {
    lvchange -ay -K usbarchive-vg/snap-lvbup
}

function menubackup_mountbuparchivesnapshot {
    mount /dev/usbarchive-vg/snap-lvbup ${mntdir}
}

function menubackup_createdarthinvol {
    #lvcreate -n lvdar --virtual-size 100GiB usbarchive-vg
    lvcreate --virtualsize "100G" --name lvdar --thinpool usbarchive-tp usbarchive-vg
}

function menubackup_createmountpoint {
    mntdir="$(sudo mktemp -d)"
}

function menubackup_mountdarthinvol {
    mount /dev/mapper/${targethostname}-vg/snap-lvsrv ${mntdir}
}

function menubackup_selectburner {
    lsblk --paths --list --output NAME,TYPE | grep rom
}

function menubackup_formatbluray {
    # should reserve 256MiB
    dvd+rw-format -ssa=default /dev/sr0
}

function menubackup_mediainfo {
    dvd+rw-mediainfo /dev/sr0
}

function menubackup_createbackupuser {
    # NOTE: for gpg keys and unpriviledged commands, see dar-helper.sh
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         useradd -m root-backups
}

function menubackup_createbackupusergpgkeys {
    printf "\n"
    # NOTE: gpg user id
    # "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"
    # "Dar Admin (to sign dar slices's checksum) <daradmin@localdomain>"
    # BUG: gpg: cannot open '/dev/tty': No such device or address
    # see https://forums.gentoo.org/viewtopic-t-1037418-start-0.html
    # see https://stackoverflow.com/questions/47713652/su-dev-tty-no-such-device-or-address
    # NOTE: --pinentry-mode loopback and/or --no-tty and/or --batch
    # --pinentry-mode loopback: doesn't solve anything
    # --no-tty: gpg: Sorry, no terminal at all requested - can't get input
    # BUT: gpg: Warning: using insecure memory!
    # NOTE/TODO: see the bug and the note after, about /dev/tty
    # yet it works if we just run a shell in the container with su -l root-backups,
    # only then run the gpg command
    # we run it this way for now to be able to work on the bup archive cleanme.bup
    # sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "gpg -v --pinentry-mode loopback --quick-generate-key \"Root Backups <root-backups@localdomain>\"" root-backups
    # NOTE: user backup-sig password to protect the key
    # TODO: manage revokation certificate correctly
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         sudo -i -u root-backups gpg -v --pinentry-mode loopback --quick-generate-key "Root Backups <root-backups@localdomain>"
}

function menubackup_showbackupusergroups {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         groups root-backups
}

function menubackup_addbackupusertogroup11 {
    # NOTE:
    # in fedora this correspond to the cdrom group
    # in gentoo this correspond to the floppy group
    # we need this to access the disks burner for now
    # we will need to remove this group from the user's groups list
    # as later systemd will be able to manage access rights
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         usermod -a -G 11 root-backups
    # NOTE: alternative: gpasswd --add root-backups 11
    # but would this works considering that the --delete options does not recognize the guid
    # by its number?
}

function menubackup_delbackupuserfromgroup11 {
    # NOTE: https://unix.stackexchange.com/questions/29570/how-do-i-remove-a-user-from-a-group
    # removes all secondary/supplementary groups from username, leaving them as a member of only their primary group
    # NOTE: this should be fine as we probably won't add the user to any other group,
    # at least not at this stage
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         usermod -G "" root-backups
    # NOTE: gpasswd --delete root-backups 11 did not recognize the guid by its number
}

function menubackup_installdarhelper {
    sudo mkdir -p -v ${mount_root_path}/opt/bin
    sudo cp -v dar-helper-*.sh ${mount_root_path}/opt/bin
    ls -l ${mount_root_path}/opt/bin/dar-helper-*.sh
    sudo chmod -v +x ${mount_root_path}/opt/bin/dar-helper-launcher.sh
    ls -l ${mount_root_path}/opt/bin/dar-helper-*.sh
}

function menubackup_setrootpasswd {
    # NOTE:
    # dar-helper script is started as a regular user, yet some commands need to be run as root
    # so we need to be able to su to root
    # NOTE: use root identity from mooltipass, already created
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         passwd
}

function menubackup_createbupsystemdararchive {
    # NOTE: we do not use compression as bup already does
    # NOTE: we do not use par with dar, as both bup and dvdisaster already add some ECC
    # NOTE: dual layers blurays are around 45GiB, and 20% of that,
    # needed for dvdisaster redundancy, is around 10GiB. So let's have slices of 35GiB
    # NOTE: we write the slices directly on the thin volume as the thin pool avoid the need
    # of double writes (one for the slice on the thin volume and one for the copy-on-write
    #  on the snapshot volume)
    # NOTE: symmetric (--key :)or asymmetric encryption (--key gnupg:email)?
    # asymmetric encryption rely on a pair of public and private keys
    # we need to keep the private key accessible outside of the archive, and thus
    # we need to encrypt it with a passphrase. Doesn't that just move the problem
    # away? It can make sense if we want to be able to give/remove access to the archive.
    # This is not our case here, so let's just use symmetric encryption for now
    # NOTE: we need a new key in the mooltipass for the dar archive, actually, one for each
    # archive (system, data and olddata or whatever)
    # NOTE: this need to be run as root, as we access the bup repo, but most of dar-helper.sh
    # can be run unpriviledged
    # TODO/NOTE: su run dar-helper.sh with an unpriviledged user
    # TODO/NOTE: dar-helper.sh has to be inside the chroot, as we run dar inside the chroot
    # we install it in /opt/bin/dar-helper.sh
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     dar --create ${mntdir2}/system --slice 35G --fs-root ${mntdir}/system --hash sha512 --key : --execute su -l -c '/opt/bin/dar-helper.sh %p %n %N %e %c %u' root-backups
    # TODO/NOTE: how do we specify the working directory? the one which will contain the slices
    # TODO/NOTE: also we have to specify which directory to backup
    # NOTE: mntdir is the mount point of the volume dedicated to hold the temporary slices
    # NOTE: mntdir2 is the mount point of the snapshot containing the bup repositories
    # NOTE: then from the snapshot mounted in mntdir2, we consider only the directory given
    # with --go-into
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         dar --create ${mntdir}/system_bup --fs-root ${mntdir2}/srv/backup --go-into system --slice 35G --hash sha512 --key : --execute su -l -c '/opt/bin/dar-helper.sh %p %b %n %N %e %c %u' root-backups
}

function menubackup_createbupdatadararchive {
    # NOTE: we do not use compression as bup already does
    # NOTE: dual layers blurays are around 45GiB, and 20% of that,
    # needed for dvdisaster redundancy, is around 10GiB. So let's have slices of 35GiB
    # NOTE: we write the slices directly on the thin volume as the thin pool avoid the need
    # of double writes (one for the slice on the thin volume and one for the copy-on-write
    #  on the snapshot volume)
    # NOTE: symmetric (--key :)or asymmetric encryption (--key gnupg:email)?
    # asymmetric encryption rely on a pair of public and private keys
    # we need to keep the private key accessible outside of the archive, and thus
    # we need to encrypt it with a passphrase. Doesn't that just move the problem
    # away? It can make sense if we want to be able to give/remove access to the archive.
    # This is not our case here, so let's just use symmetric encryption for now
    dar --create ${mntdir2}/data --slice 35G --pause --fs-root ${mntdir}/data --hash sha512 --key :
}

function menubackup_checksumdararchivesslice {
    # NOTE: do we want to use dar --test and/or dar --diff at this stage as well?
    # NOTE: can we do that per slice and not for the whole archive?
    # http://dar.linux.free.fr/doc/usage_notes.html#Multi_recipient_signed_archive_weakness
    sha512sum -c my_secret_group_stuff.1.dar.sha512
}

function menubackup_signdararchiveslice {
    # NOTE: we need to have a dedicated pair of keys here, or do we want to reuse one?
    # no we don't, we do not plan to share this archive
    # http://dar.linux.free.fr/doc/usage_notes.html#Multi_recipient_signed_archive_weakness
    gpg --local-user name --sign --detach-sign my_secret_group_stuff.1.dar.sha512
}

function menubackup_createiso9660image {
    # OPT ISO9660: create image
    # mkisofs
    # OPT ISO9660: create image alternative
    # NOTE: input/output character sets?
    # NOTE: iso-level? 3 or 4?
    # according to https://en.wikipedia.org/wiki/ISO_9660
    # we need level 3 to use "the multi-extent (fragmentation) feature of ISO 9660 Level 3
    # to create ISO 9660 file systems and single files up to 8 TiB"
    # level "4" does not seem anything more we could need, as Rock Ridge already permits
    # longer names
    # -full-iso9660-filenames ?
    # -udf? check with dvdisaster. dvdisaster's manual is unclear about it.
    # let's use as few options as possible for now
    genisoimage -iso-level 3 -rock -o $IMAGEFILE /srv/backup/dar/system
}

function menubackup_augmentiso9660image {
    # dvdisaster augment image
    # defaults: --ecc-target image --encoding-algorithm auto
    # encoding-io-strategy: mmap for SSDs (or fast drives), readwrite for HDDs (or slow drives)
    # NOTE: let's not use the encoding-io-strategy option for now
    dvdisaster --image $IMAGEFILE --method RS03 --threads $NBTHREADS
}

function menubackup_burnimagetobluray {
    # burn cd/dvd
    # cdrecord
    # burn cd/dvd alternative
    # wodim
    # burn dvd/bluray
    # NOTE: will dvd-compat annoys dvdisaster? We will see
    growisofs -dvd-compat -Z /dev/sg0=$IMAGEFILE
}

function menubackup_dvdisasterscan {
    # dvdisaster scan
    dvdisaster --drive $DRIVE --scan
}

function menubackup_dvdisasterverify {
    # NOTE: split into two functions, and add a function to remove the image file
    # dvdisaster verify image
    dvdisaster --drive $DRIVE --image $IMAGEFILEFROMDRIVE --read
    dvdisaster --image $IMAGEFILEFROMDRIVE --test
}

# NOTE/TODO: we may want to create snapshots of the volumes we run the tests on
# NOTE/TODO: we may want to create thin volumes to test restoration
# NOTE/TODO: tests should run at the end of the backup chain, but should be flexible enough
# to run on any part of the chain for finer diagnosis
# chain: bup host (system,data) -> bup network (system,data) -> dar -> dvdisaster -> disks -> dvdisaster -> dar -> bup network (system,data) -> bup host (system,data)
# dar -> bup network (system,data): we do a full restore of the bup repos
# bup network (system,data) -> bup host (system,data): we restore only a given host
# we want to test to restore a given host directly from dar, without restoring the whole bup repos

function menubackup_dartestsystem {
    # test dar archive integrity
    dar --test

}

function menubackup_dartestdata {
    # test dar archive integrity
    dar --test

}

function menubackup_dardiffsystem {
    # diff dar archive with fs, dar way
    dar --diff
}

function menubackup_dardiffdata {
    # diff dar archive with fs, dar way
    dar --diff
}

# TODO: dar partial restore and full restore
# partial will be about files composing a bup repo
# TODO: dar restore from AVFS

function menubackup_darextractsystem {
    # restore it in a new thin volume
    dar --extract

}

function menubackup_darextractdata {
    # restore it in a new thin volume
    dar --extract
}

function menubackup_darpartialextractsystem {
    # restore it in a new thin volume
    # NOTE: use dar_manager
    dar --extract

}

function menubackup_darpartialextractdata {
    # restore it in a new thin volume
    # NOTE: use dar_manager
    dar --extract
}

function menubackup_bupfscksystem {
    # test bup integrity
    bup fsck
}

function menubackup_bupfsckdata {
    # test bup integrity
    bup fsck
}

function menubackup_manualbupdiffsystem {
    # diff of both volumes
    diff -rd ${mntdir} ${mntdir}
}

function menubackup_manualbupdiffdata {
    # diff of both volumes
    diff -rd ${mntdir} ${mntdir}
}

function menubackup_bupfullrestoresystem {
    # verify file restoration: full system restore
    bup restore
    # 100  bup --bup-dir=/srv/backup/cleanme.bup restore -C /var/tmp/mntrestore /old-backups-to-be-cleaned/latest/mnt/
    # 101  bup --bup-dir=/srv/backup/cleanme.bup restore -C /var/tmp/mntrestore /old-backups-to-be-cleaned/latest/mnt/.
}

function menubackup_bupfullrestoredata {
    # verify file restoration: full system restore
    bup restore
}

function menubackup_buppartialrestoresystem {
    # NOTE: we may not be able to restore all the data at the same time, so we anticipate
    # this by having the command needed to restore only the bup backups we want
    # verify file restoration: full system restore
    bup restore
}

function menubackup_buppartialrestoredata {
    # NOTE: we may not be able to restore all the data at the same time, so we anticipate
    # this by having the command needed to restore only the bup backups we want
    # verify file restoration: full system restore
    bup restore
}

function menubackup_manualsystembackupdiff {
    # diff
    #diff -rd
    # NOTE:
    #│14:39:52 dicebox |  I've started a diff -rq to verify that the restoration went fine. I mention it in case you know a better way to compare two directories                                                  │
    #               │14:43:17  johill | I tend to just sha1sum everything                                                                                                                                                         │   
    #              │21:12:58 dicebox | I've done that sometimes too, and I think I'll do that instead next time, as I count at least three advantages by using checksums                                                         │ 
    #             │21:13:40 dicebox | I can more easily track the progress, by comparing the number of files done and the total number of files                                                                                 │   
    #            │21:14:17 dicebox | I can easily verify again the original or the backup, in around half the time                                                                                                             │ 
    #           │21:14:58 dicebox | and kind of related to the second point, I can delay the verification of the copy after creating the checksums of the original data                                                       │   
    #          │21:15:18 dicebox | so if it takes too long, I can at least stop the machine for the night                                                                                                                    │
    #         │21:15:51  johill | it might also be easier on the disk (if you have rotating ones) to checksum contiguously rather than compare around, but large block sizes during compare would also address that
    # │21:25:08  johill | I guess you could do two sha1sum lists, sort them and then diff that                                                                                                                      │
                 #   │21:25:15  johill | would address all of it                                                                                                                                                                   │

    #│21:25:26 dicebox | yeah, but that would take more time                                                                                                                                                       │
     #               │21:25:27  johill | except maybe empty directories
    #21:31:49 dicebox | and yeah, checksums do not help with empty directories, nor with metadata, like file permissions, extended attributes (including selinux context), acl, and all the dates                 │
     #               │21:31:56  johill | yeah                                                                                                                                                                                      │
     #               │21:32:18  johill | neither does diff :)                                                                                                                                                                      │
    #                │21:32:31 dicebox | I've just looked at ls -l of a few files to check that at least some of the metadata is restored and hope for the best                                                                    │
    #                │21:32:35 dicebox | indeed                                                                                                                                                                                    │
     #               │21:33:23  johill | I guess you could write some kind of deep compare tool for that sort of thing
    # dicebox | I've just realised that I could used find -type not just to compare the list of files, but the list of any type of element, including directories, so being able to  │
    #    │                 | check on the existence of empty directories
    # NOTE: use bup to check for new/modified files when doing incremental backup
    # NOTE: we want to run that in the snapshots
    # they will be backed up in the bup repo, so we could reuse them when we restore from bup
    # NOTE: is FIND-* ok? should we call them LIST-* or LST-* instead? or without any prefix?
    #dir=/var
    currentdir=$(pwd); cd /
    #sudo find . ! -path "./SHA1SUMS" -type f -exec sha1sum {} \; | sudo tee -a SHA1SUMS > /dev/null
    sudo sha1sum -c SHA1SUMS
    # NOTE: should LC_ALL=C be moved next to sort instead?
    # NOTE: we don't need to exclude the files created as we are not looking for regular files
    LC_ALL=C sudo find . -type b | sort | sudo tee -a FIND-BLOCKS2 > /dev/null
    diff FIND-BLOCKS FIND-BLOCKS2
    LC_ALL=C sudo find . -type c | sort | sudo tee -a FIND-CHARS2 > /dev/null
    diff FIND-CHARS FIND-CHARS2
    LC_ALL=C sudo find . -type d | sort | sudo tee -a FIND-DIRS2 > /dev/null
    diff FIND-DIRS FIND-DIRS2
    LC_ALL=C sudo find . -type p | sort | sudo tee -a FIND-PIPES2 > /dev/null
    diff FIND-PIPES FIND-PIPES2
    LC_ALL=C sudo find . -type l | sort | sudo tee -a FIND-SYMLINKS2 > /dev/null
    diff FIND-SYMLINKS FIND-SYMLINKS2
    LC_ALL=C sudo find . -type s | sort | sudo tee -a FIND-SOCKETS2 > /dev/null
    diff FIND-SOCKETS FIND-SOCKETS2
    cd ${currentdir}

}

function menubackup_manualdatabackupdiff {
    # diff
    diff -rd
}

function menubackup_ {
    printf "\n"
}

function fromtemporarysystem_downloadprofilesquickhack {
    git clone https://gitlab.com/dicebox/gentoo-hardened-systemd.git
    printf "\n"
    # git clone
}

function fromtemporarysystem_verifyprofilesquickhack {
    printf "\n"
    #check checksum? git does it for us anyway. maybe signature?
}

function fromtemporarysystem_installprofilesquickhack {
    # manually? create a profilesquickhack installation script?
    #***** profile-quickhack
    #copy parent, profiles.desc in /etc/portage/profile-quickhack/
    #copy postsync-rules-quickhack.sh in /etc/portage/repo.postsync.d/
    #check those files if they are still up-to-date

    #sudo mkdir -v ${mount_root_path}/etc/portage/profile-quickhack/
    #sudo cp -v profiles-quickhack/parent profiles-quickhack/profiles.desc profiles-quickhack/postsync-rules-quickhack.sh ${mount_root_path}/etc/portage/profile-quickhack/
    sudo cp -v gentoo-hardened-systemd/hardened-systemd.sh ${mount_root_path}/etc/portage/repo.postsync.d/

    #in /mnt/gentoo/root
    #cat profiles-stack.py
    #!/usr/bin/python
    #import portage
    #for p in portage.settings.profiles:
    #    print("%s" % p)

    #chmod u+x profiles-stack.py
}

function fromtemporarysystem_uninstallprofilesquickhack {
    printf "\n"
}

function fromtemporarysystem_copyresolvconf {
    #***** resolv.conf
    sudo cp --dereference /etc/resolv.conf ${mount_root_path}/etc/
}

function fromtemporarysystem_createfstabbtrfs {
    btrfs_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvbtrfssubvolumes --output value --match-tag UUID)"

    # /
    echo "UUID=${btrfs_uuid} / btrfs defaults,subvolid=$(sudo btrfs subvolume list /mnt/gentoo/ | grep rootdir | awk '//{print $2}') 1 1" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /home /usr /var
    for d in ${rootdirs}; do
        echo "UUID=${btrfs_uuid} /${d} btrfs defaults,subvolid=$(sudo btrfs subvolume list /mnt/gentoo/ | grep "${d}$" | awk '//{print $2}') 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null
    done

    # /usr/local /usr/src
    for d in ${usrdirs}; do
        echo "UUID=${btrfs_uuid} /usr/${d} btrfs defaults,subvolid=$(sudo btrfs subvolume list /mnt/gentoo/ | grep usr${d} | awk '//{print $2}') 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null
    done

    # /var/cache/distfiles /var/cache/binpkgs
    for d in ${vardirs}; do
        echo "UUID=${btrfs_uuid} /var/${d} btrfs defaults,subvolid=$(sudo btrfs subvolume list /mnt/gentoo/ | grep var${d#cache/} | awk '//{print $2}') 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null
    done

    # swap
    swap_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvswap --output value --match-tag UUID)"
    echo "UUID=${swap_uuid} swap swap defaults 0 0" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /srv/db-vm-swapfile
    dbvmswapfile_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvext4 --output value --match-tag UUID)"
    echo "UUID=${dbvmswapfile_uuid} /srv/db-vm-swapfile ext4 defaults 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /boot
    efiboot_uuid="$(sudo blkid ${bootkey}1 --output value --match-tag UUID)"
    echo "UUID=${efiboot_uuid} /boot vfat defaults,noauto 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    cat ${mount_root_path}/etc/fstab
    printf "\n"
    #***** fstab
    #rw,relatime,seclabel,ssd,space_cache
}

function fromtemporarysystem_createfstab {
    # TODO: change mount options, like using nodev
    # NOTE: we use discard as we make sure it is not forwareded to the physical device
    # swap
    swap_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvswap --output value --match-tag UUID)"
    echo "UUID=${swap_uuid} swap swap defaults,discard 0 0" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /
    root_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvroot --output value --match-tag UUID)"
    echo "UUID=${root_uuid} / ext4 defaults,discard 1 1" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /home
    home_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvhome --output value --match-tag UUID)"
    echo "UUID=${home_uuid} /home ext4 defaults,discard 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /opt
    opt_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvopt --output value --match-tag UUID)"
    echo "UUID=${opt_uuid} /opt ext4 defaults,discard 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /srv
    srv_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvsrv --output value --match-tag UUID)"
    echo "UUID=${srv_uuid} /srv ext4 defaults,discard 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /usr
    usr_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvusr --output value --match-tag UUID)"
    echo "UUID=${usr_uuid} /usr ext4 defaults,discard 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /var
    var_uuid="$(sudo blkid /dev/mapper/${targethostname}--vg-lvvar --output value --match-tag UUID)"
    echo "UUID=${var_uuid} /var ext4 defaults,discard 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    # /boot
    efiboot_uuid="$(sudo blkid ${bootkey}1 --output value --match-tag UUID)"
    echo "UUID=${efiboot_uuid} /boot vfat defaults,noauto 1 2" | sudo tee -a ${mount_root_path}/etc/fstab > /dev/null

    cat ${mount_root_path}/etc/fstab
    printf "\n"
    #***** fstab
    #rw,relatime,seclabel,ssd,space_cache
}

function fromtemporarysystem_createcrypttab {
    # Is it really needed for full disk luks encryption?
    printf "\n"
    #***** crypttab
    #gentoo-luks UUID=233c7f9e-f812-42dc-8aa3-8717bd96e765
}

function fromtemporarysystem_installportagedependancies {
    # TODO: /etc/portage/package.accept_keywords
    # required by app-crypt/gkeys (argument)
    # =app-crypt/gkeys-0.2 ~amd64
    # https://wiki.gentoo.org/wiki//etc/portage/package.accept_keywords#Find_obsolete_entries
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge app-portage/cpuid2cpuflags app-portage/repoman
    #     emerge app-arch/tarsync app-portage/cpuid2cpuflags app-crypt/gentoo-keys app-crypt/gkeys app-crypt/gnupg
}

function fromtemporarysystem_uninstallportagedependancies {
    printf "\n"
}

function fromtemporarysystem_createmakeconf {
    # TODO: USE="cryptsetup" , needed for LUKS support, as systemd needs to support luks, as
    # it is embedded inside our initrd
    # but systemd first needs to be installed with -cryptsetup and then again with cryptsetup
    # TODO: ask for the numbers of threads
    # backup current make.conf
    sudo cp -v ${mount_root_path}/etc/portage/make.conf ${mount_root_path}/etc/portage/make.conf.fromstage3
    # example.conf
    sudo cp -v ${mount_root_path}/usr/share/portage/config/make.conf.example ${mount_root_path}/etc/portage/make.conf
    # use_expand flags
    mystr="# USE_EXPAND flags\n# ================\n# Generated from /usr/portage/profiles/desc/\n# Look there for avalaible values for each flag\n"
    for s in $(find /mnt/gentoo/var/db/repos/gentoo/profiles/desc/ -type f -exec basename {} \; | sed 's/^[^\.]*/\U&=/' | cut -d "." -f 1); do mystr="${mystr}# ${s}\n"; done
    sudo sed -i "s;^# Host Setting$;${mystr}\n&;" ${mount_root_path}/etc/portage/make.conf

    CPU_FLAGS_X86="$(sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --quiet cpuid2cpuflags | cut -d " " -f 2-)"
    sudo sed -i "s/^# CPU_FLAGS_X86=$/&\nCPU_FLAGS_X86=\"${CPU_FLAGS_X86}\"/" ${mount_root_path}/etc/portage/make.conf

    #INPUT_DEVICES="libinput"
    #L10N="en en-US"
    #LINGUAS="en en_US"
    #VIDEO_CARDS="intel i965"

    COMMON_FLAGS="-march=native -O2 -pipe"
    sudo sed -i "s/^# Decent examples:$/&\nCOMMON_FLAGS=\"${COMMON_FLAGS}\"\n/" ${mount_root_path}/etc/portage/make.conf
    sudo sed -i "s/^#CFLAGS=\"-march=athlon64 -O2 -pipe\"$/&\nCFLAGS=\"\${COMMON_FLAGS}\"\n/" ${mount_root_path}/etc/portage/make.conf
    sudo sed -i "s/^#CXXFLAGS=\"\${CFLAGS}\"$/&\nCXXFLAGS=\"\${COMMON_FLAGS}\"\n/" ${mount_root_path}/etc/portage/make.conf
    sudo sed -i "s/^#FFLAGS=\"\${CFLAGS}\"$/&\nFFLAGS=\"\${COMMON_FLAGS}\"\n/" ${mount_root_path}/etc/portage/make.conf
    sudo sed -i "s/^#FCFLAGS=\"\${FFLAGS}\"$/&\nFCFLAGS=\"\${COMMON_FLAGS}\"\n/" ${mount_root_path}/etc/portage/make.conf


    EMERGE_DEFAULT_OPTS="--ask y --ask-enter-invalid --color y --jobs=5 --load-average=4 --verbose"
    sudo sed -i "s/^#EMERGE_DEFAULT_OPTS=\"\"$/&\nEMERGE_DEFAULT_OPTS=\"${EMERGE_DEFAULT_OPTS}\"/" ${mount_root_path}/etc/portage/make.conf
    MAKEOPTS="-j5 -l4"
    sudo sed -i "s/^#MAKEOPTS=\"-j2\"$/&\nMAKEOPTS=\"${MAKEOPTS}\"/" ${mount_root_path}/etc/portage/make.conf
    #FEATURES="userfetch webrsync-gpg"
    FEATURES="userfetch"
    sudo sed -i "s/^# splitdebug test userpriv usersandbox\"$/&\nFEATURES=\"${FEATURES}\"/" ${mount_root_path}/etc/portage/make.conf

    #PORTAGE_GPG_DIR="# Holds the keys of the kingdom for webrsync-gpg.\n"
    #PORTAGE_GPG_DIR="${PORTAGE_GPG_DIR}PORTAGE_GPG_DIR=\"/var/lib/gentoo/gkeys/keyrings/gentoo/release\"\n\n"
    #sudo sed -i "s;^# PORTAGE_TMPDIR is the location;${PORTAGE_GPG_DIR}&;" ${mount_root_path}/etc/portage/make.conf
}

function fromtemporarysystem_enableunstablegentoosources {
    printf "\n"
}

function fromtemporarysystem_setupgpgportage {
    # webrsync-gpg needs gnupg and gentoo-keys which arent in hardened stage 3. Could they be included?
    # https://wiki.gentoo.org/wiki//etc/portage/repos.conf/webrsync.conf
    # emerge app-crypt/gentoo-keys app-crypt/gnupg app-arch/tarsync

    #  gpg --homedir /var/lib/gentoo/gkeys/keyrings/gentoo/release --with-fingerprint --list-keys
    #sudo gpg --homedir ${mount_root_path}/var/lib/gentoo/gkeys/keyrings/gentoo/release --with-fingerprint --list-keys
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         gkeys refresh-key -C gentoo
    #  gpg --homedir /var/lib/gentoo/gkeys/keyrings/gentoo/release --edit-key 0xDB6B8C1F96D8BF6D trust
    printf "Enter '4' and 'quit' in gpg shell, if needed.\n"
    sudo gpg --homedir ${mount_root_path}/var/lib/gentoo/gkeys/keyrings/gentoo/release --edit-key 0xDB6B8C1F96D8BF6D trust
    #  gpg>4
    #  gpg>quit
    # chown -R portage:portage /var/lib/gentoo/gkeys/keyrings/gentoo/release ?
    sudo chmod -v 0700 ${mount_root_path}/var/lib/gentoo/gkeys/keyrings/gentoo/release
    sudo chmod -Rv go-rwx ${mount_root_path}/var/lib/gentoo/gkeys/keyrings/gentoo/release

    printf "Also chown for userfetch.\n"
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         chown --recursive --verbose portage:portage /var/db/repos/gentoo
}

function fromtemporarysystem_createreposconf {
    printf "\n"
    #***** repos.conf
    #/mnt/gentoo/etc/portage/repos.conf/gentoo.conf
    # Based on /usr/share/portage/config/repos.conf
    # Based on https://wiki.gentoo.org/wiki//etc/portage/repos.conf/webrsync.conf

    #[DEFAULT]
    #main-repo = gentoo

    #[gentoo]
    #location = /usr/portage
    ##sync-type = rsync
    #sync-type = webrsync
    ##sync-uri = rsync://rsync.gentoo.org/gentoo-portage
    ##sync-uri =
    #auto-sync = yes

    ## for daily squashfs snapshots
    ##sync-type = squashdelta
    ##sync-uri = mirror://gentoo/../snapshots/squashfs
    #****** gentoo.conf
    sudo mkdir -v ${mount_root_path}/etc/portage/repos.conf/

    # do we need to set an empty value to sync-uri? (sync-uri = )?
    #cat <<EOF | sudo tee -a ${mount_root_path}/etc/portage/repos.conf/gentoo.conf > /dev/null
#[DEFAULT]
#main-repo = gentoo
#
#[gentoo]
#location = /var/db/repos/gentoo
#sync-type = webrsync
#auto-sync = yes
#EOF

    sudo cp -v ${mount_root_path}/usr/share/portage/config/repos.conf ${mount_root_path}/etc/portage/repos.conf/gentoo.conf
}

function fromtemporarysystem_syncportage {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emaint sync -a
    # getting a problem: WKD does not work, and keyserver does not always work.
    # get more info with
    # gemato verify --debug --openpgp-key /usr/share/openpgp-keys/gentoo-release.asc  /var/db/repos/gentoo/
    # get even more info by editing /usr/lib64/python3.6/site-packages/gemato/openpgp.py
    # change gpg call from
    # p = subprocess.Popen([impl, '--batch'] + options,
    # to
    # p = subprocess.Popen([impl, '--batch'] + ['-vv'] + ['--debug-level'] + ['guru'] + ['--debug-all'] + options,
    # if wkd does not work and keyserver neither, workaround is to edit /etc/portage/repos.conf/gentoo.conf
    # and unset sync-openpgp-keyserver, so
    # sync-openpgp-keyserver =
}

function fromtemporarysystem_updateportage {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --oneshot portage
}

function fromtemporarysystem_setupportageprofile {
    # TODO: profile_systemd could be made simpler if we accessed directly ${mount_root_path}/etc/portage/make.profile
    # NOTE: the output of eselect profile is messy, with ANSI escape sequences which make it
    # not trivial to sed. So we use sed
    #profile_systemd="$(
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --quiet \
    #       eselect profile list \
    #| grep "*" | awk '{print $2}' | sed "s/$/\/systemd/")"
    #profile_number="$(
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --quiet \
    #     eselect profile list \
    #| grep "${profile_systemd}" | awk '{print $1}' | sed 's/.*\[\([0-9]*\)\].*/\1/' )"
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     eselect profile set --force "${profile_number}"
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     eselect profile show
    profile_stage3="$(readlink --canonicalize ${mount_root_path}/etc/portage/make.profile)"
    profile_systemd="${profile_stage3}/systemd"
    # NOTE: Should we remove the symlink before recreating it, instead of using options
    # potentially incompatible with other than GNU ln implementations?
    sudo ln -fnrsv ${profile_systemd} ${mount_root_path}/etc/portage/make.profile
}

function fromtemporarysystem_updateworld {
    # TODO: Unable to enable loopback interface: EPERM
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --ask --update --deep --newuse @world     # installs systemd
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --depclean
}

function fromtemporarysystem_setusecryptsetup {
    printf "\n"
}

function fromtemporarysystem_updateworld {
    # TODO: Unable to enable loopback interface: EPERM
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --ask --update --deep --newuse @world     # installs systemd
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --depclean
}

function fromtemporarysystem_rebuildtoolchain {
    printf "Prefer to bootstrap base as it rebuilds the toolchain as well\n"
    # toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    # emerge -1 binutils glibc
    # emerge -e --keep-going @system
}

function fromtemporarysystem_editscriptbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    # https://wiki.gentoo.org/wiki/FAQ#How_do_I_install_Gentoo_using_a_stage1_or_stage2_tarball.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         /var/db/repos/gentoo/scripts/bootstrap.sh --pretend
    testvirtual="if [ \"\${myLIBC}\" == \"virtual/libc\" ]; then\n  myLIBC=\"\"\nfi\n\n"
    sudo sed -i "s:^# This stuff should never fail but will if not enough is installed\.$:${testvirtual}&:" ${mount_root_path}/var/db/repos/gentoo/scripts/bootstrap.sh
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         /var/db/repos/gentoo/scripts/bootstrap.sh --pretend
}

function fromtemporarysystem_runscriptbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         /var/db/repos/gentoo/scripts/bootstrap.sh

    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         gcc-config --list-profiles
}

function fromtemporarysystem_fixtoolchainbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         gcc-config 1
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         env-update && source /etc/profile
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --ask --verbose --oneshot sys-devel/libtool
}

function fromtemporarysystem_rebuildworldbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --ask --verbose --emptytree --with-bdeps=y @world
}

function fromtemporarysystem_resumerebuildworldbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --resume
}

function fromtemporarysystem_rundispatchconfbootstrapbase {
    printf "\n"
    # stage 1,2,3 AND/OR toolchain bootstrap
    # https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide/Building_the_Gentoo_Base_System_Minus_Kernel#Bootstrapping_the_Base_System_.28Optional_but_Recommended.29
    # https://wiki.gentoo.org/wiki/Hardened/FAQ#How_do_I_switch_to_the_hardened_profile.3F
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         dispatch-conf
}

function fromtemporarysystem_rebuildworld {
    printf "\n"
    # emerge -e --keep-going @world
}

function fromtemporarysystem_copyxkb2kbdkeymaps {
    sudo mkdir -v --parent ${mount_root_path}/opt/local/share/kbd/keymaps/
    # for keymap check https://src.fedoraproject.org/rpms/kbd/blob/master/f/kbd.spec
    sudo cp -r /usr/lib/kbd/keymaps/xkb/ ${mount_root_path}/opt/local/share/kbd/keymaps/
}

function fromtemporarysystem_createkeymapsymlink {
    # Let's put that in its own function, so this function can be reused when the package containing the keymaps gets updated.
    # This, until we manage to package the converted xkb keymaps thanks to the fedora script
    sudo ln -vsr ${mount_root_path}/usr/local/share/kbd/keymaps/xkb/ ${mount_root_path}/usr/share/keymaps/
}

function fromtemporarysystem_firstbootprompt {
    printf "timezone(path from /usr/share/zoneinfo/ ex:\"Europe/Paris\"): ${timezone}\n"
    read timezone
    printf "lang(ex:\"fr_FR.UTF-8\"): ${lang}\n"
    read lang
    printf "lcmessages(ex:C): ${lcmessages}\n"
    read lcmessages
    printf "keymap(ex:us, us-acentos, us-altgr-intl): ${keymap}\n"
    read keymap
}

function fromtemporarysystem_firstboot {
    # add a non-interactive equivalent
    # sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #      systemd-firstboot --locale= --locale-messages= --keymap= --timezone= --hostname=
    # What about root password? prompt? from a file? from a variable?
    # What about machine-id? is it set randomly when we install systemd? it exists at this stage as of 7th august 2019
    # locale: /etc/locale.conf
    # locale-messages: /etc/locale.conf
    # keymap: /etc/vconsole.conf (font too)
    # for keymap check https://src.fedoraproject.org/rpms/kbd/blob/master/f/kbd.spec
    # and see how they convert xkb keymaps to kbd keymaps
    # timezone: /etc/localtime (symlink)
    # hostname: /etc/hostname
    # locale not prompted, why?
    # timezone already set at this stage, when installing systemd? and not prompted, why?
    # root password not prompted, why?
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     systemd-firstboot --prompt
    # giving up on systemd-firstboot, making our own
    sudo sh -c "echo ${targethostname} > ${mount_root_path}/etc/hostname"
    if [ -n "${lang}" ]; then
        sudo sh -c "echo \"LANG=${lang}\" > ${mount_root_path}/etc/locale.conf"
    fi
    if [ -n "${lcmessages}" ]; then
        sudo sh -c "echo \"LC_MESSAGES=${lcmessages}\" >> ${mount_root_path}/etc/locale.conf"
    fi
    if [ -n "${keymap}" ]; then
        if [[ "${keymap}" =~ "xkb/" ]]; then
            #sudo sh -c "echo \"KEYMAP=/usr/local/share/kbd/keymaps/${keymap}.map.gz\" > ${mount_root_path}/etc/vconsole.conf"
            sudo sh -c "echo \"KEYMAP=${keymap}\" > ${mount_root_path}/etc/vconsole.conf"
        else
            sudo sh -c "echo \"KEYMAP=${keymap}\" > ${mount_root_path}/etc/vconsole.conf"
        fi
    fi
    # TODO: move passwd at just before reboot
    # TODO: make sure the right keymap is loaded when entering the passwd
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     passwd
}

function fromtemporarysystem_setupsystemdconsolekeymap {
    printf "\n"
    # firstboot can only set keymap and hostname because systemd is not started inside the nspawn container
    #systemd-firstboot --prompt

    # keymap
    # https://src.fedoraproject.org/rpms/kbd/blob/master/f/kbd.spec
    # should contact http://kbd-project.org
    #from kbd.spec in kbd-2.0.4-5.fc28.src.rpm
    #xml2lst.pl is in kbd-2.0.4-5.fc28.src.rpm
    # Convert X keyboard layouts to console keymaps
    #mkdir -p $RPM_BUILD_ROOT/lib/kbd/keymaps/xkb
    #perl xml2lst.pl < /usr/share/X11/xkb/rules/base.xml > layouts-variants.lst
    #while read line; do
    #  XKBLAYOUT=`echo "$line" | cut -d " " -f 1`
    #  echo "$XKBLAYOUT" >> layouts-list.lst
    #  XKBVARIANT=`echo "$line" | cut -d " " -f 2`
    #  ckbcomp "$XKBLAYOUT" "$XKBVARIANT" | gzip > $RPM_BUILD_ROOT/lib/kbd/keymaps/xkb/"$XKBLAYOUT"-"$XKBVARIANT".map.gz
    #done < layouts-variants.lst


    # Convert X keyboard layouts (plain, no variant)
    #cat layouts-list.lst | sort -u >> layouts-list-uniq.lst
    #while read line; do
    #  ckbcomp "$line" | gzip > $RPM_BUILD_ROOT/lib/kbd/keymaps/xkb/"$line".map.gz
    #done < layouts-list-uniq.lst

    #which gives among other files
    #/usr/lib/kbd/keymaps/xkb/us-intl.map.gz

    #compromis: utiliser us-acentos
}

function fromtemporarysystem_linksystemdmtab {
    printf "\n"
    # as of 7th august 2019, already set at this stage, is it set when installing systemd?
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         ln -vsfr /proc/self/mounts /etc/mtab
}

function fromtemporarysystem_setupx11keyboard {
    # /etc/X11/xorg.conf.d/00-keyboard.conf
    printf "\n"
}

function fromtemporarysystem_selectgentookernelrelease {
    printf "\n"
    # merge gentoo-sources closest version to running kernel in fedora/host
    # force version example
    # emerge =sys-kernel/gentoo-sources-4.16.8
    #/etc/portage/package.accept_keywords
    #sys-kernel/gentoo-sources ~amd64

        #vermajminfed=$(uname --release | cut -d "-" -f 1) # on liveusb we won't necessarily run the last available kernel, so we need something else here
    #verfed=$(echo $vermajminfed | cut -d "." -f 1)
    #majfed=$(echo $vermajminfed | cut -d "." -f 2)
    #minfed=$(echo $vermajminfed | cut -d "." -f 3)
    #candidates=""
    #for vermajmin in $(ls ${mount_root_path}/var/db/repos/gentoo/sys-kernel/gentoo-sources/gentoo-sources-*); do
    #    candidates="${candidates} $(echo $vermajmin | grep -o "[0-9.]*[0-9]")"
    #done
    #preval=""
    #val=""
    #for vercandidate in  $(echo $candidates | grep -o " [0-9]" | sort -h | uniq); do
    #    val=$(($vercandidate - $verfed))
    #    if [[ $val -eq 0 ]]; then
    #        vernear=$vercandidate
    #        break
    #    elif [[ -z $vernear ]]; then
    #        vernear=$vercandidate
    #    elif [[  $val -lt $vernear && $val -ge 0 ]]; then
    #        vernear=$vercandidate
    #    elif [[ $val -gt $vernear && $val -le 0 ]]; then
    #        vernear=$vercandidate
    #    fi
    #done
    #for majcandidate in  $(echo $candidates | grep -o " $vernear.*" | grep -o "\.[0-9]*\." | grep -o "[0-9]*" | sort -h | uniq); do
    #    val=$(($majcandidate - $majfed))
    #    if [[ $val -eq 0 ]]; then
    #        majnear=$majcandidate
    #        break
    #    elif [[ -z $majnear ]]; then
    #        majnear=$majcandidate
    #    elif [[  $val -lt $majnear && $val -ge 0 ]]; then
    #        majnear=$majcandidate
    #    elif [[ $val -gt $majnear && $val -le 0 ]]; then
    #        majnear=$majcandidate
    #    fi
    #done
    #for mincandidate in  $(echo $candidates | grep -o " $vernear\.$majnear.*" | grep "\.[0-9]*\([ ]\|$\)" | grep -o "[0-9]*" | sort -h | uniq); do
    #    val=$(($mincandidate - $minfed))
    #    echo $val
    #    if [[ $val -eq 0 ]]; then
    #        minnear=$mincandidate
    #        break
    #    elif [[ -z $minnear ]]; then
    #        minnear=$mincandidate
    #    elif [[  $val -lt $minnear && $val -ge 0 ]]; then
    #        minnear=$mincandidate
    #    elif [[ $val -gt $minnear && $val -le 0 ]]; then
    #        minnear=$mincandidate
    #    fi
    #done
    #nearest="$vernear.$majnear.$minnear"

    # Comparisons of default gentoo-sources, closest version to running kernel of gentoo-sources, and last stable vanilla kernel
    # default gentoo-sources: systemd kernel setting, paired with mesa, no extra work to select it
    # closest version to running kernel of gentoo-sources: buggy algo to select it, could have problems with mesa
    # vanilla kernel: no official way to (easily) get it from a script, no systemd kernel setting, could have problems with mesa, but still preferred from a feature+security perspective
    # we will go with default gentoo-sources for now
    printf "gentoo-sources version (ex: 5.4.14, see https://packages.gentoo.org/packages/sys-kernel/gentoo-sources): ${krnrelease}\n"
    read krnrelease
    # TODO: display version of running kernel, config files in /boot, and available versions of sys-kernel/gentoo-sources, and kernel versions available for the running version of fedora
}

function fromtemporarysystem_unmaskunstablegentoosources {
    echo "sys-kernel/gentoo-sources ~amd64" | sudo tee -a ${mount_root_path}/etc/portage/package.accept_keywords > /dev/null
}

function fromtemporarysystem_installkernelgentoosources {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         -E USE="symlink ${USE}" \
         emerge =sys-kernel/gentoo-sources-${krnrelease}
}

function fromtemporarysystem_listinstalledkernels {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         eselect kernel list
}

function fromtemporarysystem_selectkernelsymlink {
    printf "Enter the kernel you want, from listinstalledkernels:\n"
    read ikerver
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         eselect kernel set ${ikerver}
}

function fromtemporarysystem_installbasicsystempackages {
    #install tools now: tmux, openssh, btrfs, lvm, etc
    #bootloader
    #git git-annex

    # https://wiki.gentoo.org/wiki/Useful_Portage_tools
    # sys-kernel/linux-firmware, as of september 2nd 2019 (with outdated repo data)
    # needs a change in package.license
    # we need to be able to detect such cases or ask the user to rerun the function
    # if an unmask is needed.
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge sys-fs/btrfs-progs sys-fs/btrfsmaintenance sys-fs/lvm2 app-portage/layman app-portage/eix app-portage/gentoolkit app-portage/pfl www-client/pybugz sys-kernel/linux-firmware sys-kernel/dracut sys-fs/cryptsetup media-fonts/terminus-font sys-boot/efibootmgr
    #emerge sys-kernel/gentoo-sources sys-kernel/linux-firmware sys-kernel/dracut

    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         dispatch-conf
}

function fromtemporarysystem_configurelvm.conf {
    # NOTE: let's keep the presentation of the document by using tabulations
    # Enable thin pool autoextend
    sudo sed -i "s/^\tthin_pool_autoextend_threshold = 100$/\tthin_pool_autoextend_threshold = 70/" ${mount_root_path}/etc/lvm/lvm.conf
    sudo sed -i "s/^\t# thin_pool_discards = \"passdown\"$/\tthin_pool_discards = \"nopassdown\"/" ${mount_root_path}/etc/lvm/lvm.conf
    sudo sed -i "s/^\t# thin_pool_zero = 1$/\tthin_pool_zero = 0/" ${mount_root_path}/etc/lvm/lvm.conf
}

function fromtemporarysystem_createkernelbuilderuser {
    # TODO: rename kernel-builder to root-kernel or root-kernelbuilder
    # https://forums.gentoo.org/viewtopic-t-1064076-start-0.html
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         useradd -m kernel-builder
}

function fromtemporarysystem_createkernelbuilddirsfiles {
    kerneldirs="$(find ${mount_root_path}/usr/src/ -maxdepth 1 -type d -exec basename {} \; | grep -v "^src$")"
    for d in kerneldirs; do
        if ! [ -e "${mount_root_path}/home/kernel-builder/${d}" ]; then
            sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
                 su -l -c "mkdir -v ${kerneldirs}" kernel-builder
        elif ! [ -d "${mount_root_path}/home/kernel-builder/${d}"]; then
            printf "${mount_root_path}/home/kernel-builder/${d} exists but is not a directory. This is a problem!\n"
        fi
    done
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "mkdir -v kernel.d" kernel-builder
    # NOTE: we need to touch dracut.cpio only when we embed it in the kernel
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "mkdir -v initramfs.d" kernel-builder
    # TODO: explore the storage space and decompression gain when compressing the initramfs
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "touch initramfs.d/dracut.cpio" kernel-builder
}

function fromtemporarysystem_copyfedorakernelconfig {
    # NOTE: fedora has compressed modules installed while its config has
    # # CONFIG_MODULE_COMPRESS is not set
    # from #fedora-kernel
    # jcline | dicebox, yes, the reason is we manually compress them, if I recall correctly due to stripping debug symbols in combination with module signing
    # cline | dicebox, it's all handled in the __modsign_install_post rpm macro
    # https://fedoraproject.org/wiki/Kernel/Spec
    # https://src.fedoraproject.org/rpms/kernel/blob/HEAD/f/kernel.spec
    # compressing modules save a lot of space, let's set it in our config

    # list kernels in ${mount_root_path}/usr/src and ask user to select one
    # sudo ln -sfvr ${mount_root_path}/usr/src/${selectedkernel} ${mount_root_path}/usr/src/linux
    # TODO: rename function from *fedoraconfigkernel to *fedorakernelconfig
    # TODO: copy config from nearest (from the left) version of fedora kernel
    # https://koji.fedoraproject.org/koji/search?match=glob&type=build&terms=kernel-4.19.*
    sudo cp -v /boot/config-$(uname --release) ${mount_root_path}/home/kernel-builder/fedora-config
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         chown -v kernel-builder:kernel-builder /home/kernel-builder/fedora-config
    # from https://www.kernel.org/doc/Documentation/kbuild/kconfig.txt
    # To see a list of new config symbols, use
	  #   cp user/some/old.config .config
	  #   make listnewconfig
}

function fromtemporarysystem_verifyfedoraconfigkernel {
    # CONFIG LOOP
    # check kernel modules/options: selinux, systemd, kernel self-protection
    # For selinux and systemd, check emerge log, for ksp check next url
    # https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings
    # For selinux and systemd, double check in gentoo wiki and arch wiki
    git clone https://gitlab.com/dicebox/kernel-verify-selfprotection
    ./verify-selfprotection.sh ${mount_root_path}/usr/src/linux/.config

    # TODO: verify EFI early printk

    # for selinux and systemd, (re)emerge the package and look the state of recommended configs
    # or work from:
    # https://forums.gentoo.org/viewtopic-p-8244208.html?sid=dcb50c60c1b27ab62b4fecab24d31f76
    # find /mnt/gentoo/var/db/pkg -type f -name *.ebuild -exec grep "CONFIG_CHECK" {} \;
    # for now, lets just reemerge
    oneshotemerge="$(grep -B 3 "Unable to find kernel sources at /usr/src/linux" ${mount_root_path}/var/log/portage/elog/summary.log | grep -o "[a-z]*-[a-z]*/[a-zA-Z0-9]*" | sort | uniq)"
    # re-emerge with --pretend and --oneshot
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --oneshot ${oneshotemerge}

    # make menuconfig
    # END CONFIG LOOP
}

function fromtemporarysystem_verifyconfigkernelselfprotectionproject {
    # check kernel modules/options: selinux, systemd, kernel self-protection
    # For selinux and systemd, check emerge log, for ksp check next url
    # https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings
    # For selinux and systemd, double check in gentoo wiki and arch wiki
    git clone https://gitlab.com/dicebox/kernel-verify-selfprotection
    ./verify-selfprotection.sh ${mount_root_path}/usr/src/linux/.config
}

function fromtemporarysystem_listpackagesrequiredconfigs {
    # NOTE: they still can't find our config as it is in kernel-builder's home
    # TODO: verify EFI early printk

    # for selinux and systemd, (re)emerge the package and look the state of recommended configs
    # or work from:
    # https://forums.gentoo.org/viewtopic-p-8244208.html?sid=dcb50c60c1b27ab62b4fecab24d31f76
    # find /mnt/gentoo/var/db/pkg -type f -name *.ebuild -exec grep "CONFIG_CHECK" {} \;
    # for now, lets just reemerge
    oneshotemerge="$(grep -B 3 "Unable to find kernel sources at /usr/src/linux" ${mount_root_path}/var/log/portage/elog/summary.log | grep -o "[a-z]*-[a-z]*/[a-zA-Z0-9]*" | sort | uniq)"
    # re-emerge with --pretend and --oneshot
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         emerge --oneshot ${oneshotemerge}
}

function fromtemporarysystem_buildfedoraconfigkernel {
    # https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools.git/tree/README
    # https://forums.gentoo.org/viewtopic-t-1064076-start-0.html
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         useradd -m kernel-builder
    kerneldirs="$(find ${mount_root_path}/usr/src/ -maxdepth 1 -type d -exec basename {} \; | grep -v "^src$")"
    for d in kerneldirs; do
        if ! [ -e "${mount_root_path}/home/kernel-builder/${d}" ]; then
            sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
                 su -l -c "mkdir -v ${kerneldirs}" kernel-builder
        elif ! [ -d "${mount_root_path}/home/kernel-builder/${d}"]; then
            printf "${mount_root_path}/home/kernel-builder/${d} exists but is not a directory. This is a problem!\n"
        fi
    done
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "mkdir -v initramfs.d" kernel-builder
    # TODO: explore the storage space and decompression gain when compressing the initramfs
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "touch initramfs.d/dracut.cpio" kernel-builder
    # TODO: keymap
    # keymap inside kernel? (defkeymap.map, CONFIG_HW_CONSOLE) or simply in the initrd?
    # in initrd for now
    # TODO: kernel+dracut cmdline
    # cmdline options inside kernel? or simply in the initrd?
    # let's keep everyting in initrd, dracut can use /etc/cmdline.d/*.conf
    # and/or --kernel-cmdline (where are they stored in this case?)
    # it should work as the BOOT_IMAGE will be embdded in the kernel
    # thanks to CONFIG_INITRAMFS_SOURCE
    # see
    # see what? I can't remember :/
    # about the cmdline, it will instead be embedded inside the kernel,
    # and locked with CONFIG_CMDLINE_OVERRIDE
    # We use the /usr/src/linux symlink to set the kernelversion variable.
    # TODO: kernelversion is called somewhere else, should we make it a global variable?
    kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
    # accept defaults except for gentoo systemd, so do not go too fast to not miss it
    # olddefconfig: Same as oldconfig but sets new symbols to their default value without prompting
    # We would need to manage systemd and openrc options manually with olddefconfig
    # TODO: embed initrd, first a dummy one
    # path of initrd: /home/kernel-builder/initramfs.d/dracut.cpio.gz
    # TODO: KCONFIG_CONFIG=/path/to/myconfig make menuconfig, make a backup first, make sure to copy to .config if compiling the kernel after

     # see kernelversion in fromtemporarysystem_buildfedoraconfigkernel
    modulesversion="$(echo ${kernelversion} | sed "s/^linux-//" )"
    # TODO: grab uuids from host
    # /dev/mapper/centranthus--vg-lvbtrfssubvolumes
    # /dev/mapper/centranthus--vg-lvswap
    rootuuid="608f0264-3e47-44ee-8c12-c790698ff747"
    # TODO: can we use UUID_SUB to identify the subvolume?
    rootsubvol="/rootdir"
    lukskeyfile="/centranthus-luks-key.gpg"
    luksuuid="7587e8da-7e68-4eb9-a1ad-57841ec613c8"
    lukskeyuuid="346C-636F"
    lvbtrfssubvolumes="608f0264-3e47-44ee-8c12-c790698ff747"
    lvswap="e2b5b3e6-b8e6-4fd3-b539-120c1abbe18f"
    keymap="us-altgr-intl"
    # TODO: source code pro like in spacemacs?
    # console wants a PSF (PC Screen Font) font, which is a bitmap with a fixed width font
    # truetype/opentype are vector fonts
    # it would need (too much?) work to convert from psf to ttf
    # let's stick with terminus for now
    # ter-v14n: terminus font, v=all mappings/code pages, 8x14, normal style
    font="ter-v14n"
    # TODO: investigate superfluous kernel commandline options when fstab is in the initrd
    # TODO: ask user if swap and resume partitions are the same. For now we assume yes.
    kernelcmdline="root=UUID=${rootuuid} rootflag=subvol=${rootsubvol} ro rd.luks.uuid=${luksuuid} rd.luks.key=${lukskeyfile}:UUID=${luksuuid}:UUID=${lukskeyuuid} rd.lvm.lv=UUID=${lvbtrfssubvolumes} rd.lvm.lv=UUID=${lvswap} resume=UUID=${lvswap} rd.vconsole.keymap=${keymap} rd.vconsole.unicode rd.vconsole.font=${font}"
    printf "$kernelcmdline\n"
    printf "copy the above kernel cmdline, and press enter when done.\n"
    read
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" menuconfig" kernel-builder
         #su -l -c "KCONFIG_CONFIG=\"${kernelversion}.config\" make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" menuconfig" kernel-builder
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" oldconfig" kernel-builder
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" menuconfig" kernel-builder
   sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         make -C /usr/src/linux mrproper
   sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
        su -l -c "time make -j 5 -l 4 -C /home/kernel-builder/${kernelversion}" kernel-builder
    # su -l -c "time make -j 5 -l 4 -C /usr/src/linux O=\"/home/kernel-builder/linux-4.19.57-gentoo\"" kernel-builder
    # OR, to be checked
    # su -l -c "make -C /home/kernel-builder/linux-4.19.57-gentoo oldconfig" kernel-builder
    # su -l -c "make -j 5 -l 4 -C /home/kernel-builder/linux-4.19.57-gentoo" kernel-builder
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         make -C /home/kernel-builder/linux-4.19.57-gentoo modules_install
        #make -C /home/kernel-builder/linux-4.19.57-gentoo modules_install install
}

function fromtemporarysystem_cleankerneltree {
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     make -C /usr/src/linux distclean
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #    su -l -c "mkdir -v /home/kernel-builder/${kernelversion" kernel-builder
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
        su -l -c "make -C /home/kernel-builder/${kernelversion} O=\"/home/kernel-builder/${kernelversion}\" clean" kernel-builder
}

function fromtemporarysystem_mrproperkerneltree {
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
        #     make -C /usr/src/linux distclean
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "mkdir -v /home/kernel-builder/${kernelversion" kernel-builder
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /home/kernel-builder/${kernelversion} O=\"/home/kernel-builder/${kernelversion}\" mrproperclean" kernel-builder
}

function fromtemporarysystem_distcleankerneltree {
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
        #     make -C /usr/src/linux distclean
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "mkdir -v /home/kernel-builder/${kernelversion" kernel-builder
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /home/kernel-builder/${kernelversion} O=\"/home/kernel-builder/${kernelversion}\" distclean" kernel-builder
}

function fromtemporarysystem_displaygeneratedkernelcmdline {
    # NOTE: instead of using rd.luks.key to be able to use an encrypted LUKS keyfile,
    # we use a LUKS volume to protect an otherwise unencrypted keyfile
    # We probably wrongly only tested the syntax rd.luks.key=luks-uuid=/keypath:UUID=key-uuid
    # instead of trying rd.luks.key=/keypath:UUID=luk-uuid:UUID=key-uuid
    # NOTE: we need to add a service in the initrd, to unmount and close the fs on which is the keyfile and the corresponding LUKS volume, after root is mounted, and before switchroot,
    # see dracut boot process doc to check exactly what hook to use here

    # see kernelversion in fromtemporarysystem_buildfedoraconfigkernel
    modulesversion="$(echo ${kernelversion} | sed "s/^linux-//" )"
    # TODO: grab uuids from host
    # /dev/mapper/centranthus--vg-lvbtrfssubvolumes
    # /dev/mapper/centranthus--vg-lvswap
    rootuuid="ee86b4f1-4045-4bfb-aa44-06d7e219bf66"
    # TODO: can we use UUID_SUB to identify the subvolume?
    rootsubvol="/rootdir"
    lukskeyfile="/centranthus-luks-key.bin"
    luksbootuuid="6ece085b-8430-4f90-a89f-f8a0c637393e"
    luksmainuuid="08d89ede-9d79-43ff-926f-dea4e62b9611"
    lukskeyuuid="27095ed8-aa3c-463d-8cfe-c2ad500b2d1c"
    lvbtrfssubvolumes="ee86b4f1-4045-4bfb-aa44-06d7e219bf66"
    lvswap="7715f1f5-4abd-467e-b6bb-fd036095cc93"
    keymap="us-altgr-intl"
    # TODO: source code pro like in spacemacs?
    # console wants a PSF (PC Screen Font) font, which is a bitmap with a fixed width font
    # truetype/opentype are vector fonts
    # it would need (too much?) work to convert from psf to ttf
    # let's stick with terminus for now
    # ter-v14n: terminus font, v=all mappings/code pages, 8x14, normal style
    #font="ter-v14n"
    font="eurlatgr"
    # TODO: investigate superfluous kernel commandline options when fstab is in the initrd
    # TODO: ask user if swap and resume partitions are the same. For now we assume yes.
    #kernelcmdline="root=UUID=${rootuuid} rootflag=subvol=${rootsubvol} ro rd.luks.uuid=${luksuuid} rd.luks.key=${lukskeyfile}:UUID=${luksuuid}:UUID=${lukskeyuuid} rd.lvm.lv=UUID=${lvbtrfssubvolumes} rd.lvm.lv=UUID=${lvswap} resume=UUID=${lvswap} rd.vconsole.keymap=${keymap} rd.vconsole.unicode rd.vconsole.font=${font}"
    # rd.luks.key dracut doc could be outdate, both arch wiki and man systemd-cryptsetup-generator
    # give this format instead: rd.luks.key=uuid-of-luks-device=/path/to/key:keydevice
    # keydevice being either LABEL=keydevice-label or UUID=keydevice-uuid
    #kernelcmdline="ro rd.luks.uuid=${luksuuid} rd.luks.key=${luksuuid}=${lukskeyfile}:UUID=${lukskeyuuid} rd.vconsole.keymap=${keymap} rd.vconsole.unicode rd.vconsole.font=${font}"
    #kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${luksmainuuid}=${lukskeyfile}:UUID=${lukskeyuuid} rd.vconsole.keymap=${keymap} rd.vconsole.unicode rd.vconsole.font=${font}"
    #kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${luksmainuuid}=${lukskeyfile}:UUID=${lukskeyuuid}"
    #kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${lukskeyfile}:UUID=${lukskeyuuid}:UUID=${luksmainuuid}"
    # NOTE: dracut+systemd seem to ignore rd.luks.key=keyfile:UUID:UUID format,
    # using rd.luks.key=UUID=keyfile:UUID instead
    #kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${luksmainuuid}=${lukskeyfile}:UUID=${lukskeyuuid} rd.lvm.vg=${targethostname}-vg rd.break=initqueue"
    kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${luksmainuuid}=${lukskeyfile}:UUID=${lukskeyuuid} rd.lvm.vg=${targethostname}-vg root=UUID=${rootuuid} rootflags=subvol=${rootsubvol}"
    printf "$kernelcmdline\n"
    printf "copy the above kernel cmdline, and press enter when done.\n"
    read
}

function fromtemporarysystem_oldconfigkernel {
    # TODO: keymap
    # keymap inside kernel? (defkeymap.map, CONFIG_HW_CONSOLE) or simply in the initrd?
    # in initrd for now
    # TODO: kernel+dracut cmdline
    # cmdline options inside kernel? or simply in the initrd?
    # let's keep everyting in initrd, dracut can use /etc/cmdline.d/*.conf
    # and/or --kernel-cmdline (where are they stored in this case?)
    # it should work as the BOOT_IMAGE will be embdded in the kernel
    # thanks to CONFIG_INITRAMFS_SOURCE
    # see
    # see what? I can't remember :/
    # about the cmdline, it will instead be embedded inside the kernel,
    # and locked with CONFIG_CMDLINE_OVERRIDE

    kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" oldconfig KCONFIG_CONFIG=/home/kernel-builder/fedora-config" kernel-builder
}

function fromtemporarysystem_configkernel {
    # NOTE: EFI black screen:
    # https://forums.gentoo.org/viewtopic-t-1039388-start-0.html
    # the problem was config_efi_mixed CONFIG_EFI_MIXED set by fedora, which prevents the kernel to boot directly from EFI
    # NOTE: dracut uses an EFI stub and not "a bootloader that supports the EFI handover protocol" so we still disable CONFIG_EFI_MIXED when we use dracut to generate the EFI executable
    # TODO: keymap
    # keymap inside kernel? (defkeymap.map, CONFIG_HW_CONSOLE) or simply in the initrd?
    # in initrd for now
    # TODO: kernel+dracut cmdline
    # cmdline options inside kernel? or simply in the initrd?
    # let's keep everyting in initrd, dracut can use /etc/cmdline.d/*.conf
    # and/or --kernel-cmdline (where are they stored in this case?)
    # it should work as the BOOT_IMAGE will be embdded in the kernel
    # thanks to CONFIG_INITRAMFS_SOURCE
    # see
    # see what? I can't remember :/
    # about the cmdline, it will instead be embedded inside the kernel,
    # and locked with CONFIG_CMDLINE_OVERRIDE
    # NOTE TODO: use dracut to generate the EFI exe, and do not embed the initrd
    # or the cmdline in the kernel. Compiling the kernel is longer than building the initrd
    # with dracut, and thus it is much faster to change the kernel cmdline, the initrd itself.

    kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /usr/src/linux O=\"/home/kernel-builder/${kernelversion}\" menuconfig KCONFIG_CONFIG=/home/kernel-builder/fedora-config" kernel-builder
}

function fromtemporarysystem_backupconfig {
    # TODO: be smarter, use git instead
    printf "Enter kernel_config_description\n"
    read kernel_config_desc
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "cp -v /home/kernel-builder/${kernelversion}/.config /home/kernel-builder/${kernelversion}-${kernel_config_desc}.config" kernel-builder
}

function fromtemporarysystem_selectconfig {
    ls ${mount_root_path}/home/kernel-builder/*.config
    printf "Enter config filename:\n"
    read config_filename
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "cp -v /home/kernel-builder/${config_filename} /home/kernel-builder/${kernelversion}/.config" kernel-builder
}

function fromtemporarysystem_buildkernel {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "time make -j 5 -l 4 -C /home/kernel-builder/${kernelversion} KCONFIG_CONFIG=/home/kernel-builder/fedora-config" kernel-builder
}

function fromtemporarysystem_installkernel {
    # UEFI EDK2/OVMF sees .efi and bzImage but not other files, even if they are the same with a different name (other than .efi or bzImage)
    # why is bzImagine recognized too?
    # cp config, System.map and bzImage (in arch/x86/boot)
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "INSTALL_PATH=/home/kernel-builder/kernel.d -C /home/kernel-builder/${kernelversion} install" kernel-builder
    #su -l -c "INSTALL_PATH=/boot/EFI/gentoo make -C /home/kernel-builder/${kernelversion} install"
}

function fromtemporarysystem_uninstallkernel {
    printf "\n"
    # sudo rm -vf ${mount_root_path}/boot/EFI/gentoo/right-files
}

function fromtemporarysystem_installmodules {
    # TODO: use kernelversion instead of static path
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "make -C /home/kernel-builder/${kernelversion} modules_install"
}

function fromtemporarysystem_uninstallmodules {
    printf "\n"
    # sudo rm -vf ${mount_root_path}/lib/modules/right-dir
}

# TODO: function to ask for and to copy both font and keymap from fedora
# for now they get copied in buildinitrd
function fromtemporarysystem_copykeymap {
    # TODO: REMOVE ME, we already got fromtemporarysystem_copyxkb2kbdkeymaps
    #sudo mkdir -pv ${mount_root_path}/usr/local/share/keymaps/xkb
    # TODO: what are the diff between us-intl and ml-us-intl?
    # TODO: we copy the converted xkb2kbd keymap from fedora, we would need to package the converting script to do it properly
    # for keymap check https://src.fedoraproject.org/rpms/kbd/blob/master/f/kbd.spec
    #sudo cp -v /usr/lib/kbd/keymaps/xkb/us-intl.map.gz ${mount_root_path}/usr/local/share/keymaps/xkb/
    # Let's copy all keymaps instead
    sudo mkdir -pv ${mount_root_path}/usr/local/share/keymaps
    sudo cp -rv /usr/lib/kbd/keymaps/xkb/ ${mount_root_path}/usr/local/share/keymaps/
}

function fromtemporarysystem_configinitrd {
    # see kernelversion in fromtemporarysystem_buildfedoraconfigkernel
    declare -g kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
    declare -g modulesversion="$(echo ${kernelversion} | sed "s/^linux-//" )"
    # TODO: grab uuids from host
    # /dev/mapper/centranthus--vg-lvbtrfssubvolumes
    # /dev/mapper/centranthus--vg-lvswap
    declare -g rootuuid="bd7c6d8f-1bb7-4b00-8cc8-daf5a2bf0619"
    # TODO: can we use UUID_SUB to identify the subvolume?
    declare -g rootsubvol="/rootdir"
    declare -g lukskeyfile="/centranthus-luks-key.bin"
    declare -g luksbootuuid="bb12cf48-5806-4ae9-87ab-5b65b5f53c9f"
    declare -g luksmainuuid="6fa92177-bd5e-4a86-a977-067b72335bf5"
    declare -g lukskeyuuid="3a57a6d6-b74d-475e-8c77-1a587eec184f"
    declare -g lvbtrfssubvolumes="bd7c6d8f-1bb7-4b00-8cc8-daf5a2bf0619"
    declare -g lvswap="7715f1f5-4abd-467e-b6bb-fd036095cc93"
    declare -g keymap="us-altgr-intl"
    # TODO: source code pro like in spacemacs?
    # console wants a PSF (PC Screen Font) font, which is a bitmap with a fixed width font
    # truetype/opentype are vector fonts
    # it would need (too much?) work to convert from psf to ttf
    # let's stick with terminus for now
    # ter-v14n: terminus font, v=all mappings/code pages, 8x14, normal style
    #declare -g font="ter-v14n"
    # default fedora console font is eurlatgr
    declare -g font="eurlatgr"
    # TODO: investigate superfluous kernel commandline options when fstab is in the initrd
    # TODO: ask user if swap and resume partitions are the same. For now we assume yes.
    #declare -g kernelcmdline="root=UUID=${rootuuid} rootflag=subvol=${rootsubvol} ro rd.luks.uuid=${luksuuid} rd.luks.key=${lukskeyfile}:UUID=${luksuuid}:UUID=${lukskeyuuid} rd.lvm.lv=UUID=${lvbtrfssubvolumes} rd.lvm.lv=UUID=${lvswap} resume=UUID=${lvswap} rd.vconsole.keymap=${keymap} rd.vconsole.unicode rd.vconsole.font=${font}"
    #declare -g kernelcmdline="ro rd.luks.uuid=${luksuuid} rd.luks.key=${lukskeyfile}:UUID=${lukskeyuuid}:UUID=${luksuuid} rd.vconsole.keymap=${keymap}"
    declare -g kernelcmdline="ro rd.luks.uuid=${luksbootuuid} rd.luks.uuid=${luksmainuuid} rd.luks.key=${lukskeyfile}:UUID=${lukskeyuuid}:UUID=${luksmainuuid}"

}

function fromtemporarysystem_buildinitrd {
    printf "\n"
    # NOTE:
    # 22:55:18     dicebox | I think tomorrow I will restart my install, it's going to add some difficulty to make my system boot with a separate partition for /etc, yeah, not smart, but I needed to experiment │
    #│                     | the idea                                                                                                                                                                             │
    #               │22:56:40     dicebox | the reason it's not booting, I believe, is because I provide a fstab, and there is a swap in it, but no swapon binary in the initrd                                                  │
    #              │22:58:31     dicebox | and because I add the target's fstab in the initrd, the initrd's systemd tries to mount everything in it, but not in a /sysroot directory, over the initrd directories, which I feel │
    #             │                     | would make a mess too                                                                                                                                                                │
    #            │22:58:53     dicebox | I did that, because I had /etc and others like /usr and /var on their own partitions                                                                                                 │
    #           │22:59:16     dicebox | but systemd manages all that well, if only /etc is on /, not in a separate partition

    # output to ${mount_root_path}/home/kernel-builder/initramfs.d/initramfs.cpio
    # uncompressed output : --no-compress
    # can we build the initramfs with dracut as a normal user?
    # dracut --force --host-only --no-hostonly-cmdline
    # kernel command line
    # see man dracut : Boot parameters
    # dracut --print-cmdline
    # Specifying the root Device: This is the only option dracut really needs to boot from your root partition
    #       --kernel-cmdline <parameters>
    #       specify default kernel command line parameters
    #--hostonly-cmdline: Store kernel command line arguments needed in the initramfs
    # --no-hostonly-cmdline: Do not store kernel command line arguments needed in the initramfs
    #       --uefi-stub <FILE>
    #       Specifies the UEFI stub loader, which will load the attached kernel, initramfs and kernel command line and boots the kernel. The default is $prefix/lib/systemd/boot/efi/linux<EFI-MACHINE-TYPE-NAME>.efi.stub or
    #       $prefix/lib/gummiboot/linux<EFI-MACHINE-TYPE-NAME>.efi.stub
    #   --kernel-image <FILE>
    #       Specifies the kernel image, which to include in the UEFI executable. The default is /lib/modules/<KERNEL-VERSION>/vmlinuz or /boot/vmlinuz-<KERNEL-VERSION>
    # dracut.conf, dracut.conf.d:
    #       hostonly_cmdline="{yes|no}"
    # If set to "yes", store the kernel command line arguments needed in the initramfs
    #       kernel_cmdline="parameters"
    # Specify default kernel command line parameters
    #ssh access
    #dracut remote-luks module
    #https://github.com/dracut-crypt-ssh/dracut-crypt-ssh
    #https://github.com/dracut-crypt-ssh/dracut-crypt-ssh/pull/17
    #https://bugs.gentoo.org/617436

    # NOTE: secure boot setup for dracut
    # https://gist.github.com/Neurognostic/259f1ba1e568b31f0732a4a18f399f9e
    # dracut has to be run as root, right?
    # is --fstab really needed?
    # file dracut.cpio.gz says it's a ascii cpio archive, not a gz archive, is that expected?
    # NOTE: crypt-gpg dracut module does not permit systemd to unlock LUKS volumes with encrypted keyfiles
    # NOTE: lets try to do something about it. We encrypt the keyfile in a luks volume instead
    # NOTE: we copied 3 locale dirs to the initrd, we need to check exactly which one is needed
    # NOTE: we use dracut to generate the EFI executable, so we want to output to /efi/EFI/gentoo
    # TODO: remove --no-compress if it works, and use --xz instead
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         dracut --force --no-compress --host-only --no-hostonly-cmdline --no-hostonly-i18n --add \"crypt lvm\" --install \"gpg gpg-agent gpg-connect-agent cryptsetup ps locale grep nano tar blkid\" --include /usr/share/i18n /usr/share/i18n --include /usr/lib64/locale /usr/lib64/locale --include /usr/share/locale /usr/share/locale --kver ${modulesversion} --uefi --no-machineid /boot/EFI/gentoo/gentoo.efi
    # NOTE: this next one is for when we embed the initrd in the kernel
    #su -l -c "dracut --force --no-compress --host-only --no-hostonly-cmdline --no-hostonly-i18n --add \"crypt lvm\" --install \"gpg gpg-agent gpg-connect-agent cryptsetup ps locale grep nano tar blkid\" --include /usr/share/i18n /usr/share/i18n --include /usr/lib64/locale /usr/lib64/locale --include /usr/share/locale /usr/share/locale /home/kernel-builder/initramfs.d/dracut.cpio ${modulesversion}"
    #su -l -c "dracut --force --no-compress --host-only --no-hostonly-cmdline --no-hostonly-i18n --add \"crypt lvm\" --install \"gpg gpg-agent gpg-connect-agent cryptsetup ps locale grep nano tar blkid\" --include /usr/share/i18n /usr/share/i18n --include /usr/lib64/locale /usr/lib64/locale --include /usr/share/locale /usr/share/locale --fstab --add-fstab /etc/fstab /home/kernel-builder/initramfs.d/dracut.cpio ${modulesversion}"
    #su -l -c "dracut --force --no-compress --host-only --no-hostonly-cmdline --fstab --add-fstab /etc/fstab /home/kernel-builder/initramfs.d/dracut.cpio ${modulesversion}"
    #su -l -c "dracut --force --fstab /etc/fstab /home/kernel-builder/initramfs.d/dracut.cpio ${modulesversion}"
    #su -l -c "dracut --force --bzip2 --fstab --kernel-cmdline \"${kernelcmdline}\" /home/kernel-builder/initramfs.d/dracut.cpio.bz2 ${modulesversion}"
    #su -l -c "dracut --force --gzip --fstab --kernel-cmdline \"${kernelcmdline}\" /home/kernel-builder/initramfs.d/dracut.cpio.gz ${modulesversion}"
    # diff --list-modules and list of modules from previous command
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "dracut --list-modules --no-kernel"
    # TODO: include keymap /usr/lib/kbd/keymaps/xkb/us-intl.map.gz
    # font /usr/share/consolefonts/ter-v14n.psf.gz is automagically included!
    # TODO: edit kernelcmdline to use: the keymap above, luks, font
    # TODO: skipped rules files are actually not present, that's why they are skipped
    # let's try with whatever rules files dracut can find for now
    # TODO: should we include 40-gentoo.rules and/or 69-mooltipass.rules?
    # TODO: document dracut modules
    # Start by modules not included in the initrd for a reason or an other
#base
#bash
#biosdevname
#bootchart: TODO diagnose booting times?
#btrfs
#busybox
#caps
#cifs: TODO samba fs
#cio_ignore
#cms
#convertfs
#crypt
#crypt-gpg
#crypt-loop
#dasd
#dasd_mod
#dasd_rules
#dash: TODO replace bash with dash?
#dcssblk
#debug
#dm
#dmraid: TODO
#dmsquash-live
#dmsquash-live-ntfs: TODO
#dracut-systemd
#drm
#ecryptfs
#fcoe: TODO Fibre Channel over Ethernet
#fcoe-uefi: TODO Fibre Channel over Ethernet UEFI
#fips
#fs-lib
#fstab-sys
#gensplash
#i18n
#ifcfg
#img-lib
#integrity
#iscsi: TODO
#kernel-modules
#kernel-network-modules
#livenet
#lunmask
#lvm
#lvmmerge
#masterkey
#mdraid: TODO
#modsign: TODO add keys in /lib/modules/keys to the session keyring (what creates and fills /lib/modules/keys? what creates this session keyring? what is exactly that session?)
#multipath: TODO fail-over and load-balancing for network storage
#nbd: TODO network block device
#network: TODO base network things. Isn't systemd supposed to do what this does?
#nfs
#plymouth
#pollcdrom
#qemu
#qemu-net
#qeth_rules
#rescue
#resume
#rootfs-block
#securityfs
#selinux
#shutdown
#ssh-client
#stratis: TODO local storage manager (reuse LVM, plan to offer features found in ZFS and BTRFS)
#syslog
#systemd
#systemd-initrd
#systemd-networkd
#terminfo
#udev-rules
#uefi-lib
#url-lib
#usrmount
#virtfs
#watchdog
#zfcp
#zfcp_rules
#zipl
#znet
    # TODO: why do we need --no-kernel? otherwise it looks for
    # dracut: Cannot find module directory /lib/modules/5.3.6-200.fc30.x86_64/
    # which is a kernel outside of the chroot, or probably the running kernel?
}

function fromtemporarysystem_installinitrd {
    printf "\n"
    sudo cp ${mount_root_path}/home/kernel-builder/initramfs.d/dracut.cpio ${mount_root_path}/boot/EFI/gentoo/
}

function fromtemporarysystem_uninstallinitrd {
    printf "\n"
}

function fromtemporarysystem_copykernelinitrd2host {
    mkdir -pv ~/tmp/gis-kernelinitrd
    sudo cp ${mount_root_path}/boot/EFI/gentoo/ ~/tmp/gis-kernelinitrd
}

function fromtemporarysystem_mergeinitrdkernel {
    # set INITRAMFS_SOURCE with generated initrd
    # TODO: automagic kernel name/version and install_path
    printf "\n"
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
        su -l -c "time make -j 5 -l 4 -C /home/kernel-builder/${kernelversion}" kernel-builder
    sudo mkdir -pv ${mount_root_path}/boot/EFI/Gentoo
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
         su -l -c "INSTALL_PATH=/boot/EFI/gentoo make -C /home/kernel-builder/linux-4.19.57-gentoo install"
    #su -l -c "INSTALL_PATH=${mount_root_path}/boot/EFI/gentoo make -C /home/kernel-builder/linux-4.19.57-gentoo install"
    #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
    #     su -l -c "cp /boot/EFI/gentoo/vmlinuz-4.19.57-gentoo /boot/EFI/gentoo/gentoo.efi"
}

function fromtemporarysystem_editefi {
    printf "\n"
    # https://fedoraproject.org/wiki/Using_UEFI_with_QEMU
    # efibootmgr --create, or EFI on my dell lattitude, generates or corrupts created boot entries with the wrong UUID
    # creating the entry directly in the EFI setup does the trick
    # TODO: automagic disk, part, label and loader
    # RTFM: https://github.com/rhboot/efibootmgr
    # https://wiki.archlinux.org/index.php/EFI_system_partition#Mount_the_partition
    # https://github.com/systemd/systemd/pull/3757#issuecomment-234290236
    # TODO: A VERIFIER mount the ESP to /boot and/or  ${mount_root_path}/boot
    # TODO: A VERIFIER use INSTALL_PATH to install the kernel directly in /boot/EFI/Gentoo/
    # convert kernel to EFI executable: needs CONFIG_EFI_STUB=y
    # list efi entries
    # efibootmgr -v
    # add efi entry
    sudo efibootmgr --disk /dev/sde --part 1 --create --label "Gentoo Juju" --loader "\EFI\Gentoo\vmlinuz-4.19.57-gentoo"
    # update efi entry
}

function fromtemporarysystem_createdirssecureboot {
    sudo su -l -c "mkdir -v ${mount_root_path}/etc/efikeys"
    sudo su -l -c "mkdir -v ${mount_root_path}/etc/efikeys/private-newkeys"
    sudo su -l -c "mkdir -v ${mount_root_path}/etc/efikeys/public-newkeys"
    sudo su -l -c "mkdir -v ${mount_root_path}/etc/efikeys/oldkeys"
    sudo su -l -c "chmod -v -R 700 ${mount_root_path}/etc/efikeys"
}

function fromtemporarysystem_backupoldkeyssecureboot {
    sudo su -l -c "efi-readvar -v PK -o ${mount_root_path}/etc/efikeys/oldkeys/old_PK.esl"
    sudo su -l -c "efi-readvar -v KEK -o ${mount_root_path}/etc/efikeys/oldkeys/old_KEK.esl"
    sudo su -l -c "efi-readvar -v db -o ${mount_root_path}/etc/efikeys/oldkeys/old_db.esl"
    sudo su -l -c "efi-readvar -v dbx -o ${mount_root_path}/etc/efikeys/oldkeys/old_dbx.esl"
    sudo su -l -c "chmod -v -R 400 ${mount_root_path}/etc/efikeys/oldkeys"
}

function fromtemporarysystem_generatenewkeyssecureboot {
    openssl req -new -x509 -newkey rsa:2048 -subj "/CN=juju's platform key/" -keyout efikeys/private-newkeys/PK.key -out efikeys/public-newkeys/PK.crt -days 3650 -nodes -sha256
    openssl req -new -x509 -newkey rsa:2048 -subj "/CN=juju's key-exchange-key/" -keyout efikeys/private-newkeys/KEK.key -out efikeys/public-newkeys/KEK.crt -days 3650 -nodes -sha256
    openssl req -new -x509 -newkey rsa:2048 -subj "/CN=juju's kernel-signing key/" -keyout efikeys/private-newkeys/db.key -out efikeys/public-newkeys/db.crt -days 3650 -nodes -sha256
    chmod -v -R 400 efikeys/private-newkeys
    chmod -v -R 400 efikeys/public-newkeys/*.crt
}

function fromtemporarysystem_generatesiglistsecureboot {
    cert-to-efi-sig-list -g "$(uuidgen)" efikeys/public-newkeys/PK.crt efikeys/public-newkeys/PK.esl
    cert-to-efi-sig-list -g "$(uuidgen)" efikeys/public-newkeys/KEK.crt efikeys/public-newkeys/KEK.esl
    cert-to-efi-sig-list -g "$(uuidgen)" efikeys/public-newkeys/db.crt efikeys/public-newkeys/db.esl
    chmod -v -R 400 efikeys/public-newkeys/*.esl
}

function fromtemporarysystem_signsiglistsecureboot {
    sign-efi-sig-list -k efikeys/private-newkeys/PK.key -c efikeys/public-newkeys/PK.crt PK efikeys/public-newkeys/PK.esl efikeys/public-newkeys/PK.auth
    #sign-efi-sig-list -a -t "$(date --date='1 second' +'%Y-%m-%d %H:%M:%S')" -k efikeys/private-newkeys/PK.key -c efikeys/public-newkeys/PK.crt KEK efikeys/public-newkeys/KEK.esl efikeys/public-newkeys/KEK.auth
    sign-efi-sig-list -a -t "$(date +'%Y-%m-%d %H:%M:%S')" -k efikeys/private-newkeys/PK.key -c efikeys/public-newkeys/PK.crt KEK efikeys/public-newkeys/KEK.esl efikeys/public-newkeys/KEK.auth
    #sign-efi-sig-list -a -t "$(date --date='1 second' +'%Y-%m-%d %H:%M:%S')" -k efikeys/private-newkeys/KEK.key -c efikeys/public-newkeys/KEK.crt db efikeys/public-newkeys/db.esl efikeys/public-newkeys/db.auth
    sign-efi-sig-list -a -t "$(date +'%Y-%m-%d %H:%M:%S')" -k efikeys/private-newkeys/KEK.key -c efikeys/public-newkeys/KEK.crt db efikeys/public-newkeys/db.esl efikeys/public-newkeys/db.auth
    sign-efi-sig-list -k efikeys/private-newkeys/KEK.key -c efikeys/public-newkeys/KEK.crt dbx efikeys/oldkeys/old_dbx.esl efikeys/public-newkeys/old_dbx.auth
    chmod -v -R 400 efikeys/public-newkeys/*.auth
}

function fromtemporarysystem_convertnewkeyssecureboot {
    openssl x509 -outform DER -in efikeys/public-newkeys/PK.crt -out efikeys/public-newkeys/PK.cer
    openssl x509 -outform DER -in efikeys/public-newkeys/KEK.crt -out efikeys/public-newkeys/KEK.cer
    openssl x509 -outform DER -in efikeys/public-newkeys/db.crt -out efikeys/public-newkeys/db.cer
    chmod -v -R 400 efikeys/public-newkeys/*.cer
}

function fromtemporarysystem_mergekeyssecureboot {
    cat efikeys/oldkeys/old_KEK.esl efikeys/public-newkeys/KEK.esl > efikeys/public-newkeys/compound_KEK.esl
    cat efikeys/oldkeys/old_db.esl efikeys/public-newkeys/db.esl > efikeys/public-newkeys/compound_db.esl
    chmod -v -R 400 efikeys/public-newkeys/compound_*.esl
}

function fromtemporarysystem_signmergedkeyssecureboot {
    sign-efi-sig-list -k efikeys/private-newkeys/PK.key -c efikeys/public-newkeys/PK.crt KEK efikeys/public-newkeys/compound_KEK.esl efikeys/public-newkeys/compound_KEK.auth
    sign-efi-sig-list -k efikeys/private-newkeys/KEK.key -c efikeys/public-newkeys/KEK.crt db efikeys/public-newkeys/compound_db.esl efikeys/public-newkeys/compound_db.auth
    chmod -v -R 400 efikeys/public-newkeys/compound_*.auth
}

function fromtemporarysystem_protectkeyssecureboot {
    chmod -v -R 400 efikeys/public-newkeys
    find efikeys/ ! -wholename 'efikeys/SHA512SUM' -type f -exec sha512sum {} \; > efikeys/SHA512SUM
    chmod -v -R 400 efikeys
}

function fromtemporarysystem_selinux {
    printf "\n"
}

function fromtemporarysystem_lockefibiossetup {
    printf "\n"
}

function fromtemporarysystem_enablesecurebootsetupmode {
    printf "\n"
}

function fromtemporarysystem_rebootnow {
    printf "\n"
}

### NEWINFRASTRUCTURE_ATFIRSTBOOT ###

function atfirstboot_importsecurebootnewkeys {
    printf "\n"
}

function atfirstboot_installselinuxpolicy {
    printf "\n"
}

function atfirstboot_configureselinuxpolicy {
    printf "\n"
}

function atfirstboot_rebuildselinuxbase {
    printf "\n"
}

function atfirstboot_installselinuxbasepolicy {
    printf "\n"
}

function atfirstboot_updateworld {
    printf "\n"
}

function atfirstboot_updateconfigfiles {
    printf "\n"
}


### NEWINFRASTRUCTURE_ATSECONDBOOT ###

function atsecondboot_relabelselinux {
    printf "\n"
}

function atsecondboot_enableselinuxswapfile {
    printf "\n"
}


### NEWINFRASTRUCTURE_ATTHIRDBOOT ###

function atthirdboot_setselinuxbool {
    printf "\n"
}

function atthirdboot_createselinuxadministrator {
    printf "\n"
}

function atthirdboot_configurepam  {
    printf "\n"
} # ecryptfs, totp, mooltipass detection

function atthirdboot_createusers  {
    printf "\n"
} # one account per community?
# connected(irc_*,riot_*,mumble_*,ring_*),unsafe websearch,safe websearch,webveille(regular consulted webiste),webshopping,
# publicdata(downloads,local files transfers),personaldata(made$
# free software dev
# tmw
# minetest
# LoL
# current-job

function atthirdboot_grantselinuxprivileges {
    printf "\n"
}

# TODO
#function fromtemporarysystem_convertxkeyboardlayout{
#  printf "Welcome to fromtemporarysystem_convertxkeyboardlayout\n"
#  printf "\n"
#}
### NEWINFRASTRUCTURE_MENUATFIRSTBOOT ###
#  printf "A set of secureboot keys will get generated and their private part stored on your mooltipass\n"

# connected(irc_*,riot_*,mumble_*,ring_*),unsafe websearch,safe websearch,webveille(regular consulted webiste),webshopping,publicdata(downloads,local files transfers),personaldata(made$
