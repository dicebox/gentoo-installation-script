IMAGEFILE="/srv/backup/dar.tmp/image.iso"
EXTRACTEDISO="/srv/backup/dar.tmp/extracted.iso"
BURNER=/dev/sr0
GPGHOMEDIR="~/.gnupg"
GPGLOCALUSR=""
DVDISASTERNTHREADS=2 # no real rules here, I guess at most # threads of CPU minus 2 for the system? and reduce if IO can't follow. Check dvdisaster manual.

SELECTEDCONF=""
ETCCONF=/etc/dar-helper
HOMECONF=~/.config/dar-helper

TESTTYPE=""
