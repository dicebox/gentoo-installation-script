#!/bin/bash
# GPLv3+
# Copyright Julien Cerqueira 2019

### USER VARIABLES ###
declare -g targethostname=""

### NEWINFRASTRUCTURE_FROMRUNNINGSYSTEM ###
declare -g fedorabaseurl="https://download.fedoraproject.org/pub/fedora/linux/releases/32/Workstation/x86_64/iso/"
declare -g fedoraiso="Fedora-Workstation-Live-x86_64-32-1.6.iso"
declare -g fedorachecksum="Fedora-Workstation-32-1.6-x86_64-CHECKSUM"
declare -g fedoragpg="https://getfedora.org/static/fedora.gpg"
declare -g liveusbmedia="" # rename to liveusb_device
#targethostname=""
#mpusers="biosefiadmin ring0 remoteinstallssh"
declare -g remoteuser=""
declare -g remoteip=""

### NEWINFRASTRUCTURE_FROMTEMPORARYSYSTEM ###
#mp_udev_git=https://github.com/mooltipass/mooltipass-udev.git
#mp_udev_rules=mooltipass-udev/udev/69-mooltipass.rules
#system_udev_dir=/etc/udev/rules.d/
#mooltipy_pip=git+file:///run/media/liveuser/Gentoo\ Install/leaf/mooltipy.git/
#targethostname=""
declare -g btrfssubvol_part=""
declare -g pvname=""
declare -g vgname=""
#mpusers="tmproot tmpuser luksgpg root leaf"
# http://distfiles.gentoo.org/ redirects to gentoo.osuosl.org, which certificate is not valid for http://distfiles.gentoo.org/
# but it redirects to https
gentoobaseurl="http://distfiles.gentoo.org/"
#declare -g gentoostage3="releases/amd64/autobuilds/current-stage3-amd64-hardened+nomultilib/" #rename to gentoostage3dir
# NOTE: use this next one for a hardened selinux nomultilib stage3
declare -g gentoostage3="releases/amd64/autobuilds/current-stage3-amd64-hardened-selinux+nomultilib/" #rename to gentoostage3dir
#gentoostage3file="stage3-amd64-hardened+nomultilib-*.tar.xz"
declare -g gentoosnapshot="snapshots/" #rename to gentoosnapshotdir
declare -g gentoosnapshotfile="portage-latest.tar.xz"
# We need hkps, but cannot get it to work, are we missing something? a certificate maybe?
# We cannot get reliable behaviour, there are frequent no data replies when requesting the keys
# Lets try without specifying a server
#declare -g keyserver="hkp://keyserver.ubuntu.com:80" # port is important to not get blocked by firewalls
# https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f#mitigations
#declare -g keyserver="hkps://keys.openpgp.org"
# Gentoo keys are not yet on that server
# https://gentoo.org/news/2019/07/03/sks-key-poisoning.html
declare -g keyserver="hkps://keys.gentoo.org"
declare -g keystage3="0xBB572E0E2D182910"
declare -g keysnapshot="0xDB6B8C1F96D8BF6D"
#declare -g sigstage3="stage3-amd64-hardened+nomultilib-*.tar.xz.DIGESTS.asc"
#declare -g sigstage3="stage3-amd64-hardened-selinux+nomultilib-*.tar.xz.DIGESTS.asc"
declare -g sigstage3="stage3-*.tar.xz.DIGESTS.asc"
declare -g sigsnapshot="portage-latest.tar.xz.gpgsig"
declare -g bootkey="" # rename to bootkey_device
#bootkey_partition=""
#efibootdir="$(mktemp)" #replace from /tmp/efiboot
declare -g mainstorage="" # rename to mainstorage_device
declare -g mount_subvolumes_path="/mnt/subvolumes"
declare -g mount_root_path="/mnt/gentoo"
# We abandonned the idea to have partitions for any dir in /, in /usr and in /usr/local.
# We instead selected the dirs which didn't need to be writable at the same time as others.
# /usr is the exception. We keep it on its own partition, to make createfstab simpler.
# It will also make mounting /usr from the network easier
# no need for a separate volume for /etc, as it can be changed from rw to ro and back at will,
# thanks to bind mounts
# /home, /usr, /var, /usr/local and /usr/src, and maybe some subdirs in /var as well,
# still make sense to be on a different volume, as it permits to manage storage space more
# efficiently (/var and its subdirs, /usr/src), and shared across different machines (/home, /usr/local)
declare -g subvolumes="\
  rootdir\
  home\
  usr\
  usrlocal\
  usrsrc\
  var\
  vardistfiles\
  varbinpkgs\
"
declare -g rootdirs="\
  home\
  usr\
  var\
"
declare -g usrdirs="\
  local\
  src\
"

# TODO: modify create subvolumes, mount points, mount and unmount functions to
# take into account the new var subdirs mount points and volumes
declare -g vardirs="\
        cache/distfiles\
        cache/binpkgs\
"

declare -gA userchoice

declare -g timezone=""
declare -g lang=""
declare -g lcmessages=""
declare -g keymap=""

# TODO: add a variable to execute commands inside the container
# currently using systemd-nspawn
# alternatives: machinctl shell, systemd-run
# as far as I (Julien C.) understand, as of Sep 3 2019, any of those would be fine
# systemd-nspawn avoid needing to let the container run, which involves mounting and unmounting virtual FS for every commands
# We stick to systemd-nspawn until someone with a better understanding of differences between all choices manages to convince me (Julien C.)
# that there is a better choice.
# We include sudo for now
# will ${mount_root_path} be updated? it will, as mount_root_path is set statically before run_inside_container
declare -g run_inside_container="sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation"
