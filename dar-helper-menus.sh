function menu_main {
    clear
    declare -a ordrarray=(
        exit
        break
        startshell
        printuser
        askconfigtype
        createconfig
        loadconfig
        printconfig
        printparams
        mediainfo
        formatbluray
        checkwrite
        givewrite
        removewrite
        signchecksum
        signchecksumasroot
        verifysig
        verifysigasroot
        verifychecksum
        createiso
        augmentiso
        burndiskdryrun
        burndisk
        dvdisastermediuminfo
        dvdisasterscan
        dvdisasterextractimage
        asktesttype
        askimagetype
        dvdisastertestimage
        printdisklabel
        cleanslicechecksumsig
        cleanimages
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}
