function startshell {
    /usr/bin/bash
}

function printuser {
    whoami
}

function askconfigtype {
    # local or global
    # accept only 1 or 2 as input, and 0 too, to cancel
    while true; do
        printf "Please enter 0 to cancel, 1 to set a local (in ~/.config/) config or 2 to set a global (in /etc/) config.\n"
        read SELECTEDCONF
        clear
        printf "SELECTEDCONF=${SELECTEDCONF}\n"
        case ${SELECTEDCONF} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting a local config.\n"
                SELECTEDCONF=${HOMECONF}
                break
                ;;
            2)
                userid="$(id -u)"
                if [ "${userid}" -ne "0" ]; then
                    clear
                    printf "You need to be root to select this type.\n"
                    continue
                fi
                printf "Setting a global config.\n"
                SELECTEDCONF=${ETCCONF}
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "SELECTEDCONF=${SELECTEDCONF}\n"
}

function createconfig {
    printf "\n"
    if [ "${SELECTEDCONF}" = "" ]; then
        printf "Select a configuration file type first.\n"
        return
    fi
    if [ -f ${SELECTEDCONF} ]; then
        printf "Configuration file already exists.\n"
        return
    fi
    mkdir -v ~/.config
    strconfig="IMAGEFILE=${IMAGEFILE}\n"
    strconfig="${strconfig}EXTRACTEDISO=${EXTRACTEDISO}\n"
    strconfig="${strconfig}BURNER=${BURNER}\n"
    strconfig="${strconfig}GPGHOMEDIR=${GPGHOMEDIR}\n"
    strconfig="${strconfig}GPGLOCALUSR=${GPGLOCALUSR}\n"
    strconfig="${strconfig}DVDISASTERNTHREADS=${DVDISASTERNTHREADS}\n"
    touch ${SELECTEDCONF}
    printf "${strconfig}" > ${SELECTEDCONF}
    ls -l ${SELECTEDCONF}
}

function loadconfig {
    printf "\n"
    if [ -f ${ETCCONF} ]; then
        printf "Sourcing ${ETCCONF}. Potentially overriding default configuration.\n"
        . ${ETCCONF}
    fi
    if [ -f ${HOMECONF} ]; then
        printf "Sourcing ${HOMECONF}. Potentially overriding default configuration and /etc/dar-helper.\n"
        . ${HOMECONF}
    fi

}

function printconfig {
    printf "IMAGEFILE=${IMAGEFILE}\n"
    printf "EXTRACTEDISO=${EXTRACTEDISO}\n"
    printf "BURNER=${BURNER}\n"
    printf "GPGHOMEDIR=${GPGHOMEDIR}\n"
    printf "GPGLOCALUSR=${GPGLOCALUSR}\n"
    printf "DVDISASTERNTHREADS=${DVDISASTERNTHREADS}\n"
}

function printparams {
    printf "SLICEPATH=${SLICEPATH}\n"
    printf "SLICEBASENAME=${SLICEBASENAME}\n"
    printf "SLICENUMBER=${SLICENUMBER}\n"
    printf "SLICENUMBERWITHLEADINGZEROS=${SLICENUMBERWITHLEADINGZEROS}\n"
    printf "SLICEEXTENSION=${SLICEEXTENSION}\n"
    printf "SLICECONTEXT=${SLICECONTEXT}\n"
    printf "SLICEFULLURL=${SLICEFULLURL}\n"
}

function mediainfo {
    dvd+rw-mediainfo ${BURNER}
}

function formatbluray {
    # NOTE/TODO: does not work from systemd-nspawn. Why?
    # https://wiki.archlinux.org/index.php/systemd-nspawn#Run_docker_in_systemd-nspawn
    # we might need to add some capabilities and whitelist some syscall
    # default capabilities list in man systemd-nspawn
    # which ones of each?
    # capabilities:
    # CAP_SYS_RAWIO - sounds interesting, it seems it is not in the default set
    # syscall: see with strace?
    # growisofs seems to work when ran with strace
    # Actually, it works, but it takes a longer time than what dvd+rw-format,
    # or growisofs, expect. Just wait for the drive to "wake up"
    # that's weird behaviour that I've never experienced running commands from host
    # for now, it seems that only the first access is problematic
    # possibly the first write access
    dvd+rw-format -ssa=default ${BURNER}
}

function checkwrite {
    getfacl ${SLICEPATH}
}

function givewrite {
    su -l -c "setfacl --modify user:$(whoami):rwx ${SLICEPATH}"
}

function removewrite {
    su -l -c "setfacl --modify user:$(whoami): ${SLICEPATH}"
}

function signchecksum {
    # NOTE: do gpg output the sig in the right directory?
    # NOTE: we want a dedicated homedir, to keep it encrypted as much as possible.
    # We probably want a dedicated gnupg homedir for each gnupg use we have for that same reason.
    # NOTE: we still want to explicitly tell which local-user we want to use, to avoid any
    # unexpected behaviour, which could occur in case we use that dedicated gnupg homedir for
    # an other gnupg local-user.
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --sign --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    gpg --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function signchecksumasroot {
    # NOTE: do gpg output the sig in the right directory?
    # NOTE: we want a dedicated homedir, to keep it encrypted as much as possible.
    # We probably want a dedicated gnupg homedir for each gnupg use we have for that same reason.
    # NOTE: we still want to explicitly tell which local-user we want to use, to avoid any
    # unexpected behaviour, which could occur in case we use that dedicated gnupg homedir for
    # an other gnupg local-user.
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --sign --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    GPGHOMEDIR=/home/root-backups/.gnupg
    gpg --homedir ${GPGHOMEDIR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    ls -lh ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig
}

function verifysig {
    printf "\n"
    gpg --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function verifysigasroot {
    printf "\n"
    GPGHOMEDIR=/home/root-backups/.gnupg
    gpg --homedir ${GPGHOMEDIR} --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function verifychecksum {
    awk "{print \$1 \" ${SLICEPATH}/\"\$2}" ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512 | sha512sum --check -
}

function createiso {
    # NOTE: where do we create the image file?
    # NOTE: what directory do we backup?
    mkisofs -iso-level 3 -dir-mode 0500 -file-mode 0400 -new-dir-mode 0500 -o ${IMAGEFILE} -m lost+found -m ${IMAGEFILE} -m ${EXTRACTEDISO} /srv/backup/dar.tmp
}

function augmentiso {
    # NOTE: unable to use the long option --method, has to rely on -m
    dvdisaster --image ${IMAGEFILE} -mRS03 --create --threads ${DVDISASTERNTHREADS}
}

function burndiskdryrun {
    growisofs -dry-run -dvd-compat -Z ${BURNER}=${IMAGEFILE}
}

function burndisk {
    # NOTE/TODO: does not work from systemd-nspawn. Why?
    growisofs -dvd-compat -Z ${BURNER}=${IMAGEFILE}
}

function dvdisastermediuminfo {
    dvdisaster --device ${BURNER} --medium-info
}

function dvdisasterscan {
    dvdisaster --device ${BURNER} --scan
}

function dvdisasterextractimage {
    dvdisaster --device ${BURNER} --image ${EXTRACTEDISO} --read
}

function asktesttype {
    # quick or normal
    # accept only 1 or 2 as input, and 0 too, to cancel
    while true; do
        printf "Please enter 0 to cancel, 1 to set a quick test or 2 to set a long test.\n"
        read TESTTYPE
        clear
        printf "TESTTYPE=${TESTTYPE}\n"
        case ${TESTTYPE} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting quick test.\n"
                TESTTYPE="=q"
                break
                ;;
            2)
                printf "Setting long test.\n"
                TESTTYPE=""
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "TESTTYPE=${TESTTYPE}\n"
}

function askimagetype {
    printf "\n"
    # generated or extracted
    # accept only 1 or 2 as input, and 0 too, to cancel
        while true; do
        printf "Please enter 0 to cancel, 1 to set an extracted iso or 2 to set a generated iso.\n"
        read IMAGETYPE
        clear
        printf "IMAGETYPE=${IMAGETYPE}\n"
        case ${IMAGETYPE} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting extracted iso image.\n"
                IMAGETYPE=${EXTRACTEDISO}
                break
                ;;
            2)
                printf "Setting generated iso image.\n"
                IMAGETYPE=${IMAGEFILE}
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "IMAGETYPE=${IMAGETYPE}\n"

}

function dvdisastertestimage {
    # add option to choose which image to test: one to be burnt or one extracted
    # add option to choose if it's a quick test or not
    # use --test=q for quick test
    dvdisaster --image ${IMAGETYPE} --test ${TESTTYPE}
}

function printdisklabel {
    # string to write on the disk or in its accompanying note
    printf "Label for this disk: ${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}\n"
}

function cleanslicechecksumsig {
    rm -v ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function cleanimages {
    rm -v ${EXTRACTEDISO}
    rm -v ${IMAGEFILE}
}

function f {
    printf "\n"
}

