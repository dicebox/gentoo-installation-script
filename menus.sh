# GPLv3+
# Copyright Julien Cerqueira 2019
# TODO: add some colors in printf output

function ask_useragreement_torun {
    clear
    printf "${1}"
    printf "\n"
    printf "Do you agree to run ${2}?\n"
    printf "\n"
    printf "Enter 'YES' to proceed or press Enter to return to the menu.\n"
    useragreement=""
    read useragreement
    clear
    if [ "${useragreement}" == "YES" ];then
        ${2}
    fi
}

function display_menu {
    # $1: menu's function name
    # $2: reference of a table with functions and corresponding messages if any
    local menu_name="${1}"
    local -n ordrarray_ref=${2}
    local -i ordrarray_len=${#ordrarray_ref[@]}
    local -n dsctable_ref=${3}
    local -n cfrmtable_ref=${4}
    local choice
    local valid=""
    local msg
    while true; do
        msg="Welcome to ${menu_name}\n"
        # Generate the different choices and their corresponding index to enter
        for i in ${!ordrarray_ref[@]}; do
            msg="${msg}${dsctable_ref[${ordrarray_ref[${i}]}]}[${i}] "
        done
        msg="${msg}\n\nPlease enter the number corresponding to an entry."
        msg="${msg}\n\nYour previous choice: ${userchoice[${menu_name}]}"
        msg="${msg}\nYour choice: "
        printf "${msg}"
        read choice
        # Filter out unrecognized inputs
        for i in ${!ordrarray_ref[@]}; do
            if [ "${choice}" == "${i}" ]; then
                valid=1
                break
            fi
        done
        # Reset the input filter, store the index of the selected function and call it
        if [ ${valid} ]; then
            clear
            valid=""
            userchoice[${menu_name}]=${choice}
            if [ "${cfrmtable_ref[${ordrarray_ref[${choice}]}]+isset}" ]; then
                ask_useragreement_torun "${cfrmtable_ref[${ordrarray_ref[${choice}]}]}" ${ordrarray_ref[${choice}]}
            else
                ${ordrarray_ref[${choice}]}
            fi
            printf "\n ### MENU ### \n"
        fi
    done
}

function display_menu_simple {
    # $1: menu's function name
    # $2: reference of a table with functions and corresponding messages if any
    local menu_name="${1}"
    local -n ordrarray_ref=${2}
    local -i ordrarray_len=${#ordrarray_ref[@]}
    local choice
    local valid=""
    local msg
    while true; do
        msg="Welcome to ${menu_name}\n"
        # Generate the different choices and their corresponding index to enter
        for i in ${!ordrarray_ref[@]}; do
            msg="${msg}${ordrarray_ref[${i}]}[${i}] "
        done
        msg="${msg}\n\nPlease enter the number corresponding to an entry."
        msg="${msg}\n\nYour previous choice: ${userchoice[${menu_name}]}"
        msg="${msg}\nYour choice: "
        printf "${msg}"
        read choice
        # Filter out unrecognized inputs
        for i in ${!ordrarray_ref[@]}; do
            if [ "${choice}" == "${i}" ]; then
                valid=1
                break
            fi
        done
        # Reset the input filter, store the index of the selected function and call it
        if [ ${valid} ]; then
            clear
            valid=""
            userchoice[${menu_name}]=${choice}
            ${ordrarray_ref[${choice}]}
            printf "\n ### MENU ### \n"
        fi
    done
}

function fromtemporarysystem_menubase {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_setupdevenv
        fromtemporarysystem_setupnetwork
        fromtemporarysystem_setupsshaccess
        fromtemporarysystem_installmooltipass
        fromtemporarysystem_settargethostname
        fromtemporarysystem_generatepassphrases
        fromtemporarysystem_changepasswords
        fromtemporarysystem_downloadgentooinstallationfiles
        fromtemporarysystem_receivegentookeys
        fromtemporarysystem_verifygentooinstallationfilesdownloads
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menuusbbootkey {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_selectusbbootkey
        fromtemporarysystem_createtableusbbootkey
        fromtemporarysystem_setpartitionsizeusbbootkey
        fromtemporarysystem_partitionusbbootkey
        fromtemporarysystem_formatusbbootkey
        fromtemporarysystem_mountusbbootkey
        fromtemporarysystem_storemainlukskey
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menumainstorageluks {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_selectmainstorage
        fromtemporarysystem_preparemainstorageforluks
        fromtemporarysystem_createmainluks
        fromtemporarysystem_backupmainluksheader
        fromtemporarysystem_verifyluksheaderbackup
        fromtemporarysystem_unlockmainluks
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menumainstoragelvm {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_status_dm-event.service
        fromtemporarysystem_status_lvm2-monitor.service
        fromtemporarysystem_configurehostlvm.conf
        fromtemporarysystem_createpv
        fromtemporarysystem_createvg
        fromtemporarysystem_setthinpoolsize
        fromtemporarysystem_setthinpoolchunksize
        fromtemporarysystem_createthinpool
        fromtemporarysystem_setswapsize
        fromtemporarysystem_createswap
        fromtemporarysystem_setrootsize
        fromtemporarysystem_createroot
        fromtemporarysystem_sethomesize
        fromtemporarysystem_createhome
        fromtemporarysystem_setoptsize
        fromtemporarysystem_createopt
        fromtemporarysystem_setsrvsize
        fromtemporarysystem_createsrv
        fromtemporarysystem_setusrsize
        fromtemporarysystem_createusr
        fromtemporarysystem_setvarsize
        fromtemporarysystem_createvar
        fromtemporarysystem_setbtrfssubvolumessize
        fromtemporarysystem_createbtrfs
        fromtemporarysystem_setext4size
        fromtemporarysystem_createext4
        fromtemporarysystem_backuplvmheader
        fromtemporarysystem_verifylvmheaderbackup
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menumainstorageformatmount {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_formatswap
        fromtemporarysystem_formatroot
        fromtemporarysystem_formathome
        fromtemporarysystem_formatopt
        fromtemporarysystem_formatsrv
        fromtemporarysystem_formatusr
        fromtemporarysystem_formatvar
        fromtemporarysystem_formatbtrfs
        fromtemporarysystem_createbtrfssubvolumes
        fromtemporarysystem_create_root_mountpoints
        fromtemporarysystem_formatext4
        fromtemporarysystem_activateswap
        fromtemporarysystem_mountlvmvolumes
        fromtemporarysystem_mountbtrfssubvolumes
        fromtemporarysystem_mountext4
        fromtemporarysystem_mountefi
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menustage3portagesnapshot {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_unpackstage3
        fromtemporarysystem_copyusrlocaltoopt
        fromtemporarysystem_unpackportagesnapshot
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menu {
        declare -a ordrarray=(
        exit
        break
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menutools {
    declare -a ordrarray=(
        exit
        break
        menubackup_selectbupversion
        menubackup_selectdarversion
        menubackup_installtools
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menugitconfig {
    declare -a ordrarray=(
        exit
        break
        menubackup_createrootgituser
        menubackup_gitconfigrootgit
        menubackup_gitconfigenableinclude
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menumooltipassdb {
        declare -a ordrarray=(
        exit
        break
        menubackup_backupmooltipass
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menulukskeyheaders {
    declare -a ordrarray=(
        exit
        break
        menubackup_createbaremainluksgitrepo
        menubackup_createlukstmpdir
        menubackup_createmainluksworkdirgitrepo
        menubackup_cpmainlukskeytogitrepo
        menubackup_addcommitpushmainlukskeytogitrepo
        menubackup_rmlukstmpdir
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menulvmconfig {
    declare -a ordrarray=(
        exit
        break
        menubackup_createlvmbackuparchivedirs
        menubackup_cplvmbackuparchivetochroot
        menubackup_createbarelvmgitrepo
        menubackup_createlvmtmpdir
        menubackup_createlvmworkdirgitrepo
        menubackup_cpvgcfgbackuptogitrepo
        menubackup_addcommitpushvgcfgbackuptogitrepo
        menubackup_rmlvmtmpdir
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

# TODO: script/function to quickly repartition/reformat the disk the way it was
# and to do a system full restoration

function menubackup_menukernelconfig {
    declare -a ordrarray=(
        exit
        break
        menubackup_createbarekernelgitrepo
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menusystemdatasnapshotspre {
    declare -a ordrarray=(
        exit
        break
        menubackup_createsystemsnapshots
        menubackup_activatesystemsnapshots
        menubackup_createsystemsnapshotsmountpoint
        menubackup_mountsystemsnapshots
        menubackup_createdatasnapshot
        menubackup_activatedatasnapshot
        menubackup_createdatasnapshotmountpoint
        menubackup_mountdatasnapshot
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menubupsystemdata {
    declare -a ordrarray=(
        exit
        break
        menubackup_createsystembuprepo
        menubackup_bupindexsavesystem
        menubackup_createdatabuprepo
        menubackup_bupindexsavedata
    )
    # TODO/NOTE: bup fsck --jobs=N;bup fsck --generate --jobs=N;bup fsck --repair --jobs=N
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menubupmirror {
    declare -a ordrarray=(
        exit
        break
        menubackup_selectmirrorusbdrive
        menubackup_preparemirrorusbdrive
        menubackup_mountmirrorusbdrive
        menubackup_updatesystembuprepotousbdrive
        menubackup_updatedatabuprepotousbdrive
    )
    # TODO/NOTE: bup fsck --jobs=N;bup fsck --repair --jobs=N
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menumirrorsnapshotpre {
    declare -a ordrarray=(
        exit
        break
        menubackup_createbupmirrorsnapshot
        menubackup_activatebupsnapshots
        menubackup_mountbupmirrorsnapshot
    )
    # TODO: create mount point
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

# TODO: use dar thin vol for dar create, dar restore, bup restore and final diff
function menubackup_menutempthinvolpre {
    declare -a ordrarray=(
        exit
        break
        menubackup_createdarthinvol
        menubackup_createmountpoint
        menubackup_mountdarthinvol
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menudarsystemdataslices {
    declare -a ordrarray=(
        exit
        break
        menubackup_createbackupuser
        menubackup_createbackupusergpgkeys
        menubackup_showbackupusergroups
        menubackup_addbackupusertogroup11
        menubackup_delbackupuserfromgroup11
        menubackup_installdarhelper
        menubackup_setrootpasswd
        menubackup_createbupsystemdararchive
        menubackup_checksumdararchivesslice
        menubackup_signdararchiveslice
        menubackup_createbupdatadararchive
        menubackup_checksumdararchivesslice
        menubackup_signdararchiveslice
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menuformatdiskaugmentburnverifyiso9660 {
    declare -a ordrarray=(
        exit
        break
        menubackup_formatbluray
        menubackup_mediainfo
        menubackup_createiso9660image
        menubackup_augmentiso9660image
        menubackup_burnimagetobluray
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menurestoredarsystemdata {
    declare -a ordrarray=(
        exit
        break
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menurestorebupsystemdata {
    declare -a ordrarray=(
        exit
        break
        menubackup_bupfullrestoresystem
        menubackup_buppartialrestoresystem
        menubackup_bupfullrestoredata
        menubackup_buppartialrestoredata
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menusystemdataverifyrestore {
    # NOTE: are this menu and its entries needed?
    declare -a ordrarray=(
        exit
        break
        menubackup_manualsystembackupdiff
        menubackup_manualdatabackupdiff
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menutempthinvolpost {
    declare -a ordrarray=(
        exit
        break
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menusystemsnapshotspost {
    declare -a ordrarray=(
        exit
        break
        menubackup_unmountsystemsnapshots
        menubackup_removesystemsnapshotsmountpoint
        menubackup_deactivatesystemsnapshots
        menubackup_removesystemsnapshots
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menudatasnapshotpost {
    declare -a ordrarray=(
        exit
        break
        menubackup_unmountdatasnapshot
        menubackup_removedatasnapshotmountpoint
        menubackup_deactivatedatasnapshot
        menubackup_removedatasnapshot
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menumirrorsnapshotpost {
    declare -a ordrarray=(
        exit
        break
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menumirrorunmountpoweroff {
    declare -a ordrarray=(
        exit
        break
        menubackup_unmountmirrorusbdrive
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menubupsystemdatamirrordiagnostics {
    declare -a ordrarray=(
        exit
        break
        menubackup_bupfscksystem
        menubackup_manualbupdiffsystem
        menubackup_bupfsckdata
        menubackup_manualbupdiffdata
        menubackup_bupfscksystem
        menubackup_manualbupdiffsystem
    )
    # TODO/NOTE: bup fsck --jobs=N;bup fsck --generate --jobs=N;bup fsck --repair --jobs=N
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menudarsystemdatadiagnostics {
    declare -a ordrarray=(
        exit
        break
        menubackup_dartestsystem
        menubackup_dardiffsystem
        menubackup_darextractsystem
        menubackup_dartestdata
        menubackup_dardiffdata
        menubackup_darextractdata
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menubackup_menudvdisasterdiagnostics {
    declare -a ordrarray=(
        exit
        break
        menubackup_dvdisasterscan
        menubackup_dvdisasterverify
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menubackup {
    printf "\n"
    # NOTE: rename data bup repo to pim bup repo? PIM as in personal information management/manager. do we care?
    # TODO: mount snapshots read-only and make sure they are used instead of original volume
    # TODO / NOTE: we can/should/do encrypt user chat logs before backing them up
    # NOTE: how do we encrypt stuff? manually with gpg? git-crypt?
    # NOTE: we encrypt with a working checkout in an fscrypt encrypted directory
    # NOTE: and with a git-remote-gcrypt encrypted remote
    # NOTE: then files, especially luks keys and eventually private and symmetric keys
    # NOTE: would have been written once on a ssd/hdd
    # NOTE: we create a tmp working dir in /tmp which is tmpfs so nothing is written
    # NOTE: in clear on a ssd/hdd
    # NOTE: this is not a method we want for users
    # NOTE: we need to have data encrypted before it is stored in git
    # NOTE: this eliminate git-crypt as well
    # NOTE: we need to use gpg manually then
    # NOTE: we still want fscrypt encrypted dirs to hold the git working dirs
    # NOTE: we might still want git-remote-gcrypt too, but we need to see what it looks like
    #
    # NOTE: backup
    # we are able to diff the original data with the restored data
    # NOTE: archive
    # we cannot practically diff the original data and the restored data
    # we then rely on the fact that we diffed the original data when we introduced the
    # data in the archive, and on recovery blocks thanks to dvdisaster and bup fsck --repair
    #
        # NOTE: keep /mnt for manual mounts, otherwise use mktmp -d
    #
    # NOTE: why do we treat each volume separately?
    # this way we do not backup virtual filesystems
    # we can exclude virtual filesystems
    # actually, they shouldn't be part of the snapshot anyway
    #
    # NOTE: why do we treat /etc separately?
    # sensitive informations are excluded, hosted in /srv/etc and managed in their own git repo
    # modified config files are managed in their own git repo with scripts to generate them
    # /etc could be modified more frequently than the other directories of /, but
    # between bup and lvm snapshots, making a backup of / instead of /etc should not have
    # any meaningful impact either on cpu, ram, and storage
    # is this really true for cpu and ram?
    #
    #     /,/opt,/usr,/var
    # what to exclude?
    #
    #
    # NOTE: /srv/etc holds private keys, private certificates, files with passwords in clear
    #   and other stuff we do not want to backup without encrypting them first
    #
    # NOTE: we do not backup /home because most of it inside it would be in git repos with
    #   remotes in /srv
    #   Things which are not do not need to be backed up
    #
    #     /home/,/srv
    #       exclude /srv/backup
    # NOTE: bup backups from LVM snapshots for consistency
    #
    #
    # "clone" to external usb drive:
    # system: clone
    # data: only add branches, do not remove any
    # use bup-get, which is only available since bup 0.30
    #
    # NOTE: empty commits
    # johill  dicebox: you might be better off teaching save to just not make a commit then?
    # johill  dicebox: which you kinda could do yourself, by using 'bup save -t' and then committing the tree with "git commit-tree"
    # @rlb    johill: guess you could also check the save after the fact and then "bup rm" the tip if it's unwanted...
    # johill  or even just 'git reset --soft HEAD~1'
    # @rlb    OK, sounds plausible -- though doesn't really "hurt" as long as you still evict enough stuff to get the size down sufficiently.
    # johill  you end up with dangling commit objects, but meh...
    # @rlb    or "git branch -f name HEAD~" or... :)
    # johill  bup save -t would save you the dangling commit objects, at the expensive of all your normal commit objects not being packed
    # johill  (which can actually be a good thing, if you have rev-list heavy workloads)
    # johill  dicebox: but I guess patches welcome for a --no-empty-commit option or so
    # johill  (but I can see why that's not the default "did I actually save nothing there, or was my cronjob broken?")
    # dicebox I thought that it could be interesting to keep the first and last identical commits, and remove all in-between, that could help improve confidence in things working correctly
    # johill  I guess you could script even that pretty easily with git
    # johill  but it ends up being a rebase all the time
    #
    #
    #
    #
    #
    #
    # dar system
    # dar data
    # NOTE: linux and bd-r
    # http://fy.chalmers.se/~appro/linux/DVD+RW/Blu-ray/
    #
    # NOTE: apparently we want to use the original cdrecord
    # http://cdrtools.sourceforge.net/private/linux-dist.html
    #
    # NOTE: UDF or iso9660? is UDF's Linux's support mature and solid enough?
    # let's try with iso9660 for now.
    # Can iso9660 create a 50GB image containing only one file?
    # NOTE: according to dvdisaster's manual.pdf , only hybrid ISO/UDF images have been tested
    # and this only with genisoimage.
    # actually, I could have misread what they mean when they write ISO/UDF, which could mean
    # either ISO or UDF images, but not hybrid ones
    #
    #
    # lvcreate -n snap-lvsrv --snapshot ${targethostname}-vg/lvsrv
    # lvchange -ay -K ${targethostname}-vg/snap-lvsrv
    # mntdir="$(sudo mktemp -d)"
    # mount /dev/mapper/${targethostname}-vg/snap-lvsrv ${mntdir}
    #
    # # OPT UDF: create and mount an udf fs
    # mkudffs image.udf $((700 * 1024 / 2))
    # mkdir image
    # mount -o loop -t udf image.udf image
    #
    # # dar system
    # # NOTE: we do not use compression as bup already does
    # # NOTE: dual layers blurays are around 45GiB, and 20% of that,
    # # needed for dvdisaster redundancy, is around 10GiB. So let's have slices of 35GiB
    # # NOTE: we write the slices directly on the thin volume as the thin pool avoid the need
    # # of double writes (one for the slice on the thin volume and one for the copy-on-write
    # #  on the snapshot volume)
    # # TODO: extract catalogue
    # # TODO: create dar_manager database
    # # TODO: DCF?
    # # TODO: DUC?
    #
    # # OPT UDF: unmount udf image and remove tempdir
    # umount image
    # rmdir image
    #
    # # Next commands happen after dar --create is terminated
    # # create thin volume to check the backup
    # lvcreate --virtualsize "${lvsize}" --name lvchckbckp --thinpool "${targethostname}"-tp "${targethostname}"-vg
    # sudo mkfs.ext4 -L "${targethostname}_srv" /dev/"${targethostname}"-vg/lvchckbckp
    # mntdir="$(sudo mktemp -d)"
    # mount /dev/mapper/${targethostname}-vg/lvchckbckp ${mntdir}
    #
    # # verify file restoration: restore bup repository, diff manual way
    #
    # # create thin volume to check the backup
    # lvcreate --virtualsize "${lvsize}" --name lvchckbckp --thinpool "${targethostname}"-tp "${targethostname}"-vg
    # sudo mkfs.ext4 -L "${targethostname}_srv" /dev/"${targethostname}"-vg/lvchckbckp
    # mntdir="$(sudo mktemp -d)"
    # mount /dev/mapper/${targethostname}-vg/lvchckbckp ${mntdir}
    #
    #
    # # verify file restoration: selected data restore
    #
    # # verify file restoration: restore backup from dar-archived bup repository
    # # NOTE: we will use AVFS and a dar plugin for that
    # # http://www.boomerangsworld.de/cms/avfs/extfs?lang=en
    # # restore it/them in a new thin volume
    #
    # # AVFS + dar extension
    #
    # # NOTE: test a given size set of random files to restore
    # we might want to look at their size and the available storage space
    #
    # umount ${mntdir}
    # rmdir -v ${mntdir}
    # lvchange -an ${targethostname}-vg/lvchckbckp
    # lvremove ${targethostname}-vg/lvchckbckp
    #
    # umount ${mntdir}
    # rmdir -v ${mntdir}
    # lvchange -an ${targethostname}-vg/snap-lvsrv
    # lvremove ${targethostname}-vg/snap-lvsrv



    # # create and mount an udf fs
    # mkudffs image.udf $((700 * 1024 / 2))
    # mkdir image
    # mount -o loop -t udf image.udf image
    #
    # umount ${mntdir}
    # rmdir -v ${mntdir}
    # lvchange -an ${targethostname}-vg/snap-lvsrv
    # lvremove ${targethostname}-vg/snap-lvsrv

    declare -a ordrarray=(
        exit
        break
        menubackup_menutools
        menubackup_menugitconfig
        menubackup_menumooltipassdb
        menubackup_menulukskeyheaders
        menubackup_menulvmconfig
        menubackup_menukernelconfig
        menubackup_menusystemdatasnapshotspre
        menubackup_menubupsystemdata
        menubackup_menubupmirror
        menubackup_menumirrorsnapshotpre
        menubackup_menutempthinvolpre
        menubackup_menudarsystemdataslices
        menubackup_menuformatdiskaugmentburnverifyiso9660
        menubackup_menurestoredarsystemdata
        menubackup_menurestorebupsystemdata
        menubackup_menusystemdataverifyrestore
        menubackup_menutempthinvolpost
        menubackup_menusystemsnapshotspost
        menubackup_menudatasnapshotpost
        menubackup_menumirrorsnapshotpost
        menubackup_menumirrorunmountpoweroff
        menubackup_menubupsystemdatamirrordiagnostics
        menubackup_menudarsystemdatadiagnostics
        menubackup_menudvdisasterdiagnostics
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menuprofilesquickhack {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_downloadprofilesquickhack
        fromtemporarysystem_verifyprofilesquickhack
        fromtemporarysystem_installprofilesquickhack
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menuconfigurationfiles {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_copyresolvconf
        fromtemporarysystem_createfstab
        fromtemporarysystem_createcrypttab
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menulocaloverlay {
    printf "\n"
    # https://wiki.gentoo.org/wiki/Custom_repository
    # https://wiki.gentoo.org/wiki/Handbook:Parts/Portage/CustomTree#Defining_a_custom_repository
    # https://wiki.gentoo.org/wiki/Basic_guide_to_write_Gentoo_Ebuilds
    # https://wiki.gentoo.org/wiki/Repoman

    mkdir -pv /srv/db/repos/localrepo/{metadata,profiles}
    #ln -svr /srv/db/repos/localrepo /var/db/repos/localrepo
    chown -R portage:portage /srv/db/repos/localrepo
    #chown portage:portage /var/db/repos/localrepo
    echo 'localrepo' > /srv/db/repos/localrepo/profiles/repo_name
    # /var/db/repos/localrepo/metadata/layout.conf
    # /srv/db/repos/localrepo/metadata/layout.conf
    # masters = gentoo
    # auto-sync = false
    cd /srv/db/repos/localrepo
    git init
    git add .
    git commit
    # /etc/portage/repos.conf/localrepo.conf
    # [localrepo]
    # location = /srv/db/repos/localrepo

    # update bup ebuild
    mkdir -pv /var/db/repos/localrepo/VAR1
    cp -r /var/db/repos/gentoo/VAR1/VAR2 .
    cd VAR2/

    # create git-subrepo ebuild
    # create git-remote-gcrypt ebuild
    # create VARFS dar extension ebuild
}

function fromtemporarysystem_menuportage {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_installportagedependancies
        fromtemporarysystem_createmakeconf
        fromtemporarysystem_createreposconf
        fromtemporarysystem_syncportage
        fromtemporarysystem_updateportage
        fromtemporarysystem_setupportageprofile
        fromtemporarysystem_updateworld
        fromtemporarysystem_rebuildtoolchain
        fromtemporarysystem_editscriptbootstrapbase
        fromtemporarysystem_runscriptbootstrapbase
        fromtemporarysystem_fixtoolchainbootstrapbase
        fromtemporarysystem_runscriptbootstrapbase
        fromtemporarysystem_fixtoolchainbootstrapbase
        fromtemporarysystem_rebuildworldbootstrapbase
        fromtemporarysystem_resumerebuildworldbootstrapbase
        fromtemporarysystem_rundispatchconfbootstrapbase
        fromtemporarysystem_rebuildworldbootstrapbase
        fromtemporarysystem_resumerebuildworldbootstrapbase
        fromtemporarysystem_rundispatchconfbootstrapbase
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menusystemdkernelinitrd {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_copyxkb2kbdkeymaps
        fromtemporarysystem_createkeymapsymlink
        fromtemporarysystem_firstbootprompt
        fromtemporarysystem_firstboot
        fromtemporarysystem_selectgentookernelrelease
        fromtemporarysystem_unmaskunstablegentoosources
        fromtemporarysystem_installkernelgentoosources
        fromtemporarysystem_listinstalledkernels
        fromtemporarysystem_selectkernelsymlink
        fromtemporarysystem_installbasicsystempackages
        fromtemporarysystem_configurelvm.conf
        fromtemporarysystem_createkernelbuilderuser
        fromtemporarysystem_createkernelbuilddirsfiles
        fromtemporarysystem_copyfedorakernelconfig
        fromtemporarysystem_verifyconfigkernelselfprotectionproject
        fromtemporarysystem_listpackagesrequiredconfigs
        fromtemporarysystem_setkernelversion_static
        fromtemporarysystem_resetkernelversion_static
        fromtemporarysystem_cleankerneltree
        fromtemporarysystem_mrproperkerneltree
        fromtemporarysystem_distcleankerneltree
        fromtemporarysystem_displaygeneratedkernelcmdline
        fromtemporarysystem_oldconfigkernel
        fromtemporarysystem_configkernel
        fromtemporarysystem_backupconfig
        fromtemporarysystem_selectconfig
        fromtemporarysystem_buildkernel
        fromtemporarysystem_installkernel
        fromtemporarysystem_uninstallkernel
        fromtemporarysystem_installmodules
        fromtemporarysystem_uninstallmodules
        fromtemporarysystem_configinitrd
        fromtemporarysystem_buildinitrd
        fromtemporarysystem_installinitrd
        fromtemporarysystem_uninstallinitrd
        fromtemporarysystem_copykernelinitrd2host

    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menusecureboot {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_createdirssecureboot
        fromtemporarysystem_backupoldkeyssecureboot
        fromtemporarysystem_generatenewkeyssecureboot
        fromtemporarysystem_generatesiglistsecureboot
        fromtemporarysystem_signsiglistsecureboot
        fromtemporarysystem_convertnewkeyssecureboot
        fromtemporarysystem_mergekeyssecureboot
        fromtemporarysystem_signmergedkeyssecureboot
        fromtemporarysystem_protectkeyssecureboot
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_menufinalmessages {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_lockefibiossetup
        fromtemporarysystem_enablesecurebootsetupmode
        fromtemporarysystem_rebootnow
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function newinstallation_fromrunningsystem {
    declare -a ordrarray=(
        exit
        break
        fromrunningsystem_downloadfedora
        fromrunningsystem_verifyfedora
        fromrunningsystem_selectliveusbkey
        fromrunningsystem_preparefedorausbkey
        fromrunningsystem_installmooltipass
        fromrunningsystem_initialisemooltipassandsmcard
        fromrunningsystem_settargethostname
        fromrunningsystem_generatepassphrases
        fromrunningsystem_generatekeys
        fromrunningsystem_boottargetmarchine
        fromrunningsystem_lockefibiossetup
        fromrunningsystem_backupsecurebootkeys
        fromrunningsystem_verifysecurebootkeysbackup
        fromrunningsystem_enablesecurebootsetupmode
        fromrunningsystem_boottemporarysystem
        fromrunningsystem_setupinstallenvironmenttemporarysystem
        fromrunningsystem_setupsshaccess
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function newinstallation_fromtemporarysystem {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_menubase
        fromtemporarysystem_menuusbbootkey
        fromtemporarysystem_menumainstorageluks
        fromtemporarysystem_menumainstoragelvm
        fromtemporarysystem_menumainstorageformatmount
        fromtemporarysystem_menustage3portagesnapshot
        fromtemporarysystem_menubackup
        fromtemporarysystem_menuprofilesquickhack
        fromtemporarysystem_menuconfigurationfiles
        fromtemporarysystem_menulocaloverlay
        fromtemporarysystem_menuportage
        fromtemporarysystem_menusystemdkernelinitrd
        fromtemporarysystem_menusecureboot
        fromtemporarysystem_menufinalmessages
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function newinstallation_fromnewsystem {
    # ecryptfs, totp, mooltipass detection
    # connected(irc_*,riot_*,mumble_*,ring_*),unsafe websearch,safe websearch,webveille(regular consulted webiste),webshopping,publicdata(downloads,local files transfers),personaldata(made public),privatedata
    # /etc/machine-info
    declare -a ordrarray=(
        exit
        break
        atfirstboot_importsecurebootnewkeys
        atfirstboot_installselinuxpolicy
        atfirstboot_configureselinuxpolicy
        atfirstboot_rebuildselinuxbase
        atfirstboot_installselinuxbasepolicy
        atfirstboot_updateworld
        atfirstboot_updateconfigfiles
        atsecondboot_relabelselinux
        atsecondboot_enableselinuxswapfile
        atthirdboot_setselinuxbool
        atthirdboot_createselinuxadministrator
        atthirdboot_configurepam
        atthirdboot_createusers
        atthirdboot_grantselinuxprivileges
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menu_newinstallation {
    declare -a ordrarray=(
        exit
        break
        newinstallation_fromrunningsystem
        newinstallation_fromtemporarysystem
        newinstallation_fromnewsystem
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menu_recovery {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_settargethostname
        fromtemporarysystem_selectusbbootkey
        fromtemporarysystem_mountusbbootkey
        fromtemporarysystem_selectmainstorage
        fromtemporarysystem_unlockmainluks
        fromtemporarysystem_mountbtrfssubvolumes
        fromtemporarysystem_mountefi
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function menu_recovery_static {
    declare -a ordrarray=(
        exit
        break
        fromtemporarysystem_setvariables_static
        fromtemporarysystem_resetvariables_static
        fromtemporarysystem_mount_static
        fromtemporarysystem_unmount_static
        fromtemporarysystem_setkernelversion_static
        fromtemporarysystem_resetkernelversion_static
        fromtemporarysystem_cleankerneltree
        fromtemporarysystem_mrproperkerneltree
        fromtemporarysystem_distcleankerneltree
        fromtemporarysystem_configkernel
        fromtemporarysystem_backupconfig
        fromtemporarysystem_selectconfig
        fromtemporarysystem_buildkernel
        fromtemporarysystem_installkernel
        fromtemporarysystem_uninstallkernel
        fromtemporarysystem_installmodules
        fromtemporarysystem_uninstallmodules
        fromtemporarysystem_configinitrd
        fromtemporarysystem_buildinitrd
        fromtemporarysystem_installinitrd
        fromtemporarysystem_uninstallinitrd
        fromtemporarysystem_copykernelinitrd2host
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

function fromtemporarysystem_setvariables_static {
    # hostname
    targethostname="centranthus"
    #btrfssubvol_part="/dev/${targethostname}-vg/lvbtrfssubvolumes"
    pvname="${targethostname}""-pv"
    vgname="${targethostname}""-vg"
    # usb boot key
    bootkey="/dev/sdd"
    # main storage
    mainstorage="/dev/sda"
    #kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
}

function fromtemporarysystem_resetvariables_static {
    # hostname
    targethostname=""
    #btrfssubvol_part=""
    pvname=""
    vgname=""
    # usb boot key
    bootkey=""
    # main storage
    mainstorage=""
    #kernelversion=""
}

function fromtemporarysystem_mount_static {
    #set -xv
    #fromtemporarysystem_mountusbbootkey
    fromtemporarysystem_unlockmainluks
    #fromtemporarysystem_mountbtrfssubvolumes
    # NOTE/TODO: instead of sleeping 3 seconds
    # have a loop of 0.1 second sleeps and break the loop when the needed
    # volumes are available
    sleep 3
    fromtemporarysystem_mountlvmvolumes
    fromtemporarysystem_mountefi
}

function fromtemporarysystem_unmount_static {
    # unmount
    fromtemporarysystem_umountefi
    #fromtemporarysystem_umountbtrfssubvolumes
    fromtemporarysystem_unmountlvmvolumes
    # disable volume group
    fromtemporarysystem_deactivatevg
    # close LUK
    fromtemporarysystem_closeluks
}

function fromtemporarysystem_setkernelversion_static {
    kernelversion="$(ls -l ${mount_root_path}/usr/src/linux | rev | cut -d " " -f 1 | rev)"
}

function fromtemporarysystem_resetkernelversion_static {
    kernelversion=""
}
