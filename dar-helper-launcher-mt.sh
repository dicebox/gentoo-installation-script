# GPLv3+
# Copyright Julien Cerqueira 2019-2020

# TODO: add a variable for a path prefix, this way dar-helper-{actions,menus,variables}.sh
# can be in any directory

# NOTE: for now, bup does not support encryption, but dar does
# So we need to use root to manage bup and makes bup repos accessible only to root,
# and to manage dar archives.
# Yet, as dar archives are encrypted, we can use an other user to manage iso images.
# NOTE/TODO: create a dedicated user to handle iso images. Let's call it root-backup
# NOTE: as we use a dedicated user, do we still want a dedicated gnupg homedir?


# NOTE: where do we store the keys pair for signing? root, so we can use sudo
# and all permissions are fine. Actually, let's use a regular user, as the dar slices
# are encrypted, so they are safe to be handheld by an regular user
# NOTE: let's create a root-backup user, which would be used for the different steps here
# NOTE: store the catalogue in /srv/backup/dar/ maybe in /srv/backup/dar/catalogues?
# configuration phase
#   select burner
#   select gpg local-user
#   select number of threads for dvdisaster
# formatbluray
# dvd+rw-format -ssa=default /dev/sr0
# mediainfo
# dvd+rw-mediainfo /dev/sr0
# checksum slice: see --hash of dar
# sign checksum
# gpg --local-user name --sign --detach-sign my_secret_group_stuff.1.dar.sha512
# create iso
# genisoimage -iso-level 3 -rock -o $IMAGEFILE /srv/backup/dar/system
# dvdisaster iso
# dvdisaster --image $IMAGEFILE --method RS03 --threads $NBTHREADS
# burn disk
# growisofs -dvd-compat -Z /dev/sg0=$IMAGEFILE
# dvdisaster scan
# dvdisaster --drive $DRIVE --scan
# dvdisaster verify
# remove slice + checksum + signature

SLICEPATH="$1"
SLICEBASENAME="$2"
SLICENUMBER="$3"
SLICENUMBERWITHLEADINGZEROS="$4"
SLICEEXTENSION="$5"
SLICECONTEXT="$6"
SLICEFULLURL="$7"

#INSTALL_PATH=/opt/bin
INSTALL_PATH=.

function display_menu {
    # $1: menu's function name
    # $2: reference of a table with functions and corresponding messages if any
    local menu_name="${1}"
    local -n ordrarray_ref=${2}
    local -i ordrarray_len=${#ordrarray_ref[@]}
    local -n dsctable_ref=${3}
    local -n cfrmtable_ref=${4}
    local choice
    local valid=""
    local msg
    while true; do
        msg="Welcome to ${menu_name}\n"
        # Generate the different choices and their corresponding index to enter
        for i in ${!ordrarray_ref[@]}; do
            msg="${msg}${dsctable_ref[${ordrarray_ref[${i}]}]}[${i}] "
        done
        msg="${msg}\n\nPlease enter the number corresponding to an entry."
        msg="${msg}\n\nYour previous choice: ${userchoice[${menu_name}]}"
        msg="${msg}\nYour choice: "
        printf "${msg}"
        read choice
        # Filter out unrecognized inputs
        for i in ${!ordrarray_ref[@]}; do
            if [ "${choice}" == "${i}" ]; then
                valid=1
                break
            fi
        done
        # Reset the input filter, store the index of the selected function and call it
        if [ ${valid} ]; then
            clear
            valid=""
            userchoice[${menu_name}]=${choice}
            if [ "${cfrmtable_ref[${ordrarray_ref[${choice}]}]+isset}" ]; then
                ask_useragreement_torun "${cfrmtable_ref[${ordrarray_ref[${choice}]}]}" ${ordrarray_ref[${choice}]}
            else
                ${ordrarray_ref[${choice}]}
            fi
            printf "\n ### MENU ### \n"
        fi
    done
}

function display_menu_simple {
    # $1: menu's function name
    # $2: reference of a table with functions and corresponding messages if any
    local menu_name="${1}"
    local -n ordrarray_ref=${2}
    local -i ordrarray_len=${#ordrarray_ref[@]}
    local choice
    local valid=""
    local msg
    while true; do
        msg="Welcome to ${menu_name}\n"
        # Generate the different choices and their corresponding index to enter
        for i in ${!ordrarray_ref[@]}; do
            msg="${msg}${ordrarray_ref[${i}]}[${i}] "
        done
        msg="${msg}\n\nPlease enter the number corresponding to an entry."
        msg="${msg}\n\nYour previous choice: ${userchoice[${menu_name}]}"
        msg="${msg}\nYour choice: "
        printf "${msg}"
        read choice
        # Filter out unrecognized inputs
        for i in ${!ordrarray_ref[@]}; do
            if [ "${choice}" == "${i}" ]; then
                valid=1
                break
            fi
        done
        # Reset the input filter, store the index of the selected function and call it
        if [ ${valid} ]; then
            clear
            valid=""
            userchoice[${menu_name}]=${choice}
            ${ordrarray_ref[${choice}]}
            printf "\n ### MENU ### \n"
        fi
    done
}

function reload_actions {
    . ${INSTALL_PATH}/dar-helper-actions-mt.sh
}

function reload_menus {
    . ${INSTALL_PATH}/dar-helper-menus-mt.sh
}

function reload_variables {
    . ${INSTALL_PATH}/dar-helper-variables-mt.sh
}

function f {
    printf "\n"
}

function menu_launcher {
    clear
    reload_variables
    reload_actions
    reload_menus
    declare -a ordrarray=(
        exit
        menu_main
        reload_actions
        reload_menus
        reload_variables
    )
    display_menu_simple ${FUNCNAME[0]} ordrarray
}

menu_launcher
