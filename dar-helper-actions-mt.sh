# NOTE: while sleep 1; do clear && ps aux | grep myscript; done
# this is a nice way to follow forks

# NOTE:
# /srv/backup/dar.extract
# /srv/backup/bup.extract

# NOTE:
# sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation --bind=/dev/sr0 dar --create /srv/backup/dar.tmp/cleanme_bup --fs-root /srv/backup/readonly.mnt/backup/ --go-into cleanme.bup --slice 34G --hash sha512 --key : --execute "/opt/bin/dar-helper-launcher.sh %p %b %n %N %e %c %u"

# NOTE:
# test hdd
# smartctl --test=long --captive /dev/DEVICE # foreground
# smartctl --test=long /dev/DEVICE # background (good enough)
# monitor with: smartctl --log=selftest /dev/DEVICE
# Phoronix Test Suite: https://openbenchmarking.org/suite/pts/disk
# get byte size:
# lsblk --bytes --noheadings --output SIZE --nodeps /dev/DEVICE
# 8M=8388608
# 16M=16777216
# dd if=/dev/urandom of=/dev/DEVICE oflag=sync bs=16M count=$(($(lsblk --bytes --noheadings --output SIZE --nodeps /dev/DEVICE)/16777216 + 1)) status=progress

# NOTE: disk block alignement
# https://wiki.debian.org/DiskBlockAlignment
# https://unix.stackexchange.com/questions/421587/dmsetup-luksformat-creating-an-alignment-inconsistency

# NOTE: mooltipass
# use reverse domain+hostname, ex:
# local.garden.usb
# local.garden.hostname
# mooltipy login set local.garden.usb -u archives

# NOTE: usb hdd setup
# luks:
# sudo cryptsetup luksFormat /dev/DEVICE # use mooltipy to generate the password
# lvm:
# pvcreate --verbose /dev/mapper/
# pvs -o +pe_start,vg_extent_size --units b
# vgcreate --verbose archives-vg /dev/mapper/
# TODO: check vg alignment physical extent size
# vgs -o +vg_extent_size --units b
# lvcreate --type thin-pool --size 1TiB --chunksize 2MiB -n archives-tp archives-vg
# lvcreate --virtualsize 1TiB --name lvcleanme --thinpool archives-tp archives-vg
# lvcreate --virtualsize 1TiB --name lvsystem --thinpool archives-tp archives-vg
# lvcreate --virtualsize 1TiB --name lvdata --thinpool archives-tp archives-vg
# fs:
# mkfs.ext4 -L cleanme /dev/archives-vg/lvcleanme
# mkfs.ext4 -L system /dev/archives-vg/lvsystem
# mkfs.ext4 -L data /dev/archives-vg/lvdata
# tune2fs -l /dev/archives-vg/lvcleanme | grep "Block size"
# tune2fs -l /dev/archives-vg/lvsystem | grep "Block size"
# tune2fs -l /dev/archives-vg/lvdata | grep "Block size"
# TODO:
# mount cleanme
# restore bup repo from dar archive
# dar --extract /run/media/leaf/CDROM/cleanme_bup --fs-root /run/media/leaf/cleanme
# bup fsck
# bup --bup-dir=/run/media/leaf/cleanme/cleanme.bup fsck --jobs=3
# restore cleanme dir from bup repo
# bup --bup-dir=/run/media/leaf/cleanme/cleanme.bup restore --outdir=/run/media/leaf/cleanme --numeric-ids old-backups-to-be-cleaned/latest/mnt/.

function grablock {
    # $1: lockfile's name

    # should we test existence of the dir?
    while ! mkdir -v ${1}; do
        sleep 0.1
    done > /dev/null 2>&1
}

function releaselock {
    # $1: lockfile's name

    # should we test existence of the dir?
    rmdir -v ${1} > /dev/null 2>&1
}

function cleansdirlockfifoend {
    if [ -d ${WORKDIRPATH}/${WORKDIR}/log${LOCKSUFFIX} ]; then
        rmdir -v ${WORKDIRPATH}/${WORKDIR}/log${LOCKSUFFIX}
    fi
    for sl in ${STALELOCKS[*]}; do
        if [ -d ${WORKDIRPATH}/${WORKDIR}/${sl}${LOCKSUFFIX} ]; then
            rmdir -v ${WORKDIRPATH}/${WORKDIR}/${sl}${LOCKSUFFIX}
        fi
        if [ -d ${WORKDIRPATH}/${WORKDIR}/${sl} ]; then
            rm -R -v ${WORKDIRPATH}/${WORKDIR}/${sl}
        fi
    done
}

function cleandirfifoend {
    for sl in ${MPFUNC[*]}; do
        printf "cleaning ${sl}\n"
        if [ -d ${WORKDIRPATH}/${WORKDIR}/${sl} ]; then
            rm -R -v ${WORKDIRPATH}/${WORKDIR}/${sl}
        fi
    done
}

function searchfinishedprocesses {
    # 1: family name to clean
    local fname="${1}"
    printf "fname searchfinishedprocesses: ${fname}\n"

    # we need to receive a message from the background process when they terminate
    # we do not see/know any simple way to do that with fifos
    # let's simply use temporary files instead

    # go through all families and their respective instances
    # for each
    # if it exists, source their end file (something like:
    # ${WORKDIRPATH}/${WORKDIR}/${FUNCNAME[0]}/inst${ninst}${ENDSUFFIX} ) , and
    # call unregistermpinstance with the grabbed values

    # is it faster to ls and filter? by how much? do we care?
    # do we want to free only a family given as a parameter?
    # we cannot rely on PIDs because of PIDs warp around

    #for f in ${!NUMPROCDICT[*]}; do
    #    declare -n fnamearr=${f}arr
    #    for i in ${!fnamearr[*]}; do
    #        if [ -f "${WORKDIRPATH}/${WORKDIR}/${f}/inst${i}${ENDSUFFIX}" ]; then
    #            unregistermpinstance ${f} ${i}
    #        fi
    #    done
    #done
    declare -n fnamearr=${fname}arr
    for ninst in ${!fnamearr[*]}; do
        printf "Looking for ${WORKDIRPATH}/${WORKDIR}/${fname}/inst${ninst}${ENDSUFFIX}\n"
        if [ -f "${WORKDIRPATH}/${WORKDIR}/${fname}/inst${ninst}${ENDSUFFIX}" ]; then
            kill -s SIGTERM ${fnamearr[${ninst}]}
            unregistermpinstance ${fname} ${ninst}
        fi
    done
}

function getfreeninst {
    # NEEDS BASH >= 4.3, to use declare -n
    # $1: function name of the caller
    # return (echo) instance number, so fnamearray can be correctly set at index(=instance number)

    # because of bash, we need to split that in two functions
    # unless we missed a way to update our arrays from the subshell
    #printf "gfi1: ${1}\n"

    # CRITICAL SECTION START: STOP in registermpinstance
    #grablock ${1}

    # rename array with a local name
    declare -n fnamearr=${1}arr
    local fnamearrlen=${#fnamearr[@]}
    local fnamearrindexes=${!fnamearr[@]}

    # with printf, the [@] syntaxe to get all the values in the array doesn't work
    # so we use the [*] syntaxe
    #printf "fnamearr[@]=${fnamearr[*]} \n"

    # look for the first free slot, or append at the end of the array
    # then return its number
    # we might be able to be more efficient that going through the whole array
    # as we can easily get all the indexes, but I can't think about something which can help
    # considering the low number of calls, it should be plenty enough efficient anyway
    # we might register the highest index, so we could easily detext if we need to extend the array
    # if ${#fnamearr[*]} == highest[${1}arr]
    for i in $(seq 0 ${fnamearrlen}); do
        if [ "${i}" = "${fnamearrlen}" ]; then
            break
        fi
        if [ "${fnamearr[${i}]}" = "" ]; then
            break
        fi
    done
    #fnamearr[${i}]="1" # this does not work as we run in a subshell, let's only return the index
    FREENINST="${i}"
    printf "FREENINST: ${FREENINST}\n"
}

function registermpinstance {
    # $1: function name of the caller

    local fname=${1}
    printf "fname registermpinstance: ${fname}\n"
    declare -n fnamearr=${fname}arr
    #printf "fnamearr=${fname}arr\n"
    fnamearr[${FREENINST=}]=1
    #printf "fnamearr[${2}]=${fnamearr[${2}]}\n"
    if [ "${NUMPROCDICT[${fname}]}" = "" ]; then
        mkdir -v ${WORKDIRPATH}/${WORKDIR}/${fname}
    fi
    NUMPROCDICT[${fname}]=${#fnamearr[*]}
    #printf "NUMPROCDICT[${fname}]=${#fnamearr[*]}\n"
    # CRITICAL SECTION STOP: START in getfreeninst
    #releaselock ${1}
}

function unregistermpinstance {
    # $1: function name of the caller
    # $2: instance's number

    local fname="${1}"
    local ninst="${2}"
    declare -n fnamearr=${fname}arr

    # CRITICAL SECTION START
    #grablock ${1}

    # free instance number
    unset fnamearr[${ninst}]
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${ENDSUFFIX}"
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${PIDSUFFIX}"
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${LOGSUFFIX}"
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${BGLOGSUFFIX}"
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${FIFOSUFFIX}"
    rm -v "${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${BGFIFOSUFFIX}"
    if [ "${!fnamearr[*]}" = "" ]; then
        # if no more instances for the family, see the family name
        # this is related to tests of family emptiness in selectmpprocess
        # it seems cleaner to unset instead of keeping it set at 0
        # it seems more reliable in case, for odd reasons, we have to test
        # an empty family in selectmpprocess, which is the default scenario now
        # but would not it we set them to 0
        unset NUMPROCDICT[${fname}]
        rmdir -v ${WORKDIRPATH}/${WORKDIR}/${fname}
    else
        # update number of instances
        NUMPROCDICT[${fname}]=${#fnamearr[*]}
    fi

    # CRITICAL SECTION STOP
    #releaselock ${1}
}

function runinbackground {
    # TODO: menu in foreground process, to send signals to both runinbackground and the background process
    # signals like sigterm and sigkill
    # $1: family name
    # $2: instance's number
    # $3: string of command with options to run

    local fname="${1}"
    local ninst="${2}"
    local bgcommand="${3}"

    # in the same menu where mpexample is proposed,
    # add before it a askremovestalelock
    # but our menus entries cannot have any parameter, can they?
    # something like "askremovestalelock mpexample"?
    # but if we have to write that,
    # why not having a static array of functions lock to check at start?

    # send to background: mpexample &

    # init: !!!ALWAYS!!! use getfreeninst and registermpinstance as a tight couple!
    #ninst=$(getfreeninst ${FUNCNAME[0]})
    #ninst="${ninst##*[^[:digit:]]}" # keeping only last digits in case we grab stuffs in the subshell
    #registermpinstance ${FUNCNAME[0]} ${ninst}

    local fifo="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${FIFOSUFFIX}"
    local bgfifo="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${BGFIFOSUFFIX}"
    local log="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${LOGSUFFIX}"
    local bglog="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${BGLOGSUFFIX}"
    local pid="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${PIDSUFFIX}"
    local end="${WORKDIRPATH}/${WORKDIR}/${1}/inst${2}${ENDSUFFIX}"

    # create files
    mkfifo --mode=0600 ${bgfifo}
    touch ${bglog}
    #mkfifo --mode=0600 ${fifo} >> ${log} 2>&1
    #touch ${log} >> ${log} 2>&1

    #printf "1: ${1}\n" >> ${log} 2>&1
    #printf "2: ${2}\n" >> ${log} 2>&1
    #printf "3: ${3}\n" >> ${log} 2>&1
    #printf "1: ${1}\n"
    #printf "2: ${2}\n"
    #printf "3: ${3}\n"

    # init fifo, is this needed?
    # for now, log is empty until we send something in the fifo
    # can we solve that?
    while sleep 0.1; do
        if read line; then
            echo > ${bgfifo} &
            break
        fi
    done
    printf "Caught the echo from the calling function/script.\n"
    printf "1: ${1}\n"
    printf "2: ${2}\n"
    printf "3: ${3}\n"

    # some code
    #${3} >> ${log} 2>&1 < ${fifo}
    #${3} < ${bgfifo} >> ${log} 2>&1 &
    ${3} < ${bgfifo} > ${bglog} 2>&1 &
    #${3} < ${bgfifo} >> ${log} 2>&1
    #${3}

    bgpid=$!
    echo ${bgpid} > ${pid}

    #bgpid=$!
    #while sleep 0.1
    #      kill -0 ${bgpid} >/dev/null 2>&1; do # tests that background process exists
    #    if read line; then
    #        echo "${line}" > ${bgfifo}
    #    fi
    #done

    #rm -vf ${bgfifo}

    # merge log dialogue
    #printf "Do you want to merge this log with the main log? (just press enter for now, this is a dummy):\n" >> ${log} 2>&1
    printf "Do you want to merge this log with the main log? (just press enter for now, this is a dummy):\n"
    while sleep 0.1; do
        if read line; then
            break
        fi
    done
    local usrchoice="${line}"
    kill -s SIGTERM ${bgpid}
    printf "usrchoice: ${usrchoice}\n"
    #read usrchoice < ${fifo}
    #printf "usrchoice: ${usrchoice}\n"
    #read usrchoice < ${fifo}
    #printf "usrchoice: ${usrchoice}\n"
    #read usrchoice < ${fifo}
    #printf "usrchoice: ${usrchoice}\n"
    # grablock
    # merge
    # releaselock

    # clean files
    #rm -v ${log} >> ${log} 2>&1
    #rm -v ${fifo} >> ${log} 2>&1

    # exit
    # send signal to main fifo with ${FUNCNAME[0]} ${1}
    touch ${end}
    printf "Process terminated. Will be cleaned later.\n"
}

function prepareruninbackground {
    # 1: function name, we cannot use SELPFAM as it is not necessarily initialized nor
    # set at the right value
    # 2: command to run in background

    local fname="${1}"
    local bgcommand="${2}"
    printf "fname prepareruninbackground: ${fname}\n"
    printf "bgcommand: ${bgcommand}\n"

    # init: !!!ALWAYS!!! use searchfinishedprocesses, getfreeninst and registermpinstance as a tight trio!
    searchfinishedprocesses ${fname}
    getfreeninst ${fname}
    printf "fname prepareruninbackground 2: ${fname}\n"
    #ninst=$(getfreeninst ${FUNCNAME[0]})
    #ninst="${ninst##*[^[:digit:]]}" # keeping only last digits in case we grab stuffs in the subshell
    registermpinstance ${fname}

    local fifo="${WORKDIRPATH}/${WORKDIR}/${fname}/inst${FREENINST}${FIFOSUFFIX}"
    local log="${WORKDIRPATH}/${WORKDIR}/${fname}/inst${FREENINST}${LOGSUFFIX}"
    local bgpid="${WORKDIRPATH}/${WORKDIR}/${fname}/inst${FREENINST}${PIDSUFFIX}"
    # create files
    mkfifo --mode=0600 ${fifo}
    # needed so we get log output as soon as the background function start
    # is there an other, less hacky way?
    echo > ${fifo} &
    touch ${log}

    sudo printf "Unlocked sudo\n"

    runinbackground ${fname} ${FREENINST} "${bgcommand}" < ${fifo} > ${log} 2>&1 &

    fpid=$!
    sleep 1
    declare -n fnamearr=${SELPFAM}arr
    printf "debugpid: $!\n"
    fnamearr[${FREENINST}]=${fpid} # shouldn't we modify, and maybe rename registermpinstance instead? or maybe split it to create the directory in an other function?

    while sleep 0.1; do
        if [ -f "${bgpid}" ]; then
            break
        fi
    done
    declare -n fnamebgarr=${SELPFAM}bgarr
    fnamebgarr[${FREENINST}]="$(cat "${bgpid}")"
    printf "fnamearr: ${SELPFAM}arr\n"
    printf "ninst:${FREENINST}\n"
    printf "function pid (fnamearr[${FREENINST}]): ${fnamearr[${FREENINST}]}\n"
    printf "process pid: ${fnamebgarr[${FREENINST}]}\n"

}

function createworkdir {
    if ! [ -d ${WORKDIRPATH}/${WORKDIR} ]; then
        mkdir --parents -v ${WORKDIRPATH}/${WORKDIR}
    fi
}

function startshell {
    /usr/bin/bash
}

function bgexample {
    local bgcommand=""
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
}

function mpexample {
    # init: !!!ALWAYS!!! use searchfinishedprocesses, getfreeninst and registermpinstance as a tight trio!
    searchfinishedprocesses
    ninst=$(getfreeninst ${FUNCNAME[0]})
    ninst="${ninst##*[^[:digit:]]}" # keeping only last digits in case we grab stuffs in the subshell
    registermpinstance ${FUNCNAME[0]} ${ninst}

    fifo="${WORKDIRPATH}/${WORKDIR}/${FUNCNAME[0]}/inst${ninst}${FIFOSUFFIX}"
    log="${WORKDIRPATH}/${WORKDIR}/${FUNCNAME[0]}/inst${ninst}${LOGSUFFIX}"
    #end="${WORKDIRPATH}/${WORKDIR}/${FUNCNAME[0]}/inst${ninst}${ENDSUFFIX}"
    # create files
    mkfifo --mode=0600 ${fifo}
    # needed so we get log output as soon as the background function start
    # is there an other, less hacky way?
    echo > ${fifo} &
    touch ${log}

    runinbackground ${FUNCNAME[0]} ${ninst} "" < ${fifo} > ${log} 2>&1 &
    #runinbackground ${FUNCNAME[0]} ${ninst} "" > ${log} 2>&1 < ${fifo} &
    #runinbackground ${FUNCNAME[0]} ${ninst} "" > ${log} 2>&1 &
    #runinbackground ${FUNCNAME[0]} ${ninst} "" < ${fifo}  &> ${log} &
    #runinbackground ${FUNCNAME[0]} ${ninst} "" &> ${log} &
    #tail -f ${fifo} | runinbackground ${FUNCNAME[0]} ${ninst} "" &> ${log} &
    #runinbackground ${FUNCNAME[0]} ${ninst} "" &
}

function selectmpfunction {
    # TODO: give an index for each choice to make it easier to select one
    while true; do
        for f in ${!NUMPROCDICT[*]}; do
            searchfinishedprocesses ${f}
        done
        if [ "${!NUMPROCDICT[*]}" = "" ]; then
            printf "No function to choose from.\n"
            break
        fi
        printf "List of functions to select from, to manage its corresponding processes.\n"
        printf "${!NUMPROCDICT[*]}\n" # This works as we unset a key when its value falls to 0
        printf "Enter your choice.\n"
        read fchoice
        for f in ${!NUMPROCDICT[*]}; do
            #printf "f: ${f}\n"
            #printf "fchoice: ${fchoice}\n"
            if [ "${fchoice}" = "${f}" ]; then
                clear
                SELPFAM=${fchoice}
                printcurrentmpfunction
                return
            fi
        done
        clear
        printf "Not valid. Try again please.\n"
    done
}

function printcurrentmpfunction {
    printf "SELPFAM=${SELPFAM}\n"
}

function selectmpprocess {
    local fname=${SELPFAM}
    declare -n fnamearr=${SELPFAM}arr
    #printf "fnamearr=${SELPFAM}arr\n"
    #printf "fnamearr[*]=${!fnamearr[*]}\n"
    while true; do
        for f in ${!NUMPROCDICT[*]}; do
            searchfinishedprocesses ${f}
        done
        if [ "${!fnamearr[*]}" = "" ]; then
            printf "No instance to choose from.\n"
            break
        fi
        printf "Enter the number of one of the ${SELPFAM} instances.\n"
        printf "If none is displayed, you might want to select an other function/family.\n"
        printf "${!fnamearr[*]}\n"
        read ichoice
        for i in ${!fnamearr[*]}; do
            if [ "${ichoice}" = "${i}" ]; then
                clear
                SELPNUM=${ichoice}
                printcurrentmpprocess
                return
            fi
        done
        clear
        printf "Not valid. Try again please.\n"
    done
}

function printcurrentmpprocess {
        printf "SELPNUM=${SELPNUM}\n"
}

function displaymptail {
    # check log availability
    # less +F log of proc number

    # https://unix.stackexchange.com/questions/197199/is-there-any-way-to-exit-less-follow-mode-without-stopping-other-processes-in
    # preferred for long output, as we can change pages, instead of tail -f
    tail ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${LOGSUFFIX}
    ls -l ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${LOGSUFFIX}
}

function displaympless {
    # check log availability
    # less +F log of proc number

    # https://unix.stackexchange.com/questions/197199/is-there-any-way-to-exit-less-follow-mode-without-stopping-other-processes-in
    # preferred for long output, as we can change pages, instead of tail -f
    less ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${LOGSUFFIX}
    ls -l ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${LOGSUFFIX}
}

function inputmp {
    # check fifo availability
    # printf type your input
    printf "Enter your input to ${SELPFAM}.${SELPNUM}\n"
    # read input
    read usrinput
    # let's send it in background, in case the pipe is broken
    $(echo "${usrinput}" > ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${FIFOSUFFIX}) &
    # echo input to fifo
}

function displaycmdtail {
    # check log availability
    # less +F log of proc number

    # https://unix.stackexchange.com/questions/197199/is-there-any-way-to-exit-less-follow-mode-without-stopping-other-processes-in
    # preferred for long output, as we can change pages, instead of tail -f
    tail ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${BGLOGSUFFIX}
    ls -l ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${BGLOGSUFFIX}
}

function displaycmdless {
    # check log availability
    # less +F log of proc number

    # https://unix.stackexchange.com/questions/197199/is-there-any-way-to-exit-less-follow-mode-without-stopping-other-processes-in
    # preferred for long output, as we can change pages, instead of tail -f
    less ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${BGLOGSUFFIX}
    ls -l ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${BGLOGSUFFIX}
}

function input2cmd {
    # check fifo availability
    # printf type your input
    printf "Enter your input to ${SELPFAM}.${SELPNUM}\n"
    # read input
    read usrinput
    # let's send it in background, in case the pipe is broken
    $(echo "${usrinput}" > ${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${BGFIFOSUFFIX}) &
    # echo input to fifo
}

function sigtermbgfunction {
    declare -n fnamearr=${SELPFAM}arr
    printf "fnamearr: ${SELPFAM}arr\n"
    printf "ninst:${SELPNUM}\n"
    printf "function pid (fnamearr[${SELPNUM}]): ${fnamearr[${SELPNUM}]}\n"
    printf "function pid: ${fnamearr[${SELPNUM}]}\n"
    if ! kill -s SIGTERM ${fnamearr[${SELPNUM}]}; then
        local end="${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${ENDSUFFIX}"
        touch ${end}
    fi
}

function sigkillbgfunction {
    declare -n fnamearr=${SELPFAM}arr
    printf "function pid: ${fnamearr[${SELPNUM}]}\n"
    if ! kill -s SIGKILL ${fnamearr[${SELPNUM}]}; then
        local end="${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${ENDSUFFIX}"
        touch ${end}
    fi
}

function sigtermbgcommand {
    declare -n fnamebgarr=${SELPFAM}bgarr
    printf "command pid: ${fnamebgarr[${SELPNUM}]}\n"
    if ! kill -s SIGTERM ${fnamebgarr[${SELPNUM}]}; then
        local end="${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${ENDSUFFIX}"
        touch ${end}
    fi
}

function sigkillbgcommand {
    declare -n fnamebgarr=${SELPFAM}bgarr
    printf "command pid: ${fnamebgarr[${SELPNUM}]}\n"
    if ! kill -s SIGKILL ${fnamebgarr[${SELPNUM}]}; then
        local end="${WORKDIRPATH}/${WORKDIR}/${SELPFAM}/inst${SELPNUM}${ENDSUFFIX}"
        touch ${end}
    fi
}

function cleanmultiprocesssetup {
    printf "\n"
    rmdir ${TMPDIR}
}

function printuser {
    whoami
}

function askconfigtype {
    # local or global
    # accept only 1 or 2 as input, and 0 too, to cancel
    while true; do
        printf "Please enter 0 to cancel, 1 to set a local (in ~/.config/) config or 2 to set a global (in /etc/) config.\n"
        read SELECTEDCONF
        clear
        printf "SELECTEDCONF=${SELECTEDCONF}\n"
        case ${SELECTEDCONF} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting a local config.\n"
                SELECTEDCONF=${HOMECONF}
                break
                ;;
            2)
                userid="$(id -u)"
                if [ "${userid}" -ne "0" ]; then
                    clear
                    printf "You need to be root to select this type.\n"
                    continue
                fi
                printf "Setting a global config.\n"
                SELECTEDCONF=${ETCCONF}
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "SELECTEDCONF=${SELECTEDCONF}\n"
}

function createconfig {
    printf "\n"
    if [ "${SELECTEDCONF}" = "" ]; then
        printf "Select a configuration file type first.\n"
        return
    fi
    if [ -f ${SELECTEDCONF} ]; then
        printf "Configuration file already exists.\n"
        return
    fi
    mkdir -v ~/.config
    strconfig="IMAGEFILE=${IMAGEFILE}\n"
    strconfig="${strconfig}EXTRACTEDISO=${EXTRACTEDISO}\n"
    strconfig="${strconfig}BURNER=${BURNER}\n"
    strconfig="${strconfig}GPGHOMEDIR=${GPGHOMEDIR}\n"
    strconfig="${strconfig}GPGLOCALUSR=${GPGLOCALUSR}\n"
    strconfig="${strconfig}DVDISASTERNTHREADS=${DVDISASTERNTHREADS}\n"
    touch ${SELECTEDCONF}
    printf "${strconfig}" > ${SELECTEDCONF}
    ls -l ${SELECTEDCONF}
}

function loadconfig {
    printf "\n"
    if [ -f ${ETCCONF} ]; then
        printf "Sourcing ${ETCCONF}. Potentially overriding default configuration.\n"
        . ${ETCCONF}
    fi
    if [ -f ${HOMECONF} ]; then
        printf "Sourcing ${HOMECONF}. Potentially overriding default configuration and /etc/dar-helper.\n"
        . ${HOMECONF}
    fi

}

function printconfig {
    printf "IMAGEFILE=${IMAGEFILE}\n"
    printf "EXTRACTEDISO=${EXTRACTEDISO}\n"
    printf "BURNER=${BURNER}\n"
    printf "GPGHOMEDIR=${GPGHOMEDIR}\n"
    printf "GPGLOCALUSR=${GPGLOCALUSR}\n"
    printf "DVDISASTERNTHREADS=${DVDISASTERNTHREADS}\n"
}

function printparams {
    printf "SLICEPATH=${SLICEPATH}\n"
    printf "SLICEBASENAME=${SLICEBASENAME}\n"
    printf "SLICENUMBER=${SLICENUMBER}\n"
    printf "SLICENUMBERWITHLEADINGZEROS=${SLICENUMBERWITHLEADINGZEROS}\n"
    printf "SLICEEXTENSION=${SLICEEXTENSION}\n"
    printf "SLICECONTEXT=${SLICECONTEXT}\n"
    printf "SLICEFULLURL=${SLICEFULLURL}\n"
}

function selectburner {
    printf "\n"
    while true; do
        printf "Enter the index of the burner you want to select:\n"
        nbburner="$(($(ls ${BURNERPATH}/sr* | wc -l) - 1))"
        for b in $(seq 0 ${nbburner}); do
            printf "${BURNERPATH}${b} [${b}]  "
        done
        printf "\n"
        read iusr
        for b in $(seq 0 ${nbburner}); do
            if [ "${iusr}" = "${b}" ]; then
                BURNER="sr${iusr}"
                breakwhile=1
                break
            fi
        done
        if [ "${breakwhile}" = "1" ]; then
            break
        fi
        printf "Unrecognized input, please try again.\n"
    done
}

function printcurrentburner {
    printf "${BURNERPATH}${BURNER}\n"
    printf "${ISODIR}${ISOIMAGE}.${BURNER}${ISOSUFFIX}\n"
    printf "${ISODIR}${ISOEXTRACTED}.${BURNER}${ISOSUFFIX}\n"
}

function mediainfo {
    local bgcommand="dvd+rw-mediainfo ${BURNERPATH}${BURNER}"
    printf "bgcommand mediainfo: ${bgcommand}\n"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
}

function formatbluray {
    local bgcommand="dvd+rw-format -ssa=default ${BURNERPATH}${BURNER}"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    # NOTE/TODO: does not work from systemd-nspawn. Why?
    # https://wiki.archlinux.org/index.php/systemd-nspawn#Run_docker_in_systemd-nspawn
    # we might need to add some capabilities and whitelist some syscall
    # default capabilities list in man systemd-nspawn
    # which ones of each?
    # capabilities:
    # CAP_SYS_RAWIO - sounds interesting, it seems it is not in the default set
    # syscall: see with strace?
    # growisofs seems to work when ran with strace
    # Actually, it works, but it takes a longer time than what dvd+rw-format,
    # or growisofs, expect. Just wait for the drive to "wake up"
    # that's weird behaviour that I've never experienced running commands from host
    # for now, it seems that only the first access is problematic
    # possibly only the first write access

    # increment NUMPROCDICT
    # mkfifo
    #
    # rm fifo

    # it would be cumbersome to move this outside of this function
    # as NUMPROCDICT and SELPROCDICT would already
    # have been modified
    # ask to merge or rm log

    # decrement NUMPRODICT
    # check SELPROCDICT
}

function checkwrite {
    getfacl ${ISODIR}
}

function givewrite {
    su -l -c "setfacl --modify user:$(whoami):rwx ${ISODIR}"
}

function removewrite {
    su -l -c "setfacl --modify user:$(whoami): ${ISODIR}"
}

function checkwritechroot {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
                   getfacl ${ISODIRPLAIN}
}

function givewritechroot {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
                   setfacl --modify user:root-backups:rwx ${ISODIRPLAIN}
}

function removewritechroot {
    sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
                   setfacl --modify user:root-backups: ${ISODIRPLAIN}
}

function signchecksum {
    # NOTE: do gpg output the sig in the right directory?
    # NOTE: we want a dedicated homedir, to keep it encrypted as much as possible.
    # We probably want a dedicated gnupg homedir for each gnupg use we have for that same reason.
    # NOTE: we still want to explicitly tell which local-user we want to use, to avoid any
    # unexpected behaviour, which could occur in case we use that dedicated gnupg homedir for
    # an other gnupg local-user.
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --sign --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    gpg --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function signchecksumasroot {
    # NOTE: do gpg output the sig in the right directory?
    # NOTE: we want a dedicated homedir, to keep it encrypted as much as possible.
    # We probably want a dedicated gnupg homedir for each gnupg use we have for that same reason.
    # NOTE: we still want to explicitly tell which local-user we want to use, to avoid any
    # unexpected behaviour, which could occur in case we use that dedicated gnupg homedir for
    # an other gnupg local-user.
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --sign --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    #gpg --homedir ${GPGHOMEDIR} --local-user ${GPGLOCALUSR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    GPGHOMEDIR=/home/root-backups/.gnupg
    gpg --homedir ${GPGHOMEDIR} --detach-sign ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    ls -lh ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig
}

function verifysig {
    printf "\n"
    gpg --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function verifysigasroot {
    printf "\n"
    GPGHOMEDIR=/home/root-backups/.gnupg
    gpg --homedir ${GPGHOMEDIR} --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

#function verifychecksum {
#    awk "{print \$1 \" ${SLICEPATH}/\"\$2}" ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512 | sha512sum --check -
#}

function selectimagefile {
    printf "\n"
}

function printcurrentimagefile {
    printf "\n"
}

function selectdararchive {
    printf "Enter a dar archive:\n"
    read DARARCHIVE
    printdararchive
}

function printdararchive {
    printf "DARARCHIVE: ${DARARCHIVE}\n"
}

function selectdarslice {
    printf "Enter a dar slice:\n"
    read DARSLICE
    printdarslice
}

function printdarslice {
    printf "DARSLICE: ${DARSLICE}\n"
}

function createiso {
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    local bgcommand="\
          sudo chroot ${mount_root_path} sudo -i -u root-backups mkisofs -iso-level 3 -dir-mode 0500 -file-mode 0400 -new-dir-mode 0500 -o ${ISODIRPLAIN}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX} ${ISODIRPLAIN}${DARARCHIVE}/${DARSLICE}"
          #sudo systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation \
          #               sudo -i -u root-backups mkisofs -iso-level 3 -dir-mode 0500 -file-mode 0400 -new-dir-mode 0500 -o ${ISODIRPLAIN}${ISOIMAGE}.${BURNER}${ISOSUFFIX} ${ISODIRPLAIN}${DARARCHIVE}/${DARSLICE}"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    # NOTE: where do we create the image file?
    # NOTE: what directory do we backup?
    #mkisofs -iso-level 3 -dir-mode 0500 -file-mode 0400 -new-dir-mode 0500 -o ${ISODIR}${ISOIMAGE}.${BURNER}${ISOSUFFIX} -m lost+found -m "${ISODIR}${ISOIMAGE}*" -m "${ISODIR}${ISOEXTRACTED}*" /srv/backup/dar.tmp
}

function augmentiso {
    # NOTE/TODO: check directory size before creating the iso, and add a sparse file so the size of the directory is at least 25GiB
    # NOTE: despite working --threads/-x , encoding is really slow
    # trying to play with --prefetch-sectors n (in MiB), --encoding-io-strategy [readwrite|mmap] and -encoding-algorithm [32bit|64bit|SSE2|AltiVec]
    # NOTE/TODO: retest --method RS03 and --methodeRS03, not sure I tested without the space
    # pretty sure I tested with a space, and it didn't work, the option was not recognized
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    # NOTE: test 1
    # sudo chroot /mnt/gentoo sudo -i -u root-backups dvdisaster --image /srv/backup/dar.tmp/cleanme_bup.4.dar.iso -mRS03 --create --threads 4 --encoding-io-strategy readwrite --encoding-algorithm SSE2 --prefetch-sectors 8192
    # NOTE: test 2
    # sudo chroot /mnt/gentoo sudo -i -u root-backups dvdisaster --image /srv/backup/dar.tmp/cleanme_bup.4.dar.iso -mRS03 --create --threads 4 --encoding-io-strategy readwrite --encoding-algorithm 64bit --prefetch-sectors 8192
    # use more cpu ressources as expected, fan regularly go full
    # NOTE: benchmark
    # SSE2 readwrite
    # SSE2 mmap
    # 64bit readwrite
    # 64bit mmap
    # repeat for 1, 2, 3, 4, 5 threads
    # repeat for prefetch: 32MiB, 64MiB, 128MiB, 256MiB, 512MiB
    # repeat 5 times, in different orders
    # so 4x5x5x5 runs
    # NOTE: for now let's use that
    # a priori it is more efficient to use SSE2, and only 2 threads as there are only 2 physical cores
    # sudo chroot /mnt/gentoo sudo -i -u root-backups dvdisaster --image /srv/backup/dar.tmp/cleanme_bup.4.dar.iso -mRS03 --create --threads 2 --encoding-io-strategy readwrite --encoding-algorithm SSE2 --prefetch-sectors 256
    local bgcommand="sudo chroot ${mount_root_path} sudo -i -u root-backups dvdisaster --image ${ISODIRPLAIN}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX} -mRS03 --create --threads ${DVDISASTERNTHREADS} --encoding-io-strategy readwrite --encoding-algorithm SSE2 --prefetch-sectors 256"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    # NOTE: unable to use the long option --method, has to rely on -m
    #dvdisaster --image ${ISODIR}${ISOIMAGE}.${BURNER}${ISOSUFFIX} -mRS03 --create --threads ${DVDISASTERNTHREADS}
}

function burndiskdryrun {
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    #local bgcommand="sudo chroot ${mount_root_path} sudo -i -u root-backups growisofs -dry-run -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIRPLAIN}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX}"
    local bgcommand="growisofs -dry-run -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIR}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX}"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    #growisofs -dry-run -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIR}${ISOIMAGE}.${BURNER}${ISOSUFFIX}
}

function burndisk {
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    #local bgcommand="sudo chroot ${mount_root_path} sudo -i -u root-backups growisofs -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIRPLAIN}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX}"
    local bgcommand="growisofs -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIR}${DARARCHIVE}.${DARSLICE}${DARSUFFIX}${ISOSUFFIX}"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    # NOTE/TODO: does not work from systemd-nspawn. Why?
    #growisofs -dvd-compat -Z ${BURNERPATH}${BURNER}=${ISODIR}${ISOIMAGE}.${BURNER}${ISOSUFFIX}
}

function dvdisastermediuminfo {
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    local bgcommand="dvdisaster --device ${BURNERPATH}${BURNER} --medium-info"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    #dvdisaster --device ${BURNERPATH}${BURNER} --medium-info
}

function createburnermountdir {
    printf "\n"
    mkdir -v ${WORKDIRPATH}/${WORKDIR}/${BURNER}
}

function mountburner {
    printf "\n"
    sudo mount -v ${BURNERPATH}${BURNER} ${WORKDIRPATH}/${WORKDIR}/${BURNER}
}

function unmountburner {
    printf "\n"
    sudo umount -v ${WORKDIRPATH}/${WORKDIR}/${BURNER}
}

function removeburnermountdir {
    printf "\n"
    rmdir -v ${WORKDIRPATH}/${WORKDIR}/${BURNER}
}

function verifysigondisk {
    printf "\n"
    # NOTE: seen in man udisksctl "this program is not intended to be used by scripts or other programs"
    # mktemp tmpdir
    # mount disk
    # save pwd
    curdir="$(pwd)"
    # cd to disk mount point
    gpg --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    # cd back to previous pwd
    cd "${curdir}"
    # umount disk
    # rm tmpdir
}

function verifysigondiskasroot {
    printf "\n"
    #GPGHOMEDIR=/home/root-backups/.gnupg
    #gpg --homedir ${GPGHOMEDIR} --verify ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512.sig ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
    #sudo bash -c "GPGHOMEDIR=${ROOTPREFIX}/home/root-backups/.gnupg; gpg --homedir ${GPGHOMEDIR} --verify ${WORKDIRPATH}/${WORKDIR}/${BURNER}/*.sha512.sig ${WORKDIRPATH}/${WORKDIR}/${BURNER}/*.sha512"
    sudo bash -c "gpg --homedir ${ROOTPREFIX}/home/root-backups/.gnupg --verify ${WORKDIRPATH}/${WORKDIR}/${BURNER}/*.sha512.sig ${WORKDIRPATH}/${WORKDIR}/${BURNER}/*.sha512"
}

function verifychecksumondisk {
    printf "\n"
    pwd
    curdir="$(pwd)"
    cd ${ISODIR}${BURNER}
    pwd
    sha512sum -c *.sha512
    cd "${curdir}"
    pwd
}

function verifychecksumondiskasroot {
    sudo bash -c "pwd; curdir=\"$(pwd)\"; cd ${WORKDIRPATH}${WORKDIR}/${BURNER}; pwd; sha512sum -c *.sha512; cd \"${curdir}\"; pwd"
    #printf "\n"
    #pwd
    #curdir="$(pwd)"
    #cd ${ISODIR}${BURNER}
    #pwd
    #sha512sum -c *.sha512
    #cd "${curdir}"
    #pwd
}

function dvdisasterscan {
    # NOTE: the --device doesn't work, needs to use -d
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    local bgcommand="dvdisaster -d ${BURNERPATH}${BURNER} --scan"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    #echo "${bgcommand}"
    #dvdisaster --device ${BURNERPATH}${BURNER} --scan
}

function selectextractediso {
    printf "\n"
}

function printcurrentexcractediso {
    print "\n"
}

function dvdisasterextractimage {
    # NOTE: the --device doesn't work, needs to use -d
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    local bgcommand="dvdisaster -d ${BURNERPATH}${BURNER} --image ${ISODIR}${ISOEXTRACTED}.${BURNER}${ISOSUFFIX} --read"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    #echo "${bgcommand}"
    #dvdisaster --device ${BURNERPATH}${BURNER} --image ${ISODIR}${ISOEXTRACTED}.${BURNER}${ISOSUFFIX} --read
}

function asktesttype {
    # quick or normal
    # accept only 1 or 2 as input, and 0 too, to cancel
    while true; do
        printf "Please enter 0 to cancel, 1 to set a quick test or 2 to set a long test.\n"
        read TESTTYPE
        clear
        printf "TESTTYPE=${TESTTYPE}\n"
        case ${TESTTYPE} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting quick test.\n"
                TESTTYPE="=q"
                break
                ;;
            2)
                printf "Setting long test.\n"
                TESTTYPE=""
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "TESTTYPE=${TESTTYPE}\n"
}

function askimagetype {
    printf "\n"
    # generated or extracted
    # accept only 1 or 2 as input, and 0 too, to cancel
        while true; do
        printf "Please enter 0 to cancel, 1 to set an extracted iso or 2 to set a generated iso.\n"
        read IMAGETYPE
        clear
        printf "IMAGETYPE=${IMAGETYPE}\n"
        case ${IMAGETYPE} in
            0)
                printf "Cancelling.\n"
                break
                ;;
            1)
                printf "Setting extracted iso image.\n"
                IMAGETYPE="${ISOEXTRACTED}.${BURNER}"
                break
                ;;
            2)
                printf "Setting generated iso image.\n"
                IMAGETYPE="${DARARCHIVE}.${DARSLICE}${DARSUFFIX}"
                break
                ;;
        esac
        printf "Error, please retry.\n"
    done
    printf "IMAGETYPE=${IMAGETYPE}\n"

}

function dvdisastertestimage {
    # systemd-nspawn --directory=${mount_root_path} --machine=gentoo-installation
    # add option to choose which image to test: one to be burnt or one extracted
    # add option to choose if it's a quick test or not
    # use --test=q for quick test
    local bgcommand="dvdisaster --image ${ISODIRPLAIN}${IMAGETYPE}${ISOSUFFIX} --test ${TESTTYPE}"
    prepareruninbackground ${FUNCNAME[0]} "${bgcommand}"
    #dvdisaster --image ${ISODIR}${IMAGETYPE}.${BURNER}${ISOSUFFIX} --test ${TESTTYPE}
}

function dartest {
    # manual test OK
    dar --test ${WORKDIRPATH}/${WORKDIR}/${BURNER}/${DARARCHIVE}
}

function dardiff {
    printf "\n"
    dar --diff ${WORKDIRPATH}/${WORKDIR}/${BURNER}/${DARARCHIVE} --fs-root /srv/backup/readonly.mnt/backup/ --go-into cleanme.bup
}

function darextract {
    printf "\n"
    dar --extract /mnt/gentoo/var/tmp/dar-helper/sr0/cleanme_bup --fs-root /mnt/gentoo/srv/backup/dar.extract/
}

function bupfsck {
    # it uses par2 to verify
    sudo bup --bup-dir=/mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup fsck --jobs=3 --quick --verbose
}

function bupfsckparanoid {
    printf "\n"
    sudo bup --bup-dir=/mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup fsck --jobs=3 --verbose
}

function bupfsckrepair {
    printf "\n"
    # does that make sense at all?
    sudo bup --bup-dir=/mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup fsck --jobs=3 --quick --repair --verbose
}

function bupfsckrepairparanoid {
    printf "\n"
    sudo bup --bup-dir=/mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup fsck --jobs=3 --repair --verbose
}

function diffrestoredbup {
    printf "\n"
    sudo diff --recursive /mnt/gentoo/srv/backup/readonly.mnt/backup/cleanme.bup/ /mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup/
}

function buprestore {
    printf "\n"
    sudo bup --bup-dir=/mnt/gentoo/srv/backup/dar.extract/cleanme-bup/cleanme.bup restore --outdir=/mnt/gentoo/srv/backup/bup.extract/ --numeric-ids old-backups-to-be-cleaned/latest/mnt/.
}

function diffrestoreddata {
    printf "\n"
    sudo diff --recursive /mnt/gentoo/srv/backup/readonly.mnt/ /mnt/gentoo/srv/backup/bup.extract/
}

function printdisklabel {
    # string to write on the disk or in its accompanying note
    printf "Label for this disk: ${DARARCHIVE}.${DARSLICE}${DARSUFFIX}\n"
}

function cleanslicechecksumsig {
    rm -v ${SLICEPATH}/${SLICEBASENAME}.${SLICENUMBER}.${SLICEEXTENSION}.sha512
}

function cleanimages {
    sudo chroot ${mount_root_path} sudo -i -u root-backups rm -v ${ISODIRPLAIN}${IMAGETYPE}${ISOSUFFIX}
}

function f {
    printf "\n"
}

