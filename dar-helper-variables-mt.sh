# STATIC array of locks we have to clean at startup
# not ideal, but do not have a better idea
# only add functions name of functions meant to be managed
# as multi-processes
# do not forget to clean LOGLOCK as well
# actually, we use it to clean logs, fifos and ends
MPFUNC=(
    mpexample
    bgexample
    mediainfo
    formatbluray
    createiso
    augmentiso
    burndiskdryrun
    burndisk
    dvdisastermediuminfo
    dvdisasterscan
    dvdisasterextractimage
    dvdisastertestimage
)

mount_root_path="/mnt/gentoo"

SELPFAM="" # name of the selected "instance" function
# s/SELPROC/SELPFAM/gc
# if function in NUMPROCDICT and its NUMPROCDICT value is >0, then
# it is available
declare -gA NUMPROCDICT # key: fonction name, value: number of "instances" of the function
#declare -gA SELPROCDICT # key: function name, value: number of the selected "instance"
SELPNUM=""

FREENINST="" # this work as the creation of a background process is created manually

ROOTPREFIX="/mnt/gentoo"
SRVPREFIX="/srv"

# NOTE/TODO: shouldn't that be /srv/var/tmp instead?
#WORKDIRPATH="${ROOTPREFIX}${SRVPREFIX}/var/tmp"
WORKDIRPATH="${ROOTPREFIX}/var/tmp"
WORKDIR="dar-helper"

FIFOSUFFIX=".fifo"
BGFIFOSUFFIX=".bg.fifo"
LOGSUFFIX=".log"
BGLOGSUFFIX=".bg.log"
#LOCKSUFFIX=".lock"
PIDSUFFIX=".pid"
ENDSUFFIX=".end"


DARARCHIVE="cleanme_bup"
DARSLICE=""
DARSUFFIX=".dar"



LOGLOCK="log${LOCKSUFFIX}" # used when merging logs

#LOGPATH="${WORKDIRPATH}/${WORKDIR}"
LOG="$(date +%F_%T)-dar-helper${LOGSUFFIX}"

# TODO: IMAGILEPATH EXTRACTEDISOPATH
#IMAGEFILE="/srv/backup/dar.tmp/image.iso"
#EXTRACTEDISO="/srv/backup/dar.tmp/extracted.iso"
ISODIR="${ROOTPREFIX}/srv/backup/dar.tmp/"
ISODIRPLAIN="/srv/backup/dar.tmp/"
ISOIMAGE="image"
ISOEXTRACTED="extracted"
ISOSUFFIX=".iso"
BURNER=""
BURNERPATH="/dev/"
#declare -ga BURNERARR
#BURNERARR[0]=/dev/sr0
#BURNERARR[1]=/dev/sr1
# TODO: add function(s) to either/both detect burners and register them, or
# to add/remove one manually
GPGHOMEDIR="~/.gnupg"
GPGLOCALUSR=""
DVDISASTERNTHREADS=2 # no real rules here, I guess at most # threads of CPU minus 2 for the system? and reduce if IO can't follow. Check dvdisaster manual.

SELECTEDCONF=""
ETCCONF=/etc/dar-helper
HOMECONF=~/.config/dar-helper

TESTTYPE=""
